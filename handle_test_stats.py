import os
import json
import shutil
import argparse
from glob import glob
from datetime import date

import pandas as pd

def update_card_list():
	updated_card_list = set()
	for filename in glob('Assets/Prefabs/Resources/*/* Stats.prefab'):
		updated_card_list.add(os.path.basename(filename).split(' Stats.prefab')[0])

	test_stats = read_csv_file()
	new_cards = {card for card in updated_card_list if card not in test_stats.index}
	if not new_cards:
		return

	create_backup_of_csv_file()
	new_cards = pd.DataFrame(index=pd.Index(new_cards, name='Card name'),
		                     data={'Number of times tested': 0, 'Last time tested': None,
		                           'Number of wins': 0, 'Number of draws': 0, 'Number of losses': 0})
	test_stats = pd.concat([test_stats, new_cards], axis=0)
	test_stats.to_csv('test_stats/test_stats.csv')

def report_result(deck_name, game_result):
	create_backup_of_csv_file()
	update_card_list()

	with open('json.txt', 'r') as deck_infos_file:
		deck_infos = {deck_info['_name']: deck_info for deck_info in json.load(deck_infos_file)}
		deck_cards = [card_path.split('/')[1] for card_path in deck_infos[deck_name]['_cards_prefab_path'].keys()]
		deck_cards += [card_path.split('/')[1] for card_path in deck_infos[deck_name]['_combat_tricks_prefab_path']]
		deck_cards += [card_path.split('/')[1] for card_path in deck_infos[deck_name]['_generals_prefab_path']]

	test_stats = read_csv_file()
	if game_result == 'Win':
		test_stats.loc[deck_cards, 'Number of wins'] += 1
	elif game_result == 'Draw':
		test_stats.loc[deck_cards, 'Number of draws'] += 1
	elif game_result == 'Loss':
		test_stats.loc[deck_cards, 'Number of losses'] += 1
	else:
		raise ValueError("game_result must be one of 'Win', 'Draw' or 'Loss'.")
	test_stats.loc[deck_cards, 'Number of times tested'] += 1
	test_stats.loc[deck_cards, 'Last time tested'] = date.today()
	test_stats.to_csv('test_stats/test_stats.csv')

def rework_card(card_name):
	raise NotImplementedError()

def delete_card(card_name):
	raise NotImplementedError()

def rename_card(card_name):
	raise NotImplementedError()

def print_cards_based_on_number_of_times_tested(nb_cards_to_show, ascending=True):
	test_stats = read_csv_file()
	print(test_stats.sort_values('Number of times tested', ascending=ascending)[:nb_cards_to_show])

def print_least_recently_tested_cards(nb_cards_to_show):
	test_stats = read_csv_file()
	print(test_stats.sort_values('Last time tested', na_position='first')[:nb_cards_to_show])

def print_cards_based_on_winrate(nb_cards_to_show, ascending=False):
	test_stats = read_csv_file()
	test_stats = test_stats[test_stats['Number of times tested'] > 0]
	test_stats['Winrate'] = (test_stats['Number of wins'] + test_stats['Number of draws'] / 2) / test_stats['Number of times tested']
	print(test_stats.sort_values('Winrate', ascending=ascending)[:nb_cards_to_show])

def read_csv_file():
	return pd.read_csv('test_stats/test_stats.csv', index_col='Card name',
			           dtype={'Number of times tested': int,
			                  'Number of wins': int,
			                  'Number of draws': int,
			                  'Number of losses': int})

def create_backup_of_csv_file():
	shutil.copy2('test_stats/test_stats.csv', f'test_stats/test_stats_{date.today()}.csv')

if __name__ == '__main__':
	parser = argparse.ArgumentParser()