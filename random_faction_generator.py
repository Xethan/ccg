import random

if __name__ == '__main__':
    caractéristiques = {
        'peuples': ['Animaux', 'Créatures fantastiques', 'de la forêt', 'de la lune', 'de la mer', 'des nuages', 'du soleil'],
        'classes': ['Aristocrates', 'Bandits', 'Bardes', 'Cavaliers', 'Chevaliers', 'Conquistadors', 'Mousquetaires', 'Druides',
                    'Erudits', 'Moines', 'Ninjas', 'Pirates', 'Robots et cyborgs', 'Samouraïs', 'Scientifiques fous', 'Sorciers', 'Vikings'],
        'armes': ['Armes à distances', 'Armes à feu', 'Armes blanches', 'Arts martiaux', 'Buffs', 'Eau', 'Explosions', 'Feu', 'Foudre', 'Glace',
                  'Magie noire', 'Multi-éléments', 'Runes', 'Structures', 'Terre', 'Transformation', 'Vent', 'Technologie'],
        'environnements': ['Antiquité', 'Désert', 'Mégalopole', 'Steampunk', 'Îles'],
        'régime politique': ['Démocratie', 'Empire', 'Royaume', 'Féodal', 'Fédéralisme', 'Oligarchie', 'Technocratie', 'Anarchie', 'Plutocratie', 'Tribal']
        }

    random.seed()
    choix = []
    for possibilités in caractéristiques.values():
        choix.append(random.choice(possibilités))
        if random.randint(1, 10) == 10:
            choix.append(random.choice(possibilités))
    print(choix)