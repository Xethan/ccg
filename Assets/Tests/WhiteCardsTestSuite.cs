using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WhiteCardsTestSuite
{
    [SetUp]
    public void Setup()
    {
        SceneManager.LoadScene("Scenes/Deck Selection");
    }

    [UnityTest]
    public IEnumerator WhiteCardsTest()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(4,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(5,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario> { new CharacterScenario("Tokens/Mercenary", new Coord(6,1)) },
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string> {},
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            additional_mana_player_1: new Dictionary<int, int> { {0, 1000}, {4, 1000} });
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        Character general_1_player_2 = Board.getCharacter(new Coord(5,2));
        Character general_2_player_2 = Board.getCharacter(new Coord(8,2));
        Character spell_target = Board.getCharacter(new Coord(4,2));
        Character elite_range_target = Board.getCharacter(new Coord(6, 1));

        // ### Moon ###

        // Summon YoungInitiate
        Character young_initiate = Tools.createCardInPlayerHand<Character>(
            "White/Young Initiate", player:Board.getCurrentPlayer() );
        young_initiate.summon(Board.getTile(new Coord(0, 0)));
        Assert.AreEqual(young_initiate.Attack, 3);
        Assert.AreEqual(young_initiate.Armor, 3);
        young_initiate.receiveDamage(source:null, damage:2);

        // Summon EliteRanger for later tests
        Character elite_ranger = Tools.createCardInPlayerHand<Character>(
            "White/Elite Ranger", player:Board.getCurrentPlayer() );
        elite_ranger.summon(Board.getTile(new Coord(6, 2)));

        // Test DevotedAssassin (StrategicUnit)
        Character strategic_unit = Tools.createCardInPlayerHand<Character>(
            "White/Devoted Assassin", player:Board.getCurrentPlayer() );
        strategic_unit.play(Board.getTile(new Coord(4, 3)));

        Board.instance.endTurn();

        Assert.IsFalse( Board.isAttackValid(general_1_player_2, strategic_unit) );
        // YoungInitiate armor doesn't disappear at the end of its owner's turn
        Assert.AreEqual(young_initiate.Armor, 1);

        Board.instance.endTurn();

        // ### Sun ###

        Assert.AreEqual(young_initiate.Attack, 4);

        // Test HeatStroke
        Tools.createCardInPlayerHand<Card>(
            "White/Heat Stroke _ Night Soothing",
            player:Board.getCurrentPlayer()
        ).play(Board.getTile(new Coord(4, 2)));
        Assert.AreEqual(spell_target.Health, spell_target.MaxHealth - 6);

        // Test EliteRanger (Range and StrategicUnit)
        Assert.AreEqual( elite_ranger.Coord.squareDistance(general_2_player_2.Coord), 2);
        Assert.IsTrue( Board.isAttackValid(elite_ranger, general_2_player_2) );
        Board.instance.endTurn();
        Assert.IsFalse( Board.isAttackValid(general_1_player_2, elite_ranger) );

        Board.instance.endTurn();

        // ### Moon ###

        Assert.AreEqual(young_initiate.Armor, 3);

        // Test NightSoothing
        Tools.createCardInPlayerHand<Card>(
            "White/Heat Stroke _ Night Soothing",
            player:Board.getCurrentPlayer()
        ).play(Board.getTile(new Coord(4, 2)));
        Assert.AreEqual(spell_target.Health, spell_target.MaxHealth);

        // Test EliteRanger (Hunter 2, +5 health and Regen 5)
        elite_ranger.attack(elite_range_target);
        Assert.AreEqual(elite_range_target.Health, elite_range_target.MaxHealth - (elite_ranger.Attack + 2));
        Assert.AreEqual(elite_ranger.Health, elite_ranger.MaxHealth - elite_range_target.Attack);
        Board.instance.endTurn();
        Assert.AreEqual(elite_ranger.Health, elite_ranger.MaxHealth);

        Board.instance.endTurn();

        // ### Sun ###

        // Test DevotedAssassin (Rush)
        Character rush_unit = Tools.createCardInPlayerHand<Character>(
            "White/Devoted Assassin", player:Board.getCurrentPlayer() );
        rush_unit.play(Board.getTile(new Coord(4, 1)));
        Assert.IsTrue( Board.isAttackValid(rush_unit, general_1_player_2) );

        // Test DisturbedCycle
        Tools.createCardInPlayerHand<Card>(
            "White/Disturbed Cycle",
            player:Board.getCurrentPlayer()
        ).play(Board.getTile(new Coord(0, 0)));
        Board.instance.endTurn();
        Board.instance.endTurn();
        Assert.AreEqual(Board.daytime, Board.e_daytime.Sun);
    }
}
