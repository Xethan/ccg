using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SpidersTestSuite
{
    [SetUp]
    public void Setup()
    {
        SceneManager.LoadScene("Scenes/Deck Selection");
    }

    [UnityTest]
    public IEnumerator SpidersTest()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(0,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(1,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Green/Aspal", new Coord(5,2)),
                                                             new CharacterScenario("Green/Aspal", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario>  {
                new CharacterScenario("Green/Kahanaa", new Coord(6,2)),
                new CharacterScenario("Green/Chthunafh", new Coord(5,3)),
                new CharacterScenario("Tokens/Mercenary", new Coord(5,1)),
                new CharacterScenario("Yellow/Thakthun", new Coord(0,0)),
            },
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string>(),
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Green/Angry of aspal", 20} }
            );
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        General aspal = Board.getCharacter(new Coord(5,2)) as General;
        Character kahanaa = Board.getCharacter(new Coord(6,2)) as Character;
        Character chthunafh = Board.getCharacter(new Coord(5,3)) as Character;
        Character mercenary = Board.getCharacter(new Coord(5,1)) as Character;

        kahanaa.attack(aspal);
        Assert.AreEqual(aspal.Health, aspal.MaxHealth - kahanaa.Attack);
        Assert.AreEqual(kahanaa.Health, kahanaa.MaxHealth - aspal.Attack);

        int aspal_health_before_attack = aspal.Health;
        chthunafh.attack(aspal);
        Assert.AreEqual(aspal.Health, aspal_health_before_attack - chthunafh.CardStats.Attack);

        Board.instance.endTurn();
        Board.instance.endTurn();

        Board.getTile(kahanaa.Coord).setSpecialTile("Tiles/Web", Board.getCurrentPlayer());

        aspal_health_before_attack = aspal.Health;
        int kahanna_health_before_attack = kahanaa.Health;
        kahanaa.attack(aspal);
        Assert.AreEqual(aspal.Health, aspal_health_before_attack - kahanaa.Attack - 2);  // Backstab 2
        Assert.AreEqual(kahanaa.Health, kahanna_health_before_attack - (aspal.Attack - 1));  // Tough 1

        Board.getTile(chthunafh.Coord).setSpecialTile("Tiles/Web", Board.getCurrentPlayer());

        aspal_health_before_attack = aspal.Health;
        chthunafh.attack(aspal);
        // Chthunafh bonus attack + Flanking 1
        Assert.AreEqual(aspal.Health, aspal_health_before_attack - (chthunafh.CardStats.Attack + 2 + 1));

        Board.getTile(mercenary.Coord).setSpecialTile("Tiles/Web", Board.getCurrentPlayer());

        aspal_health_before_attack = aspal.Health;
        mercenary.attack(aspal);
        Assert.AreEqual(aspal.Health, aspal_health_before_attack - mercenary.Attack);  // No bonus
    }
}
