using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class YellowCardsTestSuite
{
    [SetUp]
    public void Setup()
    {
        SceneManager.LoadScene("Scenes/Deck Selection");
    }

    [UnityTest]
    public IEnumerator RoublabotTest()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Yellow/Azir", new Coord(4,2)),
                                                             new CharacterScenario("Yellow/Azir", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Yellow/Simire", new Coord(6,2)),
                                                             new CharacterScenario("Yellow/Simire", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario> { new CharacterScenario("Yellow/Roublabot", new Coord(5,2)) },
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string> {},
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Yellow/Four Winds", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Yellow/Four Winds", 20} },
            additional_mana_player_1: new Dictionary<int, int> { {0, 1000}, {3, 1000} });
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        Character general_1_player_1 = Board.getCharacter(new Coord(4,2));
        Character roublabot = Board.getCharacter(new Coord(5,2));
        Character general_1_player_2 = Board.getCharacter(new Coord(6,2));
        Character general_2_player_2 = Board.getCharacter(new Coord(8,2));

        // ### Blink ###
        roublabot.ActivatedAbilities[2].activate();
        _simulateLeftClick(general_2_player_2); // Should not work
        _simulateLeftClick(general_1_player_1);

        Assert.IsTrue(roublabot.Coord == new Coord(3, 2));

        roublabot.ActivatedAbilities[2].activate();
        _simulateLeftClick(general_1_player_1);
        Assert.IsTrue(roublabot.Coord == new Coord(5, 2));

        roublabot.ActivatedAbilities[2].activate();
        _simulateLeftClick(general_1_player_2);
        Assert.IsTrue(roublabot.Coord == new Coord(7, 2));

        // ### Push ###
        roublabot.ActivatedAbilities[0].activate();
        // Select target to push
        _simulateLeftClick(general_1_player_2, multi_target:true);
        // Select tile to push to
        _simulateLeftClick(general_1_player_2, multi_target:true); // should not work
        _simulateLeftClick(general_1_player_1, multi_target:true); // should not work
        _simulateLeftClick(Board.getTile(new Coord(5, 2)), multi_target:true);
        Assert.IsTrue(general_1_player_2.Coord == new Coord(5, 2));

        // ### Pull ###
        roublabot.ActivatedAbilities[1].activate();
        // Select target to pull
        _simulateLeftClick(general_1_player_1); // should not work
        _simulateLeftClick(general_2_player_2); // should not work
        _simulateLeftClick(general_1_player_2);
        Assert.IsTrue(general_1_player_2.Coord == new Coord(6, 2));

        // ### Blink again ###
        roublabot.ActivatedAbilities[2].activate();
        _simulateLeftClick(general_2_player_2); // should not work
        _simulateLeftClick(general_1_player_2);
        Assert.IsTrue(roublabot.Coord == new Coord(5, 2));

        // ### Cannot push same character again ###
        roublabot.ActivatedAbilities[0].activate();
        _simulateLeftClick(general_1_player_2, multi_target:true);
        _simulateLeftClick(Board.getTile(new Coord(7, 2)), multi_target:true);
        Assert.IsTrue(general_1_player_2.Coord == new Coord(6, 2));
    }

    private void _simulateLeftClick(MonoBehaviour target, bool multi_target=false)
    {
        Vector3 screen_position_target = Camera.main.WorldToScreenPoint(target.transform.position);

        PointerEventData event_data = new PointerEventData(EventSystem.current);
        event_data.button = PointerEventData.InputButton.Left;
        event_data.position = new Vector2(
            screen_position_target.x,
            screen_position_target.y
        );

        GameObject panel;
        if (multi_target)
            panel = MultiTargetingPanel._instance.gameObject;
        else
            panel = TargetingPanel._instance.gameObject;
        ExecuteEvents.Execute<IPointerClickHandler>(
            target:panel,
            eventData:event_data,
            functor:ExecuteEvents.pointerClickHandler
        );
    }
}
