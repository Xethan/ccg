using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class KeywordTestSuite
{
    [SetUp]
    public void Setup()
    {
        SceneManager.LoadScene("Scenes/Deck Selection");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LifestealTest()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(4,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Green/Aspal", new Coord(5,2)),
                                                             new CharacterScenario("Green/Aspal", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario>  { new CharacterScenario("Tokens/Mercenary", new Coord(4,3)) },
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string>(),
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Green/Angry of aspal", 20} }
            );
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        General xethan = Board.getCharacter(new Coord(4,2)) as General;
        General aspal = Board.getCharacter(new Coord(5,2)) as General;
        Character mercenary = Board.getCharacter(new Coord(4,3)) as Character;
        mercenary.receiveModifier(typeof(LifestealBuff));

        Board.instance.endTurn();

        aspal.move(Board._board[Coord.raw(5, 2)], Board._board[Coord.raw(5, 3)]);
        aspal.attack(xethan);
        Assert.AreEqual(xethan.Health, xethan.MaxHealth - aspal.Attack);
        Assert.AreEqual(aspal.Health, aspal.MaxHealth - xethan.Attack);

        Board.instance.endTurn();

        int xethan_health_berfore_mercenary_attack = xethan.Health;
        mercenary.attack(aspal);
        Assert.AreEqual(xethan_health_berfore_mercenary_attack + mercenary.Attack, xethan.Health);
    }

    [UnityTest]
    public IEnumerator ToughAndShiftingSandTest()
    {
        Time.timeScale = 1.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(4,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Green/Aspal", new Coord(5,2)),
                                                             new CharacterScenario("Green/Aspal", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario>  { new CharacterScenario("Tokens/Mercenary", new Coord(4,3)) },
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string>(),
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int>()
            );
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        Character mercenary = Board.getCharacter(new Coord(4,3));
        mercenary.receiveModifier(typeof(ToughBuff), param:1);
        ShiftingSandTile.summonOrLevelUpShiftingSand(Board._board[Coord.raw(4,3)], Board.getEnnemyPlayer());
        int mercenary_health_before_end_of_turn = mercenary.Health;
        Board.instance.endTurn();
        // mercenary should take 2 damage from ShiftingSand minus 1 from Tough
        Assert.AreEqual(mercenary_health_before_end_of_turn - 1, mercenary.Health);
    }

    [UnityTest]
    public IEnumerator ManaCostModifTest()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(4,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(5,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario>(),
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string> {"Purple/Unit Mana Blocker"},
            cards_prefab_path_in_hand_player_2: new List<string> {"Purple/Scryer"},
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            additional_mana_player_1: new Dictionary<int, int> { {0, 4}, {2, 4} });
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        Card unit_mana_blocker = Board.getCurrentPlayer().iterateOverHand().First();
        Card scryer = Board.getEnnemyPlayer().iterateOverHand().First();
        Assert.IsTrue(scryer.Owner.ManaReserve.canPayManaCost(scryer, scryer.ManaCost.raw_cost));
        unit_mana_blocker.play(Board._board[Coord.raw(3,2)]);
        Assert.IsFalse(scryer.Owner.ManaReserve.canPayManaCost(scryer, scryer.ManaCost.raw_cost));
        // Assert UI change ?
        Board.instance.endTurn();
        Board.instance.endTurn();
        Assert.IsTrue(scryer.Owner.ManaReserve.canPayManaCost(scryer, scryer.ManaCost.raw_cost));
    }

    [UnityTest]
    public IEnumerator CorrosionTest()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(4,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Green/Aspal", new Coord(5,2)),
                                                             new CharacterScenario("Green/Aspal", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario>  { new CharacterScenario("Purple/Draskel", new Coord(3,3)) },
            characters_player_2: new List<CharacterScenario>  { new CharacterScenario("Tokens/Mercenary", new Coord(3,2)) },
            cards_prefab_path_in_hand_player_1: new List<string> {"Purple/Corrosive Curse"},
            cards_prefab_path_in_hand_player_2: new List<string> {"Tokens/Mercenary"},
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            additional_mana_player_1: new Dictionary<int, int> { {Factions.neutral, 10} }
        );
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        Card corrosive_curse = Board.getCurrentPlayer().iterateOverHand().First();
        Card mercenary_card = Board.getEnnemyPlayer().iterateOverHand().First();

        Character xethan = Board.getCharacter(new Coord(4,2)) as Character;
        Character draskel = Board.getCharacter(new Coord(3,3)) as Character;
        Character mercenary = Board.getCharacter(new Coord(3,2)) as Character;
        mercenary.receiveModifier(typeof(IncreasedHealthBuff), param : 15);

        int health_before_combat = mercenary.Health;
        draskel.attack(mercenary);
        Assert.AreEqual(mercenary.Health, health_before_combat - draskel.Attack);

        health_before_combat = mercenary.Health;
        xethan.attack(mercenary);
        Assert.AreEqual(mercenary.Health, health_before_combat - xethan.Attack - 1);

        corrosive_curse.play(Board._board[Coord.raw(4,3)]);

        Board.instance.endTurn();

        health_before_combat = mercenary.Health;
        mercenary.attack(xethan);
        Assert.AreEqual(mercenary.Health, health_before_combat - xethan.Attack - 1);

        mercenary_card.play(Board._board[Coord.raw(4,3)]);
        Character mercenary_2 = Board.getCharacter(new Coord(4,3)) as Character;
        mercenary_2.receiveModifier(typeof(IncreasedHealthBuff), param : 15);

        Board.instance.endTurn();

        health_before_combat = mercenary.Health;
        draskel.attack(mercenary);
        Assert.AreEqual(mercenary.Health, health_before_combat - draskel.Attack - 2);

        health_before_combat = mercenary_2.Health;
        xethan.attack(mercenary_2);
        Assert.AreEqual(mercenary_2.Health, health_before_combat - xethan.Attack - 1);

        Board.instance.endTurn();

        health_before_combat = mercenary_2.Health;
        mercenary_2.attack(xethan);
        Assert.AreEqual(mercenary_2.Health, health_before_combat - xethan.Attack - 2);
    }
}
