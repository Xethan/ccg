﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class TestSuite
{
    [SetUp]
    public void Setup()
    {
        SceneManager.LoadScene("Scenes/Deck Selection");
    }

    [Test]
    public void TestSuiteSimplePasses()
    {
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator TestSuiteWithEnumeratorPasses()
    {
        Time.timeScale = 20.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(4,2)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(0,2)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Green/Aspal", new Coord(5,2)),
                                                             new CharacterScenario("Green/Aspal", new Coord(8,2)) },
            characters_player_1: new List<CharacterScenario>(),
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string>(),
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int> { {"Green/Angry of aspal", 20} });
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        General xethan = Board._board[Coord.raw(4, 2)].Character as General;
        General aspal = Board._board[Coord.raw(5, 2)].Character as General;

        aspal.move(Board._board[Coord.raw(5, 2)], Board._board[Coord.raw(5, 3)]);
        aspal.attack(xethan);
        Assert.AreEqual(xethan.MaxHealth - aspal.Attack, xethan.Health);
        Assert.AreEqual(aspal.MaxHealth - xethan.Attack, aspal.Health);
        Board.instance.endTurn();
    }
}
