using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CoordTestSuite
{
    [SetUp]
    public void Setup()
    {
        SceneManager.LoadScene("Scenes/Deck Selection");
    }

    // A UnityTest behaves like a coroutine in PlayMode and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator CoordOperatorsTest()
    {
        Assert.IsTrue( new Coord(2, 3) == new Coord(2, 3) );
        Assert.IsFalse( new Coord(2, 3) == new Coord(4, 3) );
        Assert.IsFalse( new Coord(2, 3) == new Coord(2, -3) );

        Assert.IsFalse( new Coord(2, 3) != new Coord(2, 3) );
        Assert.IsTrue( new Coord(2, 3) != new Coord(4, 3) );
        Assert.IsTrue( new Coord(2, 3) != new Coord(2, -3) );

        Assert.IsTrue( new Coord(2, 3) == (new Coord(-1, 2) + new Coord(3, 1)) );

        Assert.IsTrue( new Coord(-4, 1) == (new Coord(-1, 2) - new Coord(3, 1)) );

        Assert.IsTrue( new Coord(-2, 4) == (new Coord(-1, 2) * 2) );
        Assert.IsTrue( new Coord(0, 0) == (new Coord(-1, 2) * 0) );

        yield return null;
    }

    [UnityTest]
    public IEnumerator CoordSimpleFunctionsTest()
    {
        Assert.IsTrue( (new Coord(2, 3)).unitaryCoord() == new Coord(1, 1) );
        Assert.IsTrue( (new Coord(-2, 0)).unitaryCoord() == new Coord(-1, 0) );
        Assert.IsTrue( (new Coord(0, 0)).unitaryCoord() == new Coord(0, 0) );

        Assert.AreEqual( (new Coord(2, 3)).distance(new Coord(1, 4)), 2 );
        Assert.AreEqual( (new Coord(2, 3)).distance(new Coord(-1, 4)), 4 );

        Assert.AreEqual( (new Coord(2, 3)).squareDistance(new Coord(1, 4)), 1 );
        Assert.AreEqual( (new Coord(2, 3)).squareDistance(new Coord(-1, 4)), 3 );

        Assert.IsTrue( (new Coord(2, 3)).isNearby(new Coord(1, 4)) );
        Assert.IsFalse( (new Coord(2, 3)).isNearby(new Coord(0, 4)) );

        Assert.IsTrue( (new Coord(2, 3)).isAligned(new Coord(2, 4)) );
        Assert.IsTrue( (new Coord(2, 3)).isAligned(new Coord(2, 18)) );
        Assert.IsTrue( (new Coord(2, 3)).isAligned(new Coord(1, 3)) );
        Assert.IsFalse( (new Coord(2, 3)).isAligned(new Coord(1, 4)) );

        Assert.IsTrue( (new Coord(0, 0)).isInsideBoard() );
        Assert.IsTrue( (new Coord(2, 3)).isInsideBoard() );
        Assert.IsTrue( (new Coord(8, 6)).isInsideBoard() );
        Assert.IsFalse( (new Coord(2, -1)).isInsideBoard() );
        Assert.IsFalse( (new Coord(2, 7)).isInsideBoard() );
        Assert.IsFalse( (new Coord(-1, 3)).isInsideBoard() );
        Assert.IsFalse( (new Coord(9, 3)).isInsideBoard() );

        yield return null;
    }

    [UnityTest]
    public IEnumerator createListCoordsTest()
    {
        _assertAreEquivalent(
            (new Coord(2, 3)).createSafeCenteredCircleCoords(radius:1).ToList(),
            new List<Coord> { new Coord(1, 3), new Coord(3, 3), new Coord(2, 2), new Coord(2, 4) }
        );
        _assertAreEquivalent(
            (new Coord(2, 3)).createSafeCenteredCircleCoords(radius:3).ToList(),
            new List<Coord> {
                new Coord(0, 2), new Coord(0, 4), new Coord(1, 1), new Coord(1, 5),
                new Coord(2, 0), new Coord(2, 6), new Coord(3, 1), new Coord(3, 5),
                new Coord(4, 2), new Coord(4, 4), new Coord(5, 3)
            }
        );

        _assertAreEquivalent(
            Coord.createCircleCoords(radius:2).ToList(),
            new List<Coord> {
                new Coord(-2, 0), new Coord(2, 0), new Coord(0, -2), new Coord(0, 2),
                new Coord(-1, -1), new Coord(-1, 1), new Coord(1, -1), new Coord(1, 1)
            }
        );

        _assertAreEquivalent(
            (new Coord(2, 3)).createSafeCenteredSquareCoords(radius:3).ToList(),
            new List<Coord> {
                new Coord(0, 0), new Coord(1, 0), new Coord(2, 0), new Coord(3, 0), new Coord(4, 0), new Coord(5, 0),
                new Coord(5, 1), new Coord(5, 2), new Coord(5, 3), new Coord(5, 4), new Coord(5, 5), new Coord(5, 6),
                new Coord(0, 6), new Coord(1, 6), new Coord(2, 6), new Coord(3, 6), new Coord(4, 6),
            }
        );

        _assertAreEquivalent(
            Coord.createSquareCoords(radius:2).ToList(),
            new List<Coord> {
                new Coord(-2, -2), new Coord(-2, -1), new Coord(-2, 0), new Coord(-2, 1),
                new Coord(-2, 2), new Coord(-1, 2), new Coord(0, 2), new Coord(1, 2),
                new Coord(2, 2), new Coord(2, 1), new Coord(2, 0), new Coord(2, -1),
                new Coord(2, -2), new Coord(1, -2), new Coord(0, -2), new Coord(-1, -2)
            }
        );

        yield return null;
    }

    [UnityTest]
    public IEnumerator BoardCoordFunctionsTest()
    {
        Time.timeScale = 1.0f;

        Scenario scenario = new Scenario(
            generals_player_1: new List<CharacterScenario> { new CharacterScenario("Purple/Xethan", new Coord(2,3)),
                                                             new CharacterScenario("Purple/Xethan", new Coord(2,5)) },
            generals_player_2: new List<CharacterScenario> { new CharacterScenario("Green/Aspal", new Coord(6,3)),
                                                             new CharacterScenario("Green/Aspal", new Coord(6,5)) },
            characters_player_1: new List<CharacterScenario>  { new CharacterScenario("Tokens/Mercenary", new Coord(2,6)),
                                                                new CharacterScenario("Tokens/Mercenary", new Coord(4,4)) },
            characters_player_2: new List<CharacterScenario>(),
            cards_prefab_path_in_hand_player_1: new List<string>(),
            cards_prefab_path_in_hand_player_2: new List<string>(),
            cards_prefab_path_in_deck_player_1: new Dictionary<string, int> { {"Purple/Scryer", 20} },
            cards_prefab_path_in_deck_player_2: new Dictionary<string, int>()
            );
        DeckSelection.instance.setUpScenario(scenario);
        yield return null;
        yield return null;
        yield return null;

        Character mercenary_2_6 = Board.getCharacter(new Coord(2,6));
        Character mercenary_4_4 = Board.getCharacter(new Coord(4,4));

        assertAreEquivalent(
            Board.getCharactersInRange(mercenary_4_4.Coord.createSafeCenteredCircleCoords, radius:2).ToList(),
            new List<Character>()
        );
        assertAreEquivalent(
            Board.getCharactersInRange(mercenary_4_4.Coord.createSafeCenteredCircleCoords, radius:3).ToList(),
            new List<Character> { Board.getCharacter(new Coord(2, 3)), Board.getCharacter(new Coord(2, 5)),
                                  Board.getCharacter(new Coord(6, 3)), Board.getCharacter(new Coord(6, 5)) }
        );

        Assert.IsTrue(Board.isThereAlliedCharacterInRange(mercenary_2_6.Owner, mercenary_2_6.Coord, 1));
        Assert.IsFalse(Board.isThereAlliedCharacterInRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 1));
        Assert.IsFalse(Board.isThereAlliedCharacterInRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 2));
        Assert.IsTrue(Board.isThereAlliedCharacterInRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 3));
        Assert.IsTrue(Board.isThereAlliedCharacterInRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 4));

        Assert.IsTrue(Board.isThereAlliedCharacterInSquareRange(mercenary_2_6.Owner, mercenary_2_6.Coord, 1));
        Assert.IsFalse(Board.isThereAlliedCharacterInSquareRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 1));
        Assert.IsTrue(Board.isThereAlliedCharacterInSquareRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 2));
        Assert.IsTrue(Board.isThereAlliedCharacterInSquareRange(mercenary_4_4.Owner, mercenary_4_4.Coord, 3));

        Assert.IsTrue(Board.isCharacterIsolated(mercenary_4_4));
        Assert.IsFalse(Board.isCharacterIsolated(mercenary_2_6));
    }

    public void assertAreEquivalent<T>(List<T> list_1, List<T> list_2) where T : class
    {
        if ( list_1.Count != list_2.Count )
            Assert.Fail("Both list don't have the same length.");

        foreach (T elem in list_1)
            findInListAndDelete(elem, list_2);
    }

    public void findInListAndDelete<T>(T elem_to_find, List<T> list) where T : class
    {
        foreach (T elem in list)
        {
            if ( elem == elem_to_find )
            {
                list.Remove(elem);
                return;
            }
        }
        Assert.Fail("Could not find elem " + elem_to_find);
    }

    public void _assertAreEquivalent(List<Coord> list_1, List<Coord> list_2)
    {
        if ( list_1.Count != list_2.Count )
            Assert.Fail("Both list don't have the same length.");

        foreach (Coord elem in list_1)
            _findInListAndDelete(elem, list_2);
    }

    public void _findInListAndDelete(Coord elem_to_find, List<Coord> list)
    {
        foreach (Coord elem in list)
        {
            if ( elem == elem_to_find )
            {
                list.Remove(elem);
                return;
            }
        }
        Assert.Fail("Could not find elem " + elem_to_find);
    }
}