{"frames": {
	"crest_f1.png":{
		"rotated": false,"trimmed": true,
		"frame": {"x":0,"y":1138,"w":682,"h":683},
		"spriteSourceSize": {"x":2,"y":9,"w":687,"h":700},
		"sourceSize": {"w":687,"h":700}
	},
	"crest_f2.png":{
		"rotated": false,"trimmed": true,
		"frame": {"x":0,"y":465,"w":688,"h":671},
		"spriteSourceSize": {"x":6,"y":19,"w":698,"h":700},
		"sourceSize": {"w":698,"h":700}
	},
	"crest_f3.png":{
		"rotated": false,"trimmed": true,
		"frame": {"x":694,"y":0,"w":622,"h":698},
		"spriteSourceSize": {"x":39,"y":5,"w":700,"h":725},
		"sourceSize": {"w":700,"h":725}
	},
	"crest_f4.png":{
		"rotated": false,"trimmed": true,
		"frame": {"x":1309,"y":700,"w":621,"h":686},
		"spriteSourceSize": {"x":40,"y":17,"w":700,"h":725},
		"sourceSize": {"w":700,"h":725}
	},
	"crest_f5.png":{
		"rotated": false,"trimmed": true,
		"frame": {"x":0,"y":0,"w":692,"h":463},
		"spriteSourceSize": {"x":4,"y":1,"w":700,"h":465},
		"sourceSize": {"w":700,"h":465}
	},
	"crest_f6.png":{
		"rotated": false,"trimmed": true,
		"frame": {"x":684,"y":1138,"w":623,"h":725},
		"spriteSourceSize": {"x":39,"y":0,"w":700,"h":725},
		"sourceSize": {"w":700,"h":725}
	}
},
"meta": {
	"app": "ShoeBox",
	"size": {"w":2048,"h":2048}
}
}