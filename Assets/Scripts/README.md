Groupe de classes :
- Collection ->
- DeckSelection ->
- `Scripts/Abilities` -> Capacités activées génériques
- `Scripts/Cards` -> les cartes et leurs effets, classées par couleur. Les stats (+ nom / coût en mana / description / etc.) sont dans les prefabs Unity

- `Scripts/Game`
    - `GameElement` : arborescence des différents éléments jouables du jeu
        -> `SpecialTile`
        -> `Card`
            -> `Spell`
                -> `BattleCry`
                -> `CombatTrick`
            -> `Character`
                -> `General`
                -> `Unit`
                    -> `Token`
            -> `Artifact`

    - `ActivatedAbilityPanel` -> UI ?
    - `Board`, `Tile`, `Coord` -> Board
    - `ManaReserve`, `Player` -> Player
    - `Buff`, `GlobalCharacterModifier`, `Modifier`, `ManaCostModif` -> Card Effects

- `Scripts/Tools`
    - `ActivatedAbility` -> Card Effects
    - `CardStats`, `Factions`, `ManaCost`
    - `LoadSceneButton` `EventQueue`, `TriggerQueue`, `Trigger` -> Events
    - `SerializableDeck` -> Collection
    - `Tools` (`ìnstantiateFromPath` + `GridLayoutParameters`)

- `Script/UI`
    - `Hand`, `PanelCombatTrick`, `PanelBattleCries` -> Player ?
    - UI du Board
    - UI de Hover (à gauche)
    - DeckTracker
    - Slot pour Hand / ActivatedAbility / PanelBattleCries / PanelCombatTrick
    - TargetingPanel / CardSelectionPanel / TutorialPanel / DeckThumbnail / Shaders

- `Scripts/Cards` -> Séparer à l'intérieur des couleurs ? Par type de carte ? Mettre les Buff dans les fichers des cartes ?