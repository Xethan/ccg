using System.Collections.Generic;

public class TrampleBuff : Buff
{
	private ActivatedAbility _trample_ability = null;

	protected override string Description { get { return "Trample"; } }

	public override List<ActivatedAbility> getActivatedAbilitiesAfterBuff(List<ActivatedAbility> activated_abilities)
	{
        if ( _trample_ability is null )
            _trample_ability = new Trample(character:_buffed_character);

		activated_abilities.Add(_trample_ability);
		return activated_abilities;
	}
}