using System;
using UnityEngine;

public class Trample : ActivatedAbility
{
	public override string Cost { get { return "Move"; } }
	public override string Name { get { return "Trample"; } }

	private bool	_used_this_turn = false;

	public Trample(Character character) : base(character) {}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
			_used_this_turn = false;
	}

	public override bool payCost()
	{
		return _used_this_turn == false;
	}

	public override void doEffect()
	{
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();

			return target != null
				&& target.Character is Unit
				&& target.Character.Owner != _character.Owner
				&& target.Character.Coord.isNearby(_character.Coord)
				&& (Board.isCharacterProvoked(_character) == false
					|| target.Character.isFlagUp(Character.e_flags.Provoke))
				&& (Board.isCharacterTackled(_character) == false
					|| target.Character.isFlagUp(Character.e_flags.Tackle))
				&& target.Character.Health <= _character.Attack;
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			Character target = gameobject.GetComponent<Tile>().Character;

			target.receiveDamage(_character, _character.Attack);
			_character.receiveDamage(target, target.Attack);
			if ( target != null )
				target.death();

			if ( gameobject.GetComponent<Tile>().Character == null )
				_character.move(
					Board.getTile(_character.Coord),
					gameobject.GetComponent<Tile>()
				);

			Trigger.activate(
				Trigger.e_type.Trample,
				source: _character,
				tile: gameobject.GetComponent<Tile>()
			);
			_used_this_turn = true;
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}