using UnityEngine;

public abstract class GameElement : MonoBehaviour
{
    protected Player    _owner;

    public Player Owner
    {
        get { return _owner; }
        set { _owner = value; }
    }
}