using UnityEngine;
using System.Collections.Generic;

public abstract class GlobalCharacterModifier : Modifier
{
	public override void initialize(Character buffed_character, BuffUI buff_ui, int param, int param2, GameElement source, System.Type buff)
	{
		base.initialize(buffed_character, buff_ui, param, param2, source, buff);
		Board._global_character_modifiers.Add(this);
	}

	public override void destroy()
	{
		Board._global_character_modifiers.Remove(this);
		base.destroy();
	}

	public virtual int getAttackAfterBuff(Character character, int attack) { return attack; }
	public virtual int getHealthAfterBuff(Character character, int health) { return health; }
	public virtual int getMaxHealthAfterBuff(Character character, int max_health) { return max_health; }
	public virtual int getMovementAfterBuff(Character character, int movement) { return movement; }
	public virtual int getRangeAfterBuff(Character character, int range) { return range; }
	public virtual List<ActivatedAbility> getActivatedAbilitiesAfterBuff(Character launcher, List<ActivatedAbility> activated_abilities) { return activated_abilities; }
	public virtual Character.e_flags getFlagsAfterBuff(Character character, Character.e_flags flags) { return flags; }
	public virtual int getValueEffectAfterBuff(Trigger trigger, int amount) { return amount; }
}