using UnityEngine;

public abstract class Lieutenant : Character
{
	private BattleCry	_battle_cry;

	protected abstract string	BattleCryPrefabPath { get; }
	public BattleCry BattleCry { get { return _battle_cry; } }

	new public void initialize(CardStats lieutenant_card_stats)
	{
		base.initialize(lieutenant_card_stats);

		string card_class_name = BattleCryPrefabPath.Split(new System.Char [] {'/'})[1].Replace(" ", "");
		CardStats card_stats = Tools.instantiateFromPath(BattleCryPrefabPath + " Stats").GetComponent<CardStats>();
		_battle_cry = Tools.instantiateFromPath("Base Card").AddComponent(System.Type.GetType(card_class_name)) as BattleCry;
		_battle_cry.Owner = _owner;
		_battle_cry.initialize(this, card_stats);
	}
}