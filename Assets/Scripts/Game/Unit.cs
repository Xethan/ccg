using UnityEngine;
using System; // Enum

public abstract class Unit : Character
{
	private ManaCostUI	_mana_cost_ui;

	protected virtual e_types Types { get { return 0; } }
	public enum e_types
	{
		Spider = 1,
	};

	protected override void onCreate()
	{
		base.onCreate();
		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_card_hover_ui.PanelManaCostUI.transform, false);
		_mana_cost_ui.updateUI(_mana_cost);
	}

	protected override void _onSummon()
	{
		onSummon();
		Trigger.activate(Trigger.e_type.Summon, receiver : this, tile : Board.getTile(_coord));

		// Temporary : rush is currently given in _onSummon() but it should be able to be given at any time during the turn
		if ( isFlagUp(e_flags.Rush) )
			reactivate(e_flags.Can_move_attack);
	}

	protected virtual void onSummon() {}

	public bool Is(string unit_type)
	{
		return (Unit.e_types)Enum.Parse(typeof(Unit.e_types), unit_type) == this.Types;
	}
}