using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class Player : MonoBehaviour
{

	private static System.Random			_rng = new System.Random();

	private bool							_is_player_one;
	private List<General>					_generals = new List<General>();
	private List<Card>						_deck = new List<Card>();
	private DeckTracker						_deck_tracker;
	private ManaReserve						_mana_reserve;
	private Hand							_hand;
	private PanelBattleCries				_panel_battle_cries;
	private PanelCombatTricks				_panel_combat_tricks;

	public bool isPlayerOne { get { return _is_player_one; } }
	public List<General> Generals { get { return _generals; } }
	public ManaReserve ManaReserve { get { return _mana_reserve; } }
	public PanelBattleCries PanelBattleCries { get { return _panel_battle_cries;}}

	public IEnumerable<Card> iterateOverDeck()
	{
		foreach (Card card in _deck)
			yield return card;
	}

	public IEnumerable<Card> iterateOverHand()
	{
		foreach (CardSlot card_slot in _hand.iterateOverCardSlots())
			yield return card_slot.Card;
	}

	public void initialize(Dictionary<string, int> deck, bool is_player_one)
	{
		foreach (KeyValuePair<string, int> item in deck)
		{
			string card_prefab_path = item.Key;
			for (int i = 0; i < item.Value; ++i)
			{
				Card card = Tools.instantiateCardFromPath<Card>(card_prefab_path, owner:this);
				card.transform.SetParent(this.transform, false);
				card.gameObject.SetActive(false);
				_deck.Add(card);
			}
		}
		_is_player_one = is_player_one;
	}

	public void initializeGeneral(General general)
	{
		_generals.Add(general);
	}

	public void doMulligan()
	{
		List<Card> starting_hand = new List<Card>();

		int nb_cards_at_start = Board.nb_cards_at_start + (_is_player_one ? 0 : 1);

		for (int i = 0; i < nb_cards_at_start; ++i)
		{
			int index = Random.Range(0, _deck.Count - 1);
			starting_hand.Add(_deck[index]);
			_deck.RemoveAt(index);
		}
		CardSelectionPanel.instance.startCardSelection(starting_hand, 0, 3, resolveMulligan, selection_is_replacement:true);
	}

	private void resolveMulligan(List<CardSelectionSlot> card_slots)
	{
		List<Card> hand = new List<Card>();

		for (int i = 0; i < card_slots.Count; ++i)
		{
			card_slots[i].Card.outHover();
			card_slots[i].Card.gameObject.SetActive(false);
			if ( card_slots[i].Selected == true )
			{
				int index = Random.Range(0, _deck.Count - 1);
				Card new_card = _deck[index];

				card_slots[i].Card.transform.SetParent(this.transform, false);
				_deck.RemoveAt(index);
				_deck.Add(card_slots[i].Card);
				hand.Add(new_card);
			}
			else
				hand.Add(card_slots[i].Card);
		}
		shuffleDeck();
		initializeStartingHand(hand);
		StartCoroutine(Board.mulliganDone(this));
	}

	public void scry(int nb_cards)
	{
		List<Card> cards_to_scry = new List<Card>();

		for (int i = 0; i < nb_cards; ++i)
			cards_to_scry.Add(_deck[i]);
		CardSelectionPanel.instance.startCardSelection(cards_to_scry, 0,  nb_cards, resolveScry, selection_is_replacement:true);
	}

	private void resolveScry(List<CardSelectionSlot> card_slots)
	{
		int offset = 0;

		for (int i = 0; i < card_slots.Count; ++i)
		{
			card_slots[i].Card.outHover();
			card_slots[i].Card.gameObject.SetActive(false);
			if ( card_slots[i].Selected == true )
			{
				_deck.RemoveAt(i - offset);
				_deck.Add(card_slots[i].Card);
				offset += 1;
			}
		}
	}

	public void draw(int nb_cards)
	{
		for (int i = 0; i < nb_cards; ++i)
		{
			if ( _deck.Count <= 0 ) // lose the game / lose hp
				SceneManager.LoadScene("Scenes/Main Menu");
			else
				_hand.draw(_deck[0]);
				_deck.RemoveAt(0);
		}
		_deck_tracker.updateUI(_deck);
		Trigger.activate(Trigger.e_type.Draw, player : this, amount : nb_cards);
	}

	public void putCardInHand(Card card)
	{
		_hand.draw(card);
	}

	public void removeCardFromDeck(Card card)
	{
		int index = _deck.IndexOf(card);

		if ( index != -1 )
		{
			_deck.RemoveAt(index);
			_deck_tracker.updateUI(_deck);
		}
	}

	private void shuffleDeck()
	{
		int n = _deck.Count;
		while (n > 1)
		{
			n--;
			int k = _rng.Next(n + 1);
			Card card = _deck[k];
			_deck[k] = _deck[n];
			_deck[n] = card;
		}
	}

	public void initializeStartingHand(List<Card> starting_hand)
	{
		_hand = Tools.instantiateFromPath("Game UI/Hand").GetComponent<Hand>();
		_hand.transform.SetParent(Board.instance.transform.parent, false);
		foreach (Card card in starting_hand)
			_hand.draw(card);
		/* Has to be active to be initialized, desactivated until the start of the turn */
		_hand.gameObject.SetActive(false);

		instantiateDeckTracker();
	}

	private void instantiateDeckTracker()
	{
		_deck_tracker = Tools.instantiateFromPath("Game UI/Deck Tracker").GetComponent<DeckTracker>();
		_deck_tracker.transform.SetParent(Board.instance.transform.parent, false);
		_deck_tracker.updateUI(_deck);
		_deck_tracker.gameObject.SetActive(false);
	}

	public void instantiatePanelBattleCries(BattleCry battle_cry_1, BattleCry battle_cry_2)
	{
		_panel_battle_cries = Tools.instantiateFromPath("Game UI/Panel Battle Cries").GetComponent<PanelBattleCries>();
		_panel_battle_cries.transform.SetParent(Board.instance.transform.parent, false);
		_panel_battle_cries.initialize(battle_cry_1, battle_cry_2);
		/* Has to be active to be initialized, desactivated until the start of the turn */
		_panel_battle_cries.gameObject.SetActive(false);
	}

	public void instantiatePanelCombatTricks(List<string> combat_tricks_prefab_path)
	{
		List<CombatTrick> combat_tricks = new List<CombatTrick>();
		foreach (string prefab_path in combat_tricks_prefab_path)
		{
			CombatTrick combat_trick = Tools.instantiateCardFromPath<CombatTrick>(prefab_path, owner:this);
			combat_tricks.Add(combat_trick);
		}
		_panel_combat_tricks = Tools.instantiateFromPath("Game UI/Panel Combat Tricks").GetComponent<PanelCombatTricks>();
		_panel_combat_tricks.transform.SetParent(Board.instance.transform.parent, false);
		_panel_combat_tricks.initialize(combat_tricks);
		/* Has to be active to be initialized, desactivated until the start of the turn */
		_panel_combat_tricks.gameObject.SetActive(false);
	}

	public void instantiateManaReserve(Factions factions_1, Factions factions_2)
	{
		_mana_reserve = Tools.instantiateFromPath("Game UI/Mana Reserve").GetComponent<ManaReserve>();
		_mana_reserve.transform.SetParent(Board.instance.transform.parent, false);
		_mana_reserve.initialize(this, factions_1, factions_2, iterateOverDeck());
		_mana_reserve.gameObject.SetActive(false);
	}

	public void updateHandManaCostUI()
	{
		foreach (CardSlot card_slot in _hand.iterateOverCardSlots())
			card_slot.updateUI();
	}

	public void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == this )
		{
			draw(2);
			_mana_reserve.endTurn();
			_mana_reserve.gameObject.SetActive(false);
			_hand.gameObject.SetActive(false);
			_panel_battle_cries.endTurn();
			_panel_battle_cries.gameObject.SetActive(false);
			_panel_combat_tricks.endTurn();
			_panel_combat_tricks.gameObject.SetActive(false);
			_deck_tracker.gameObject.SetActive(false);
		}
		else if ( trigger.type == Trigger.e_type.Start_turn && trigger.player == this )
		{
			_mana_reserve.startTurn();
			_mana_reserve.gameObject.SetActive(true);
			_hand.gameObject.SetActive(true);
			_panel_battle_cries.gameObject.SetActive(true);
			_panel_combat_tricks.setActive();
			_deck_tracker.gameObject.SetActive(true);
		}
		foreach (CardSlot card_slot in _hand.iterateOverCardSlots())
			card_slot.Card.onTriggerInHand(trigger, card_slot);
		foreach (Card deck_card in iterateOverDeck())
			deck_card.onTriggerInDeck(trigger);
		_mana_reserve.onTrigger(trigger); // for ManaCostModifs
	}
}