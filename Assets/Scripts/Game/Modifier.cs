using UnityEngine;

public abstract class Modifier : MonoBehaviour
{
	protected Character			_buffed_character;
	private BuffUI				_buff_ui;
	protected int				_param;
	protected int 				_param2;
	protected GameElement 		_source;
	protected System.Type 		_buff;

	protected virtual string Description { get { return "null"; } }

	public virtual void initialize(
		Character buffed_character,
		BuffUI buff_ui,
		int param,
		int param2,
		GameElement source,
		System.Type buff
	) {
		_buffed_character = buffed_character;
		_buff_ui = buff_ui;
		_param = param;
		_param2 = param2;
		_source = source;
		_buff = buff;
		updateUI();
	}

	protected void updateUI()
	{
		if ( _buff_ui is null )
			return;

		_buff_ui.updateUI(Description);
	}

	public virtual void destroy()
	{
		if ( _buff_ui is object )
			GameObject.Destroy(_buff_ui.gameObject);
		GameObject.Destroy(this);
	}

	/* For modifiers that have effects under certain conditions */
	public virtual void onTrigger(Trigger trigger) {}
	public virtual void onTriggerInHand(Trigger trigger, CardSlot card_slot) {}
	public virtual void onTriggerInDeck(Trigger trigger) {}
}