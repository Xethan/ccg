using UnityEngine;
using UnityEngine.UI;
using System; // Math
using System.Collections.Generic;

public abstract class Character : Card
{
	protected List<ActivatedAbility>	_activated_abilities = new List<ActivatedAbility>();

	private CharacterUI					_character_ui;
	private CharacterHoverUI			_character_hover_ui;

	protected int						_attack;
	protected int						_health;
	protected int						_armor = 0;
	protected int						_max_health;
	protected int						_movement = 2;
	protected Coord						_coord;
	protected List<Artifact>			_artifacts = new List<Artifact>();

	private bool                        _is_dying = false;

	/* Default value = 0 : Invocation sickness and no ability */
	protected e_flags					_flags = 0;
	public enum e_flags
	{
		Can_move = 1,
		Can_attack = 1 << 1,
		Can_move_attack = (Can_move | Can_attack),
		Rush = 1 << 2,
		Trample = 1 << 3,
		Provoke = 1 << 4,
		StrategicUnit = 1 << 5,
		Tackle = 1 << 6,
	};

	public Coord Coord
	{
		get { return _coord; }
		set { _coord = value; }
	}
	public bool isFlagUp(e_flags flag) { return (Flags & flag) == flag; }

	protected virtual void _onSummon() {}

	/* For units that have effects under certain conditions */
	public virtual void onTrigger(Trigger trigger)
	{
		if ( trigger.Is("Start_turn") && trigger.player == _owner )
			reactivate(e_flags.Can_move_attack);

		/* Iterating over lists copied to arrays so as not to skip an element if it destroys itself */
		foreach (Modifier modifier in _modifiers.ToArray())
			modifier.onTrigger(trigger);
		foreach (ActivatedAbility activated_ability in _activated_abilities.ToArray())
			activated_ability.onTrigger(trigger);
		foreach(Artifact artifact in _artifacts.ToArray())
			artifact.onTrigger(trigger);

		// Remove armor after going trough end of turn modifier triggers (relevant for YoungInitiate)
		if ( trigger.Is("End_turn") )
		{
			_armor = 0;
			_image.material.SetFloat("_Greyscale", 0);
		}
	}

	protected override void onCreate()
	{
		_attack = _card_stats.Attack;
		_health = _card_stats.Health;
		_max_health = _card_stats.Health;
		instantiateCharacterUI();
	}

	private void instantiateCharacterUI()
	{
		_character_ui = Tools.instantiateFromPath("Game UI/Character UI").GetComponent<CharacterUI>();
		_character_ui.transform.SetParent(this.transform, false);
		_character_ui.gameObject.SetActive(false);

		_character_hover_ui = Tools.instantiateFromPath("Game UI/Character Hover UI").GetComponent<CharacterHoverUI>();
		_character_hover_ui.transform.SetParent(_card_hover_ui.transform, false);

		_character_ui.updateUI(Attack, _card_stats.Attack, Health, _card_stats.Health, MaxHealth, _armor);
		_character_hover_ui.updateUI(Attack, _card_stats.Attack, Health, _card_stats.Health, MaxHealth, _armor);
	}

	protected virtual void Update()
	{
		_character_ui.updateUI(Attack, _card_stats.Attack, Health, _card_stats.Health, MaxHealth, _armor);
		_character_hover_ui.updateUI(Attack, _card_stats.Attack, Health, _card_stats.Health, MaxHealth, _armor);
		if ( Health <= 0 )
			death();
	}

	public void death()
	{
		if ( _is_dying )
			return;

		_is_dying = true;

		Trigger death_trigger = new Trigger(Trigger.e_type.Death, receiver : this);

		Action _death = () =>
		{
			if (Board.getCharacter(_coord) == this)
				Board.getTile(_coord).Character = null;
			TileHighlightManager.outHoverCharacter();
			destroy();
		};

		TriggerQueue.instance.newTrigger(death_trigger, callback_function:_death);
	}

	public override bool canPlay(Tile tile)
	{
		return Board.isSummoningValid(this, tile) && _owner.ManaReserve.canPayManaCost(this, _mana_cost.raw_cost);
	}

	protected override bool _play(Tile tile)
	{
		if ( Board.isSummoningValid(this, tile) && _owner.ManaReserve.payManaCost(this, _mana_cost.raw_cost) )
		{
			summon(tile);
			return true;
		}
		return false;
	}

	public void summon(Tile tile)
	{
		if ( tile.Character != null )
			return;

		tile.Character = this;
		_character_ui.gameObject.SetActive(true);
		if ( _owner != null && _owner.isPlayerOne == false )
			Tools.flipImage(_image);
		_onSummon();
	}

	public void receiveArtifact(Artifact artifact, ArtifactUI artifact_ui)
	{
		_artifacts.Add(artifact);
		artifact.transform.SetParent(_card_hover_ui.PanelBuffsUI.transform, false);
	}

	public void loseArtifact(Artifact artifact_to_lose)
	{
		_artifacts.Remove(artifact_to_lose);
	}

	public void dispell()
	{
		Trigger.activate(Trigger.e_type.Dispell, receiver:this);
		foreach (Modifier modifier in _modifiers)
			modifier.destroy();
		_modifiers.Clear();
		_activated_abilities.Clear();

		/* Generals can be dispelled but it only removes their buffs, not their abilities */
		if ( this is Unit )
		{
			_text = "Dispelled";
			_card_hover_ui.updateUI(_image.sprite, _factions, _name, _text);
		}
	}

	/* Remove the possibility to move and / or attack this turn */
	public void exhaust(e_flags flag)
	{
		_flags &= ~(e_flags.Can_move_attack & flag);

		if ( !isFlagUp(e_flags.Can_attack) && !isFlagUp(e_flags.Can_move) )
			_image.material.SetFloat("_Greyscale", 1);
	}

	/* Re-add the possibility to move and / or attack this turn */
	public void reactivate(e_flags flag)
	{
		_flags |= e_flags.Can_move_attack & flag;

		if ( isFlagUp(e_flags.Can_attack) || isFlagUp(e_flags.Can_move) )
			_image.material.SetFloat("_Greyscale", 0);
	}

	public void move(Tile old_tile, Tile new_tile)
	{
		int distance = old_tile.Coord.distance(new_tile.Coord);

		exhaust(e_flags.Can_move);
		old_tile.Character = null;
		new_tile.Character = this;
		Trigger.activate(Trigger.e_type.Movement, source : this, tile : new_tile, tile2 : old_tile, amount : distance);
	}

	public void push(GameElement source, Coord direction, int amount)
	{
		if ( direction.X != 0 && direction.Y != 0 )
			throw new ArgumentException("Pushing diagonally is not allowed.");

		Tile starting_tile = Board.getTile(_coord);
		direction = direction.unitaryCoord();
		for (int remaining_amount = amount; remaining_amount > 0; remaining_amount--)
		{
			Coord new_coord = _coord + direction;

			if ( new_coord.isInsideBoard() && Board.getCharacter(new_coord) == null )
			{
				Board.getTile(_coord).Character = null;
				Board.getTile(new_coord).Character = this;
			}
			else
			{
				int damage_amount = 2*remaining_amount;
				receiveDamage(null, damage_amount);
				Trigger.activate(Trigger.e_type.Push_damage, source: source, receiver: this, amount: damage_amount);
				break;
			}
		}
		Trigger.activate(Trigger.e_type.Push,
			             source: source, receiver : this,
			             tile: starting_tile, tile2 : Board.getTile(_coord),
			             amount : amount);
	}

	public void pull(GameElement source, Character puller)
	{
		if ( !puller.Coord.isAligned(_coord) )
			throw new ArgumentException("Pulling diagonally is not allowed.");

		Tile starting_tile = Board.getTile(_coord);
		Coord unitary_vector_this_to_puller = (puller.Coord - _coord).unitaryCoord();
		int amount = _coord.distance(puller.Coord) - 1;

		for (int remaining_amount = amount; remaining_amount > 0; remaining_amount--)
		{
			Coord new_coord = _coord + unitary_vector_this_to_puller;
			if ( Board.getCharacter(new_coord) != null )
				throw new ArgumentException("Encoutered a character, pull is invalid.");

			Board.getTile(_coord).Character = null;
			Board.getTile(new_coord).Character = this;
		}
		Trigger.activate(Trigger.e_type.Pull,
			             source: source, receiver: this,
			             tile: starting_tile, tile2 : Board.getTile(_coord),
			             amount: amount);
	}

	public void teleport(GameElement source, Coord target_coord, bool allow_tp_generals=false)
	{
		Tile current_tile = Board.getTile(_coord);
		Tile target_tile = Board.getTile(target_coord);

		if ( target_tile.Character is General && allow_tp_generals == false )
			return;

		Trigger.activate(Trigger.e_type.Teleportation, source:source, tile:target_tile);

		current_tile.Character = target_tile.Character; // other character or null if target_tile is empty
		target_tile.Character = this;
	}

	public void symmetricTeleport(GameElement source, Coord symmetry_origin)
	{
		Coord target_coord = symmetry_origin + symmetry_origin - _coord;
		if ( target_coord.isInsideBoard() )
			teleport(source, target_coord);
	}

	public void attack(Character opponent)
	{
		exhaust(e_flags.Can_move_attack);
		fight(opponent);
		if ( BackstabBuff.isBackstabAttack(this, opponent) )
			Trigger.activate(Trigger.e_type.Backstab, source:this, receiver:opponent);
		if ( FlankingBuff.isFlankingAttack(this, opponent) )
			Trigger.activate(Trigger.e_type.Flanking, source:this, receiver:opponent);
	}

	public void fight(Character opponent, bool automatic_counter_attack=false)
	{
		// Attack
		Trigger attack_trigger = Trigger.activate(Trigger.e_type.Attack, source:this, receiver:opponent);
		int damage_amount = getValueEffectAfterBuffs(attack_trigger, Attack);
		damage_amount = opponent.getValueEffectAfterBuffs(attack_trigger, damage_amount);
		foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
			damage_amount = global_character_modifier.getValueEffectAfterBuff(attack_trigger, damage_amount);
		opponent.receiveDamage(this, damage_amount, check_for_death: false);

		// Counter attack
		if ( _coord.squareDistance(opponent.Coord) <= opponent.Range || automatic_counter_attack )
		{
			Trigger counter_attack_trigger = Trigger.activate(Trigger.e_type.Counter_attack, source:opponent, receiver:this);
			damage_amount = getValueEffectAfterBuffs(counter_attack_trigger, opponent.Attack);
			damage_amount = opponent.getValueEffectAfterBuffs(counter_attack_trigger, damage_amount);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				damage_amount = global_character_modifier.getValueEffectAfterBuff(counter_attack_trigger, damage_amount);
			receiveDamage(opponent, damage_amount, check_for_death:false);
		}
		if ( opponent.Health <= 0 )
			opponent.death();
		if ( Health <= 0 )
			death();
	}

	public void receiveDamage(GameElement source, int damage, bool check_for_death=true)
	{
		// Should we update the trigger damage amount after each function ?
		Trigger trigger = new Trigger(Trigger.e_type.Damage, source : source, receiver : this, amount : damage);
		if ( source is Character && source != this)
			damage = (source as Character).getValueEffectAfterBuffs(trigger, damage);
		damage = getValueEffectAfterBuffs(trigger, damage);
		foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
			damage = global_character_modifier.getValueEffectAfterBuff(trigger, damage);

		if ( damage <= 0 )
			return;

		if ( damage <= _armor )
			_armor -= damage;
		else
		{
			_health -= damage - _armor;
			_armor = 0;
		}

		Trigger.propagate(trigger);

		/* Iterating over the list copied to an array so as not to skip an artifact if it destroys itself */
		foreach (Artifact artifact in _artifacts.ToArray())
			artifact.receiveDamage(damage);

		DamageUI damage_ui = Tools.instantiateFromPath("Game UI/Damage UI").GetComponent<DamageUI>();
		damage_ui.initialize(damage);
		damage_ui.gameObject.transform.SetParent(Board.getTile(_coord).gameObject.transform, false);

		if ( check_for_death && Health <= 0 )
			death();
	}

	public void receiveHeal(int heal)
	{
		if ( heal <= 0 )
			return;

		_health += heal;
		if ( Health > MaxHealth )
			_health -= Health - MaxHealth;
		Trigger.activate(Trigger.e_type.Heal, receiver : this, amount : heal);
	}

	public void receiveArmor(int armor)
	{
		if ( armor <= 0 )
			return;

		_armor += armor;
		Trigger.activate(Trigger.e_type.Armor, receiver: this, amount: armor);
	}

	public override void onHover(GameObject panel_hover)
	{
		base.onHover(panel_hover);

		/* When in play only */
		if ( _coord != null )
			TileHighlightManager.onHoverCharacter(this);
	}

	public override void outHover()
	{
		base.outHover();

		/* When in play only */
		if ( _coord != null )
			TileHighlightManager.outHoverCharacter();
	}

	public int getValueEffectAfterBuffs(Trigger trigger, int amount)
	{
		foreach (Modifier modifier in _modifiers)
			if (modifier is Buff)
				amount = (modifier as Buff).getValueEffectAfterBuff(trigger, amount);
		foreach (Artifact artifact in _artifacts)
			foreach (Modifier modifier in artifact.Modifiers)
				if (modifier is Buff)
					amount = (modifier as Buff).getValueEffectAfterBuff(trigger, amount);
		return amount;
	}

	public int Attack
	{
		get
		{
			int attack = _attack;

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					attack = (modifier as Buff).getAttackAfterBuff(attack);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						attack = (modifier as Buff).getAttackAfterBuff(attack);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				attack = global_character_modifier.getAttackAfterBuff(this, attack);

			return Math.Max(attack, 0);
		}
	}

	public int Health
	{
		get
		{
			int health = _health;

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					health = (modifier as Buff).getHealthAfterBuff(health);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						health = (modifier as Buff).getHealthAfterBuff(health);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				health = global_character_modifier.getHealthAfterBuff(this, health);

			return Math.Max(health, 0);
		}
	}

	public int MaxHealth
	{
		get
		{
			int max_health = _max_health;

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					max_health = (modifier as Buff).getMaxHealthAfterBuff(max_health);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						max_health = (modifier as Buff).getMaxHealthAfterBuff(max_health);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				max_health = global_character_modifier.getMaxHealthAfterBuff(this, max_health);

			return Math.Max(max_health, 0);
		}
	}

	public int Movement
	{
		get
		{
			int movement = _movement;

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					movement = (modifier as Buff).getMovementAfterBuff(movement);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						movement = (modifier as Buff).getMovementAfterBuff(movement);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				movement = global_character_modifier.getMovementAfterBuff(this, movement);

			return Math.Max(movement, 0);
		}
	}

	public int Range
	{
		get
		{
			int range = 1;

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					range = (modifier as Buff).getRangeAfterBuff(range);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						range = (modifier as Buff).getRangeAfterBuff(range);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				range = global_character_modifier.getRangeAfterBuff(this, range);

			return Math.Max(range, 0);
		}
	}

	public List<ActivatedAbility> ActivatedAbilities
	{
		get
		{
			List<ActivatedAbility> activated_abilities = new List<ActivatedAbility>(_activated_abilities);

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					activated_abilities = (modifier as Buff).getActivatedAbilitiesAfterBuff(activated_abilities);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						activated_abilities = (modifier as Buff).getActivatedAbilitiesAfterBuff(activated_abilities);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				activated_abilities = global_character_modifier.getActivatedAbilitiesAfterBuff(this, activated_abilities);

			return activated_abilities;
		}
	}

	public e_flags Flags
	{
		get
		{
			e_flags flags = _flags;

			foreach (Modifier modifier in _modifiers)
				if (modifier is Buff)
					flags = (modifier as Buff).getFlagsAfterBuff(flags);
			foreach (Artifact artifact in _artifacts)
				foreach (Modifier modifier in artifact.Modifiers)
					if (modifier is Buff)
						flags = (modifier as Buff).getFlagsAfterBuff(flags);
			foreach (GlobalCharacterModifier global_character_modifier in Board._global_character_modifiers)
				flags = global_character_modifier.getFlagsAfterBuff(this, flags);

			return flags;
		}
	}

	public int Armor { get { return _armor; } }
}