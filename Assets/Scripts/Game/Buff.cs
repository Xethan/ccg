using UnityEngine;
using System.Collections.Generic;

public abstract class Buff : Modifier
{
	public virtual int getAttackAfterBuff(int attack) { return attack; }
	public virtual int getHealthAfterBuff(int health) { return health; }
	public virtual int getMaxHealthAfterBuff(int max_health) { return max_health; }
	public virtual int getMovementAfterBuff(int movement) { return movement; }
	public virtual int getRangeAfterBuff(int range) { return range; }
	public virtual List<ActivatedAbility> getActivatedAbilitiesAfterBuff(List<ActivatedAbility> activated_abilities) { return activated_abilities; }
	public virtual Character.e_flags getFlagsAfterBuff(Character.e_flags flags) { return flags; }
	public virtual int getValueEffectAfterBuff(Trigger trigger, int amount) { return amount; }
}