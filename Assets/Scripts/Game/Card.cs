﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class Card : GameElement
{
	[SerializeField]
	private CardStats		_card_stats_prefab;
	protected CardStats		_card_stats;

	protected CardHoverUI	_card_hover_ui;
	protected Image			_image;
	protected ManaCost		_mana_cost;
	protected string		_name;
	protected Factions		_factions;
	protected string		_text;

	protected List<Modifier>	_modifiers = new List<Modifier>();

	public Factions Factions { get { return _factions; } }
	public ManaCost ManaCost
	{
		get { return _mana_cost; }
		set { _mana_cost = value; }
	}
	public CardStats CardStats { get { return _card_stats; } }

	public virtual void initialize(CardStats card_stats)
	{
		_card_stats = card_stats;
		_card_stats.transform.SetParent(this.transform, false);
		this.gameObject.name = _card_stats.Name;

		_image = Tools.instantiateFromPath("Game UI/Card Image").GetComponent<Image>();
		_image.transform.SetParent(this.transform, false);

		_image.sprite = _card_stats.Sprite;
		_mana_cost = _card_stats.ManaCost;
		_name = _card_stats.Name;
		_factions = _card_stats.Factions;
		_text = _card_stats.Text;

		_image.material = new Material(_image.material);

		instantiateHoverUI();
		onCreate();
	}

	protected virtual void onCreate() {}

	public void onTriggerInHand(Trigger trigger, CardSlot card_slot)
	{
		/* Iterating over the list copied to an array so as not to skip a modifier if it destroys itself */
		foreach (Modifier modifier in _modifiers.ToArray())
			modifier.onTriggerInHand(trigger, card_slot);
	}

	public void onTriggerInDeck(Trigger trigger)
	{
		/* Iterating over the list copied to an array so as not to skip a modifier if it destroys itself */
		foreach (Modifier modifier in _modifiers.ToArray())
			modifier.onTriggerInDeck(trigger);
	}

	public void destroy()
	{
		/* _card_hover_ui may not be the child of the Card so it is destroyed individually */
		GameObject.Destroy(_card_hover_ui.gameObject);
		GameObject.Destroy(this.gameObject);
	}

	public bool play(Tile tile)
	{
		bool was_played = _play(tile);
		if ( was_played )
			Trigger.activate(Trigger.e_type.Play, source:this);
		return was_played;
	}

	public abstract bool canPlay(Tile tile);
	protected abstract bool _play(Tile tile);

	private void instantiateHoverUI()
	{
		_card_hover_ui = Tools.instantiateFromPath("Game UI/Card Hover UI").GetComponent<CardHoverUI>();
		_card_hover_ui.transform.SetParent(this.transform, false);
		_card_hover_ui.updateUI(_image.sprite, _factions, _name, _text);
		_card_hover_ui.gameObject.SetActive(false);
	}

	public virtual void onHover(GameObject card_hover_panel)
	{
		_card_hover_ui.transform.SetParent(card_hover_panel.transform, false);
		_card_hover_ui.gameObject.SetActive(true);
	}

	public virtual void outHover()
	{
		_card_hover_ui.transform.SetParent(this.transform, false);
		_card_hover_ui.gameObject.SetActive(false);
	}

	public Modifier createModifier(
		System.Type type,
		int param=0,
		int param2=0,
		GameElement source=null,
		System.Type buff=null,
		bool should_display_buff_ui=false
	) {
		BuffUI buff_ui = null;
		if ( should_display_buff_ui )
		{
			buff_ui = Tools.instantiateFromPath("Game UI/Buff UI").GetComponent<BuffUI>();
			buff_ui.transform.SetParent(_card_hover_ui.PanelBuffsUI.transform, false);
		}

		Modifier new_modifier = this.gameObject.AddComponent(type) as Modifier;
		Character buffed_character = (this is Character) ? this as Character : null;
		new_modifier.initialize(buffed_character, buff_ui, param, param2, source, buff);
		return new_modifier;
	}

	private Modifier _receiveModifier(
		System.Type type,
		int param,
		int param2,
		GameElement source,
		System.Type buff,
		bool should_display_buff_ui
	) {
		Modifier modifier = createModifier(type, param, param2, source, buff, should_display_buff_ui);
		_modifiers.Add(modifier);
		// Trigger.receiver can only be a character but a spell can receive a Modifier
		Character receiver = (this is Character) ? this as Character : null;
		Trigger.activate(Trigger.e_type.Buff, receiver:receiver, modifier:modifier);
		return modifier;
	}

	public Buff receiveModifier(
		System.Type type,
		int param=0,
		int param2=0,
		GameElement source=null,
		System.Type buff=null,
		bool should_display_buff_ui=false
	) {
		return _receiveModifier(
			type, param, param2, source, buff, should_display_buff_ui
		) as Buff;
	}

	public GlobalCharacterModifier receiveGlobalModifier(
		System.Type type,
		GameElement source,
		int param=0,
		int param2=0,
		System.Type buff=null,
		bool should_display_buff_ui=false
	) {
		return _receiveModifier(
			type, param, param2, source, buff, should_display_buff_ui
		) as GlobalCharacterModifier;
	}

	public void loseModifier(Modifier modifier_to_lose)
	{
		modifier_to_lose.destroy();
		_modifiers.Remove(modifier_to_lose);
	}
}
