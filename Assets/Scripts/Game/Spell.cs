using UnityEngine;

public abstract class Spell : Card
{
	private ManaCostUI	_mana_cost_ui;

	public abstract bool isTargetValid(Tile target);
	protected abstract void doEffect(Tile target);

	protected override void onCreate()
	{
		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_card_hover_ui.PanelManaCostUI.transform, false);
		_mana_cost_ui.updateUI(_mana_cost);
	}

	public override bool canPlay(Tile tile)
	{
		return isTargetValid(tile) && _owner.ManaReserve.canPayManaCost(this, _mana_cost.raw_cost);
	}

	protected override bool _play(Tile tile)
	{
		if ( isTargetValid(tile) && _owner.ManaReserve.payManaCost(this, _mana_cost.raw_cost) )
		{
			doEffect(tile);
			Trigger.activate(Trigger.e_type.Spell, source:this, tile:tile);
			if ( !(this is BattleCry) && !(this is CombatTrick) )
				destroy();
			return true;
		}
		return false;
	}
}