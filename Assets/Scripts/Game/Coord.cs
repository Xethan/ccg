using UnityEngine;
using System; // Math
using System.Collections.Generic;

public class Coord
{
	private readonly int	_x;
	private readonly int	_y;

	public int RawCoord { get { return _y * Board.nb_columns + _x; } }
	public string Description { get { return "(" + _x + ", " + _y + ")"; } }
	public int X { get { return _x; } }
	public int Y { get { return _y; } }

	public Coord(int x, int y)
	{
		_x = x;
		_y = y;
	}

	public Coord(int coord)
	{
		_x = coord % Board.nb_columns;
		_y = coord / Board.nb_columns;
	}

	public static bool operator==(Coord lhs, Coord rhs)
	{
		if ( (object)lhs == null && (object)rhs == null )
			return true;
		else if ( (object)lhs == null || (object)rhs == null )
			return false;

		return lhs.X == rhs.X && lhs.Y == rhs.Y;
	}

	public static bool operator!=(Coord lhs, Coord rhs)
	{
		return !(lhs == rhs);
	}

	public static Coord operator+(Coord lhs, Coord rhs)
	{
		return new Coord(lhs.X + rhs.X, lhs.Y + rhs.Y);
	}

	public static Coord operator-(Coord lhs, Coord rhs)
	{
		return new Coord(lhs.X - rhs.X, lhs.Y - rhs.Y);
	}

	public static Coord operator*(Coord lhs, int rhs)
	{
		return new Coord(lhs.X * rhs, lhs.Y * rhs);
	}

	public Coord unitaryCoord()
	{
		int new_x = _x == 0 ? 0 : _x / Math.Abs(_x);
		int new_y = _y == 0 ? 0 : _y / Math.Abs(_y);
		return new Coord(new_x, new_y);
	}

	public int distance(Coord coord)
	{
		return Math.Abs(coord.X - _x) + Math.Abs(coord.Y - _y);
	}

	public int squareDistance(Coord coord)
	{
		return Math.Max(Math.Abs(coord.X - _x), Math.Abs(coord.Y - _y));
	}

	public bool isNearby(Coord coord)
	{
		return squareDistance(coord) == 1;
	}

	public bool isAligned(Coord coord)
	{
		return (_x == coord.X || _y == coord.Y);
	}

	public bool isInsideBoard()
	{
		return _x >= 0 && _x < Board.nb_columns && _y >= 0 && _y < Board.nb_lines;
	}

	// Deprecated, use isInsideBoard instead
	public bool isOutOfBoard()
	{
		return !isInsideBoard();
	}

	public IEnumerable<Coord> createSafeCenteredCircleCoords(int radius)
	{
		foreach (Coord coord in Coord.createCircleCoords(radius))
		{
			Coord new_coord = this + coord;
			if ( new_coord.isInsideBoard() )
				yield return new_coord;
		}
	}

	public static IEnumerable<Coord> createCircleCoords(int radius)
	{
		for (int i = 0; i < radius; ++i)
		{
			yield return new Coord(i, i - radius);
			yield return new Coord(-i, radius - i);
			yield return new Coord(i - radius, -i);
			yield return new Coord(radius - i, i);
		}
	}

	public IEnumerable<Coord> createSafeCenteredSquareCoords(int radius)
	{
		foreach (Coord coord in Coord.createSquareCoords(radius))
		{
			Coord new_coord = this + coord;
			if ( new_coord.isInsideBoard() )
				yield return new_coord;
		}
	}

	public static IEnumerable<Coord> createSquareCoords(int radius)
	{
		for (int i = 0; i < radius; ++i)
		{
			yield return new Coord(-radius+i, -radius);
			yield return new Coord(i, -radius);
			yield return new Coord(radius, -radius+i);
			yield return new Coord(radius, i);
			yield return new Coord(radius-i, radius);
			yield return new Coord(-i, radius);
			yield return new Coord(-radius, radius-i);
			yield return new Coord(-radius, -i);
		}
	}

	public static int raw(int x, int y)
	{
		return y * Board.nb_columns + x;
	}
}