using UnityEngine;

public abstract class ManaCostModif : MonoBehaviour
{
	protected Player	_affected_player;
	protected int		_param;
	protected Character _source;

	public virtual void initialize(Player affected_player, int param, Character source)
	{
		_affected_player = affected_player;
		_param = param;
		_source = source;
	}

	public virtual void destroy()
	{
		GameObject.Destroy(this);
	}

	public abstract int[] getManaCostAfterModif(System.Object source, int[] mana_cost);
	public virtual void manaCostWasPayed(System.Object source, int[] mana_cost) {}
	public virtual void onTrigger(Trigger trigger) {}
}