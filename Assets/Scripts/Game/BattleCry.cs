using UnityEngine;

public abstract class BattleCry : Spell
{
	protected Character	_character;

	public Character Character { get { return _character; } }

	public void initialize(Character character, CardStats card_stats)
	{
		base.initialize(card_stats);
		_character = character;
	}
}