using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections; // Coroutines
using System.Collections.Generic;

public class Tile : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,
	IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	[SerializeField] private GameObject		_panel_character;
	[SerializeField] private GameObject[]	_panel_special_tiles = new GameObject[3];
	[SerializeField] private Color			_color_on_hover;
	[SerializeField] private Color			_color_hovering_valid_target;
	[SerializeField] private Color			_color_valid_target;
	[SerializeField] private Color			_color_unocuppied;
	[SerializeField] private Color			_color_unit;
	[SerializeField] private Color			_color_ennemy;
	[SerializeField] private Color			_color_allied_movement;
	[SerializeField] private Color			_color_ennemy_movement;

	private Image							_background_tile;
	private bool							_hovered = false;
	private e_highlight						_highlight = e_highlight.None;
	private Dictionary<e_highlight, Color>	_background_color = new Dictionary<e_highlight, Color>();

	public enum e_highlight
	{
		None,
		Allied_movement,
		Ennemy_movement,
		Valid_target
	}

	private Coord							_coord;
	private Character						_character;
	private SpecialTile[]					_special_tiles = new SpecialTile[3];
	private Player							_current_player;

	public Coord Coord { get { return _coord; } }

	public Character Character
	{
		get { return _character; }
		set
		{
			_character = value;
			if ( _character != null )
			{
				_character.transform.SetParent(_panel_character.transform, false);
				_character.Coord = _coord;
			}
			updateBackgroundColor();
		}
	}

	public void setCharacterAfterDeath(Character character)
	{
		StartCoroutine(coroutineSetCharacterAfterDeath(character));
	}

	private IEnumerator coroutineSetCharacterAfterDeath(Character character)
	{
		while ( _character != null )
			yield return null;
		Character = character;
	}

	private int getIndex(Player owner)
	{
		if (owner == null)
			return 0;
		else
			return owner.isPlayerOne ? 1 : 2;
	}

	public SpecialTile getSpecialTile(Player owner)
	{
		return _special_tiles[getIndex(owner)];
	}

	public SpecialTile setSpecialTile(string tile_prefab_path, Player owner)
	{
		loseSpecialTile(owner);
		_special_tiles[getIndex(owner)] = Tools.instantiateFromPath(tile_prefab_path).GetComponent<SpecialTile>();
		_special_tiles[getIndex(owner)].transform.SetParent(_panel_special_tiles[getIndex(owner)].transform, false);
		_special_tiles[getIndex(owner)].initialize(this, owner);
		return _special_tiles[getIndex(owner)];
	}

	public void assignSpecialTile(SpecialTile special_tile)
	{
		loseSpecialTile(special_tile.Owner);
		_special_tiles[getIndex(special_tile.Owner)] = special_tile;
		special_tile.transform.SetParent(_panel_special_tiles[getIndex(special_tile.Owner)].transform, false);
	}

	public void loseSpecialTile(Player owner, bool destroy_tile=true)
	{
		if ( _special_tiles[getIndex(owner)] != null )
		{
			_special_tiles[getIndex(owner)].outHover();
			if ( destroy_tile )
				GameObject.Destroy(_special_tiles[getIndex(owner)].gameObject);
			_special_tiles[getIndex(owner)] = null;
		}
	}

	private void destroyAllSpecialTiles()
	{
		for (int i = 0; i < _special_tiles.Length; ++i)
		{
			if ( _special_tiles[i] != null )
			{
				GameObject.Destroy(_special_tiles[i].gameObject);
				_special_tiles[i] = null;
			}
		}
	}

	public void initialize(Coord coord)
	{
		_coord = coord;
	}

	private void Awake()
	{
		_background_tile = this.gameObject.GetComponent<Image>();

		_background_color[e_highlight.Valid_target] = _color_valid_target;
		_background_color[e_highlight.Allied_movement] = _color_allied_movement;
		_background_color[e_highlight.Ennemy_movement] = _color_ennemy_movement;
	}

	private void updateBackgroundColor()
	{
		if ( _hovered == true )
		{
			if ( _highlight == e_highlight.Valid_target )
				_background_tile.color = _color_hovering_valid_target;
			else
				_background_tile.color = _color_on_hover;
		}
		else if ( _highlight != e_highlight.None )
			_background_tile.color = _background_color[_highlight];
		else if ( _character != null )
			_background_tile.color = _character.Owner == _current_player ? _color_unit : _color_ennemy;
		else
			_background_tile.color = _color_unocuppied;
	}
	public void startTurn(Player current_player)
	{
		_current_player = current_player;
		for (int i = 0; i < _special_tiles.Length; ++i)
			if ( _special_tiles[i] != null )
				_special_tiles[i].startTurn(current_player);
		updateBackgroundColor();
	}

	public void onTrigger(Trigger trigger)
	{
		for (int i = 0; i < _special_tiles.Length; ++i)
			if ( _special_tiles[i] != null )
				_special_tiles[i].onTrigger(trigger);
	}

	public void dispell()
	{
		Trigger.activate(Trigger.e_type.Dispell, tile: this);
		destroyAllSpecialTiles();
		if ( _character != null )
			_character.dispell();
	}

	public void setHighlightType(e_highlight highlight)
	{
		_highlight = highlight;
		updateBackgroundColor();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_hovered = true;
		if ( _character != null )
			_character.onHover(Board.instance.CardHoverPanel);
		for (int i = 0; i < _special_tiles.Length; ++i)
			if ( _special_tiles[i] != null )
				_special_tiles[i].onHover(Board.instance.SpecialTileHoverPanels[i]);

		updateBackgroundColor();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_hovered = false;
		if ( _character != null )
			_character.outHover();
		for (int i = 0; i < _special_tiles.Length; ++i)
			if ( _special_tiles[i] != null )
				_special_tiles[i].outHover();

		updateBackgroundColor();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
		{
			if ( _character != null && _character.Owner == _current_player )
			{
				List<ActivatedAbility> activated_abilities = _character.ActivatedAbilities;
				if ( activated_abilities.Count == 1 )
					activated_abilities[0].activate();
				else if ( activated_abilities.Count > 1)
					ActivatedAbilityPanel.instance.doActivatedAbility(activated_abilities);
			}
		}

		if ( eventData.button == PointerEventData.InputButton.Right && _character != null )
			RelatedCardsPanel.instance.openRelatedCardsPanel(_character.CardStats.RelatedCards,
				                                             _character.CardStats.RelatedTiles);
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		TileHighlightManager.onBeginDragCharacter(_character);
	}

	public void OnDrag(PointerEventData eventData) {}

	public void OnEndDrag(PointerEventData eventData)
	{
		if ( _character == null )
			return;

		List<RaycastResult> hits = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventData, hits);
		foreach (RaycastResult hit in hits)
		{
			Tile tile = hit.gameObject.GetComponent<Tile>();
			if ( tile != null )
			{
				Board.targetTile(_coord, tile.Coord);
				break;
			}
		}
		TileHighlightManager.onEndDragCharacter();
	}
}