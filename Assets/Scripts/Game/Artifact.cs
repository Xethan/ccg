using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class Artifact : Card
{
	protected abstract int BaseDurability { get; }
	protected abstract string Name { get; }
	protected abstract string Description { get; }

	private Character		_equipped_character;
	private int				_durability;
	private int				_max_durability;
	private List<Modifier>	_applied_modifiers;
	private ManaCostUI		_mana_cost_ui;
	private ArtifactUI		_artifact_ui;

	public List<Modifier> Modifiers { get { return _applied_modifiers; } }

	public abstract bool isTargetValid(Tile target);
	protected abstract List<Modifier> createModifiers(Character equipped_character);

	protected override void onCreate()
	{
		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_card_hover_ui.PanelManaCostUI.transform, false);
		_mana_cost_ui.updateUI(_mana_cost);
	}

	public override bool canPlay(Tile tile)
	{
		return isTargetValid(tile) && _owner.ManaReserve.canPayManaCost(this, _mana_cost.raw_cost);
	}

	protected override bool _play(Tile tile)
	{
		if ( isTargetValid(tile) && _owner.ManaReserve.payManaCost(this, _mana_cost.raw_cost) )
		{
			_equipped_character = tile.Character;
			_durability = this.BaseDurability;
			_max_durability = this.BaseDurability;
			_applied_modifiers = createModifiers(_equipped_character);
			_artifact_ui = Tools.instantiateFromPath("Game UI/Artifact UI").GetComponent<ArtifactUI>();
			_artifact_ui.transform.SetParent(this.gameObject.transform, false);
			LayoutElement layout_element = this.gameObject.AddComponent<LayoutElement>() as LayoutElement;
			layout_element.minHeight = 40;
			updateUI();
			_equipped_character.receiveArtifact(this, _artifact_ui);
			Trigger.activate(Trigger.e_type.Artifact, source:this, tile:tile);
			return true;
		}
		return false;
	}

	protected void updateUI()
	{
		_artifact_ui.updateUI(this.Name, _durability, _max_durability, this.Description);
	}

	public void receiveDamage(int damage)
	{
		_durability -= damage;
		if ( _durability <= 0 )
		{
			foreach (Modifier modifier in _applied_modifiers)
	 			modifier.destroy();
			GameObject.Destroy(_artifact_ui.gameObject);
			_equipped_character.loseArtifact(this);
			GameObject.Destroy(this.gameObject);
		}
		updateUI();
	}

	public void onTrigger(Trigger trigger)
	{
		foreach (Modifier modifier in _applied_modifiers)
			modifier.onTrigger(trigger);
	}
}