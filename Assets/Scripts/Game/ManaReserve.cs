using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class ManaReserve : MonoBehaviour
{
	private int[]							_mana = new int[Factions.nb_factions];
	private int[]							_mana_max = new int[Factions.nb_factions];
	private int								_nb_consumes_left = 0;
	private int 	                        _mana_reserve = 0;
	private List<ManaCostModif>				_mana_cost_modifs = new List<ManaCostModif>();

	[SerializeField] private FactionManaUI	_prefab_faction_mana_ui;
	[SerializeField] private GameObject		_green_dot_can_consume;
	[SerializeField] private GameObject		_panel_faction_mana_ui;
	[SerializeField] private GameObject		_panel_neutral_mana_ui;

	private FactionManaUI[]					_factions_mana_ui = new FactionManaUI[Factions.nb_factions];
	private FactionManaUI					_neutral_faction_ui;
	private FactionManaUI					_reserve_faction_ui;

	private Player							_player;
	private int								_general_1_faction;
	private int								_general_2_faction;
	private int								_neutral_faction = 0;

	public int[] ManaMax { get { return _mana_max; } }

	public void initialize(Player player, Factions general_1_faction, Factions general_2_faction, IEnumerable<Card> deck)
	{
		_player = player;
		_general_1_faction = general_1_faction.FirstFaction;
		_general_2_faction = general_2_faction.FirstFaction;

		_reserve_faction_ui = instanciateFactionManaUI(_neutral_faction, _panel_neutral_mana_ui);
		_factions_mana_ui[_neutral_faction] = instanciateFactionManaUI(_neutral_faction, _panel_neutral_mana_ui);
		_factions_mana_ui[_general_1_faction] = instanciateFactionManaUI(_general_1_faction, _panel_faction_mana_ui);
		if ( _factions_mana_ui[_general_2_faction] == null )
			_factions_mana_ui[_general_2_faction] = instanciateFactionManaUI(_general_2_faction, _panel_faction_mana_ui);
		foreach (Card card in deck)
		{
			if ( _factions_mana_ui[card.Factions.FirstFaction] == null )
				_factions_mana_ui[card.Factions.FirstFaction] = instanciateFactionManaUI(card.Factions.FirstFaction, _panel_faction_mana_ui);
			if ( _factions_mana_ui[card.Factions.SecondFaction] == null )
				_factions_mana_ui[card.Factions.SecondFaction] = instanciateFactionManaUI(card.Factions.SecondFaction, _panel_faction_mana_ui);
		}

		increaseMana(_general_1_faction);
		increaseMana(_general_2_faction);
	}

	private FactionManaUI instanciateFactionManaUI(int faction, GameObject parent_panel)
	{
		FactionManaUI faction_mana_ui = GameObject.Instantiate(_prefab_faction_mana_ui) as FactionManaUI;
		faction_mana_ui.initialize(this, faction);
		faction_mana_ui.transform.SetParent(parent_panel.transform, false);
		return faction_mana_ui;
	}

	private void updateUI()
	{
		_reserve_faction_ui.updateUI(_mana_reserve, _mana_reserve);
		for (int i = 0; i < _factions_mana_ui.Length; ++i)
			if ( _factions_mana_ui[i] != null )
				_factions_mana_ui[i].updateUI(_mana[i], _mana_max[i]);

		_green_dot_can_consume.SetActive(_nb_consumes_left > 0);
	}

	public void startTurn()
	{
		Array.Copy(_mana_max, _mana, length:Factions.nb_factions);
		updateUI();
	}
	public void endTurn()
	{
		receiveManaInReserve(_mana[Factions.neutral]);
		_nb_consumes_left = 1;
		updateUI();
	}

	public bool canPayManaCost(System.Object source, int[] requirements)
	{
		ManaCost mana_cost = getManaCostAfterModifs(source, requirements);

		return ( canPayColoredManaCost(mana_cost.raw_cost)
			     && mana_cost.raw_cost[Factions.neutral] <= _mana_max[Factions.neutral]
			     && mana_cost.raw_cost[Factions.neutral] <= _mana[Factions.neutral] + _mana_reserve );
	}

	private bool canPayColoredManaCost(int[] requirements)
	{
		for (int i = 1; i < requirements.Length; ++i)
			if ( requirements[i] > _mana[i] )
				return false;

		return true;
	}

	public bool payManaCost(System.Object source, int[] requirements)
	{
		if ( !canPayManaCost(source, requirements) )
				return false;

		int[] mana_cost = getManaCostAfterModifs(source, requirements).raw_cost;

		int mana_to_use_from_reserve = Math.Max(mana_cost[Factions.neutral] -_mana[Factions.neutral], 0);
		_mana_reserve -= mana_to_use_from_reserve;
		mana_cost[Factions.neutral] -= mana_to_use_from_reserve;
		for (int i = 0; i < mana_cost.Length; ++i)
			_mana[i] -= mana_cost[i];
		updateUI();

		/* Iterating over the list copied to an array so as not to skip a mana_cost_modif if it destroys itself */
		foreach (ManaCostModif mana_cost_modif in _mana_cost_modifs.ToArray())
			mana_cost_modif.manaCostWasPayed(source, mana_cost);

		return true;
	}

	public void onTrigger(Trigger trigger)
	{
		/* Iterating over the list copied to an array so as not to skip a mana_cost_modif if it destroys itself */
		foreach (ManaCostModif mana_cost_modif in _mana_cost_modifs.ToArray())
			mana_cost_modif.onTrigger(trigger);
	}

	public void onBeginDragCard(Card card)
	{
		if ( _nb_consumes_left > 0 )
			for (int i = 0; i < _factions_mana_ui.Length; ++i)
				if ( _factions_mana_ui[i] != null )
					_factions_mana_ui[i].onBeginDragCard(card);
	}

	public void onEndDragCard()
	{
		for (int i = 0; i < _factions_mana_ui.Length; ++i)
			if ( _factions_mana_ui[i] != null )
				_factions_mana_ui[i].onEndDragCard();
	}

	public bool cardDraggedToFactionManaUI(Factions card_factions, int mana_ui_faction)
	{
		if ( card_factions.isFromFaction(mana_ui_faction) )
			return consume(mana_ui_faction);
		return false;
	}

	private bool consume(int faction)
	{
		if ( _nb_consumes_left <= 0 )
			return false;

		_nb_consumes_left -= 1;
		increaseMana(faction);
		Trigger.activate(Trigger.e_type.Consume);
		return true;
	}

	public void changeNbConsumesLeft(int nb_consumes_change)
	{
		_nb_consumes_left += nb_consumes_change;
		updateUI();
	}

	public void increaseMana(int faction)
	{
		if ( faction != Factions.neutral )
		{
			_mana[Factions.neutral] += 1;
			_mana_max[Factions.neutral] += 1;
		}
		_mana[faction] += 1;
		_mana_max[faction] += 1;
		updateUI();
	}

	public void increaseManaMax(int faction)
	{
		if ( faction != Factions.neutral )
			_mana_max[Factions.neutral] += 1;
		_mana_max[faction] += 1;
		updateUI();
	}

	public void receiveManaInReserve(int amount)
	{
		_mana_reserve += amount;
		updateUI();
	}

	public void increaseManaUntilEndOfTurn(int[] increase)
	{
		for (int i = 0; i < _mana.Length && i < increase.Length; ++i)
			_mana[i] += increase[i];
		updateUI();
	}

	public ManaCostModif receiveManaCostModif(System.Type type, int param=0, Character source=null)
	{
		ManaCostModif new_mana_cost_modif = this.gameObject.AddComponent(type) as ManaCostModif;
		new_mana_cost_modif.initialize(_player, param, source);
		_mana_cost_modifs.Add(new_mana_cost_modif);
		_player.updateHandManaCostUI();
		return new_mana_cost_modif;
	}

	public void loseManaCostModif(ManaCostModif mana_cost_modif_to_lose)
	{
		mana_cost_modif_to_lose.destroy();
		_mana_cost_modifs.Remove(mana_cost_modif_to_lose);
		_player.updateHandManaCostUI();
	}

	public ManaCost getManaCostAfterModifs(System.Object source, int[] mana_cost)
	{
		foreach (ManaCostModif mana_cost_modif in _mana_cost_modifs)
			mana_cost = mana_cost_modif.getManaCostAfterModif(source, mana_cost);
		return new ManaCost(mana_cost);
	}
}