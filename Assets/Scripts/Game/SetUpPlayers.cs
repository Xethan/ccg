using UnityEngine;
using System; // Math
using System.Collections.Generic;

class SetUpPlayers
{
    public static (Player, Player) instantiateScenarioPlayers(Scenario scenario)
    {
        Player player_1 = instantiatePlayer(
            deck:scenario.cards_prefab_path_in_deck_player_1,
            combat_tricks_prefab_path:new List<string>(),
            general_1_prefab_path:scenario.generals_player_1[0].prefab_path,
            general_2_prefab_path:scenario.generals_player_1[1].prefab_path,
            coord_general_1:scenario.generals_player_1[0].coord,
            coord_general_2:scenario.generals_player_1[1].coord,
            is_player_one:true
        );
        Scenario.initializeStartingHand(player_1, scenario.cards_in_hand_prefab_path_player_1);
        Scenario.addManaToReserve(player_1.ManaReserve, scenario.additional_mana_player_1);

        Player player_2 = instantiatePlayer(
            deck:scenario.cards_prefab_path_in_deck_player_2,
            combat_tricks_prefab_path:new List<string>(),
            general_1_prefab_path:scenario.generals_player_2[0].prefab_path,
            general_2_prefab_path:scenario.generals_player_2[1].prefab_path,
            coord_general_1:scenario.generals_player_2[0].coord,
            coord_general_2:scenario.generals_player_2[1].coord,
            is_player_one:false
        );
        Scenario.initializeStartingHand(player_2, scenario.cards_in_hand_prefab_path_player_2);
        Scenario.addManaToReserve(player_2.ManaReserve, scenario.additional_mana_player_2);

        return (player_1, player_2);
    }

    // This function is separated from `instantiateScenarioPlayers` because `Board._players` needs to be set before
    public static void summonScenarioCharacters(Player owner, List<CharacterScenario> characters)
    {
        foreach (CharacterScenario character_scenario in characters)
        {
            Character character = Tools.instantiateCardFromPath<Character>(character_scenario.prefab_path, owner);
            character.summon(Board.getTile(character_scenario.coord));
        }
    }

    public static (Player, Player) instantiatePlayers()
    {
        int x_player_1 = 0;
        int x_player_2 = Board.nb_columns - 1;
        int y_general_1 = Mathf.RoundToInt((float)(Board.nb_lines / 2)) - 1;
        int y_general_2 = Mathf.RoundToInt((float)(Board.nb_lines / 2)) + 1;

        SerializableDeck deck_player_1 = DeckSelection.player_decks[0];
        SerializableDeck deck_player_2 = DeckSelection.player_decks[1];

        return (
            instantiatePlayer(
                deck:deck_player_1._cards_prefab_path,
                combat_tricks_prefab_path:deck_player_1._combat_tricks_prefab_path,
                general_1_prefab_path:deck_player_1._generals_prefab_path[0],
                general_2_prefab_path:deck_player_1._generals_prefab_path[1],
                coord_general_1:new Coord(x_player_1, y_general_1),
                coord_general_2:new Coord(x_player_1, y_general_2),
                is_player_one:true
            ),
            instantiatePlayer(
                deck:deck_player_2._cards_prefab_path,
                combat_tricks_prefab_path:deck_player_2._combat_tricks_prefab_path,
                general_1_prefab_path:deck_player_2._generals_prefab_path[0],
                general_2_prefab_path:deck_player_2._generals_prefab_path[1],
                coord_general_1:new Coord(x_player_2, y_general_1),
                coord_general_2:new Coord(x_player_2, y_general_2),
                is_player_one:false
            )
        );
    }

    private static Player instantiatePlayer(
        Dictionary<string, int> deck,
        List<string> combat_tricks_prefab_path,
        string general_1_prefab_path,
        string general_2_prefab_path,
        Coord coord_general_1,
        Coord coord_general_2,
        bool is_player_one
    ) {
        Player player = new GameObject().AddComponent(typeof(Player)) as Player;
        // player must be initialized before generals are instantiated (for flipImage in onSummon)
        player.initialize(deck, is_player_one);
        General general_1 = instantiateGeneral(player, general_1_prefab_path, coord_general_1);
        General general_2 = instantiateGeneral(player, general_2_prefab_path, coord_general_2);
        player.instantiatePanelBattleCries(general_1.BattleCry, general_2.BattleCry);
        player.instantiatePanelCombatTricks(combat_tricks_prefab_path);
        player.instantiateManaReserve(general_1.Factions, general_2.Factions);
        return player;
    }

    private static General instantiateGeneral(Player player, string prefab_path, Coord coord)
    {
        General general = Tools.instantiateCardFromPath<General>(prefab_path, owner:player);
        general.summon(Board.getTile(coord));
        return general;
    }
}