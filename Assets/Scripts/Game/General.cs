using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class General : Character
{
	private int				_xp = 0;
	protected int			_level = 1;
	private GeneralHoverUI	_general_hover_ui;
	private BattleCry		_battle_cry;

	protected abstract int[]	XpNeededToLevelUp { get; }
	protected abstract string BattleCryPrefabPath { get; }

	public int Level { get { return _level; } }
	public BattleCry BattleCry { get { return _battle_cry; } }

	protected virtual void onLevel2() {}

	public override void initialize(CardStats general_card_stats)
	{
		base.initialize(general_card_stats);
		_owner.initializeGeneral(this);
		instantiateGeneralHoverUI();

		string card_class_name = BattleCryPrefabPath.Split(new System.Char [] {'/'})[1].Replace(" ", "");
		CardStats card_stats = Tools.instantiateFromPath(BattleCryPrefabPath + " Stats").GetComponent<CardStats>();
		_battle_cry = Tools.instantiateFromPath("Base Card").AddComponent(System.Type.GetType(card_class_name)) as BattleCry;
		_battle_cry.Owner = _owner;
		_battle_cry.initialize(this, card_stats);
	}

	private void instantiateGeneralHoverUI()
	{
		_general_hover_ui = Tools.instantiateFromPath("Game UI/General Hover UI").GetComponent<GeneralHoverUI>();
		_general_hover_ui.transform.SetParent(_card_hover_ui.transform, false);
		_general_hover_ui.updateUI(_level, _xp, XpNeededToLevelUp);
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == this )
			SceneManager.LoadScene("Scenes/Main Menu");
		base.onTrigger(trigger);
	}

	protected void gainXp(int amount)
	{
		if ( amount <= 0 )
			return;

		_xp += amount;
		XpUI xp_ui = Tools.instantiateFromPath("Game UI/Xp UI").GetComponent<XpUI>();
		xp_ui.initialize(amount);
		xp_ui.gameObject.transform.SetParent(Board.getTile(_coord).gameObject.transform, false);
		Trigger.activate(Trigger.e_type.Xp_gain, receiver : this, amount : amount);
		if ( _xp >= XpNeededToLevelUp[_level] )
			levelUp();
		_general_hover_ui.updateUI(_level, _xp, XpNeededToLevelUp);
	}

	protected void levelUp()
	{
		_level += 1;
		if ( _level == 2 )
		{
			onLevel2();
			_image.sprite = Tools.instantiateSpriteFromPath("Level 2 Generals/" + _name);
			_card_hover_ui.updateUI(_image.sprite, _factions, _name, _text);
		}
		else if ( _level == 3 )
			SceneManager.LoadScene("Scenes/Main Menu");
	}
}