using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class ActivatedAbilityPanel : MonoBehaviour
{
	[SerializeField] private GameObject				_activated_ability_slots_panel;
	[SerializeField] private ActivatedAbilitySlot	_activated_ability_slot_prefab;

	public static ActivatedAbilityPanel				instance;
	private List<ActivatedAbilitySlot>				_activated_ability_slots = new List<ActivatedAbilitySlot>();

	void Awake()
	{
		instance = this;
		this.gameObject.SetActive(false);
	}

	public void doActivatedAbility(List<ActivatedAbility> activated_abilities)
	{
		int i = 0;
		for (; i < activated_abilities.Count; ++i)
		{
			if (i >= _activated_ability_slots.Count)
			{
				_activated_ability_slots.Add(GameObject.Instantiate(_activated_ability_slot_prefab) as ActivatedAbilitySlot);
				_activated_ability_slots[i].transform.SetParent(_activated_ability_slots_panel.transform, false);
			}
			else
				_activated_ability_slots[i].gameObject.SetActive(true);
			_activated_ability_slots[i].initialize(activated_abilities[i]);
		}
		while (i < _activated_ability_slots.Count)
		{
			GameObject.Destroy(_activated_ability_slots[i].gameObject);
			_activated_ability_slots.RemoveAt(i);
		}

		this.gameObject.SetActive(true);
		this.transform.SetAsLastSibling();
	}

	public void close()
	{
		this.gameObject.SetActive(false);
	}
}