using UnityEngine;
using UnityEngine.UI;

public abstract class SpecialTile : GameElement
{
	protected Tile					_tile;
	protected SpecialTileHoverUI	_special_tile_hover_ui;

	protected virtual string Name { get { return "null"; } }
	protected virtual string Description { get { return "null"; } }

	public Tile Tile { set { _tile = value; } }

	public void initialize(Tile tile, Player owner)
	{
		_tile = tile;
		_owner = owner;
		instantiateHoverUI();
	}

	public virtual void startTurn(Player current_player) {}
	public virtual void onTrigger(Trigger trigger) {}

	public void instantiateHoverUI()
	{
		_special_tile_hover_ui = Tools.instantiateFromPath("Game UI/Special Tile Hover UI").GetComponent<SpecialTileHoverUI>();
		_special_tile_hover_ui.transform.SetParent(this.transform, false);
		_special_tile_hover_ui.gameObject.SetActive(false);
		updateUI();
	}

	protected void updateUI()
	{
		_special_tile_hover_ui.updateUI(this.gameObject.GetComponent<Image>().sprite, Name, Description);
	}

	public virtual void onHover(GameObject special_tile_hover_panel)
	{
		_special_tile_hover_ui.transform.SetParent(special_tile_hover_panel.transform, false);
		_special_tile_hover_ui.gameObject.SetActive(true);
	}

	public virtual void outHover()
	{
		_special_tile_hover_ui.transform.SetParent(this.transform, false);
		_special_tile_hover_ui.gameObject.SetActive(false);
	}
}