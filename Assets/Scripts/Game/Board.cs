using UnityEngine;
using UnityEngine.UI;
using System; // Math
using System.Collections; // Coroutines
using System.Collections.Generic;

public class Board : MonoBehaviour
{
	public static Board							instance;
	public static bool							game_started = false;
	public static int							turn_nb = 1;

	public enum e_daytime { Sun, Moon };
	public static e_daytime						daytime = e_daytime.Moon;

	public const int							nb_columns = 9;
	public const int							nb_lines = 7;

	public const int							nb_cards_at_start = 5;
	public const int							max_cards_in_hand = 8;

	private const string						_public_gift_tile_prefab_path = "Tiles/Public Gift";
	[SerializeField] private Tile				_tile_prefab;

	[SerializeField] private GameObject[]		_special_tile_hover_panels;
	[SerializeField] private GameObject			_card_hover_panel;
	[SerializeField] private GameObject			_end_turn_button;

	public static Tile[]						_board = new Tile[Board.nb_columns * Board.nb_lines];
	public static List<GlobalCharacterModifier>	_global_character_modifiers = new List<GlobalCharacterModifier>();

	private static int							_current_player = 0;
	private static Player[]						_players = new Player[2];

	public GameObject[] SpecialTileHoverPanels { get { return _special_tile_hover_panels; } }
	public GameObject CardHoverPanel { get { return _card_hover_panel; } }

	void Awake()
	{
		instance = this;
		StartCoroutine(setUp());
	}

	private void instantiateBoard()
    {
        for (int i = 0; i < _board.Length; ++i)
        {
            _board[i] = GameObject.Instantiate(_tile_prefab) as Tile;
            _board[i].gameObject.name = "Tile (" + (i / nb_lines) + ", " + (i % nb_lines) + ")";
            _board[i].transform.SetParent(this.transform, false);
            _board[i].initialize(new Coord(i));
            _board[i].name = "Tile " + i;
        }
        _board[Coord.raw(4, 2)].setSpecialTile(_public_gift_tile_prefab_path, owner:null);
        _board[Coord.raw(4, 4)].setSpecialTile(_public_gift_tile_prefab_path, owner:null);
        _board[Coord.raw(5, 3)].setSpecialTile(_public_gift_tile_prefab_path, owner:null);
    }

	private IEnumerator setUp()
	{
        instantiateBoard();

		if ( Test.scenario == null )
		{
        	(_players[0], _players[1]) = SetUpPlayers.instantiatePlayers();
	        yield return null;
	        _players[0].doMulligan(); // Once the mulligan is done, mulliganDone() will be called
	    }
		else
		{
			(_players[0], _players[1]) = SetUpPlayers.instantiateScenarioPlayers(Test.scenario);
			SetUpPlayers.summonScenarioCharacters(_players[0], Test.scenario.characters_player_1);
			SetUpPlayers.summonScenarioCharacters(_players[1], Test.scenario.characters_player_2);
	        yield return null;
	        startGame();
		}

		// This needs to be done at least a frame after `instantiateBoard()`
        Tools.initializeGridLayoutParameters(this.gameObject, Board.nb_columns, Board.nb_lines, 1.0f, 0.99f, 0.01f);
	}

	public static IEnumerator mulliganDone(Player player)
	{
		if ( player == _players[0] )
		{
			yield return new WaitForSeconds(0.5f);
			_players[1].doMulligan();
		}
		else
			instance.startGame();
	}

	// not static because it interacts with end_turn_button
	private void startGame()
	{
		_end_turn_button.SetActive(true);
		game_started = true;
		startTurn();
		_players[1].ManaReserve.receiveManaInReserve(3);
	}

	private void startTurn()
	{
		Trigger.activate(Trigger.e_type.Start_turn, player : _players[_current_player]);
		foreach (Tile tile in _board)
			tile.startTurn(_players[_current_player]);
	}

	public void endTurn()
	{
		Trigger.activate(Trigger.e_type.End_turn, player : _players[_current_player]);
		_current_player = 1 - _current_player;

		StartCoroutine(waitBeforeEndingTurn());

		if ( _players[_current_player].isPlayerOne )
		{
			Board.turn_nb += 1;
			changeDaytime();
		}

		startTurn();
	}

	public static void changeDaytime()
	{
		if ( Board.daytime == Board.e_daytime.Sun )
		{
			Board.daytime = Board.e_daytime.Moon;
			Trigger.activate(Trigger.e_type.Moonrise);
		}
		else
		{
			Board.daytime = Board.e_daytime.Sun;
			Trigger.activate(Trigger.e_type.Sunrise);
		}
	}

	// Stopgap to avoid double clicks
	private IEnumerator waitBeforeEndingTurn() { yield return new WaitForSeconds(0.5f); }

	public static bool isSummoningValid(Character character, Tile tile)
	{
		if ( character.Owner != getCurrentPlayer() || tile.Character != null )
			return false;
		return isThereAlliedCharacterInSquareRange(getCurrentPlayer(), tile.Coord, 1);
	}

	public static void targetTile(Coord coord, Coord target_coord)
	{
		Character character = _board[coord.RawCoord].Character;
		Character target = _board[target_coord.RawCoord].Character;

		if ( character.Owner != _players[_current_player] )
			return;
		if ( isAttackValid(character, target) )
			character.attack(target);
		else if ( isMovementValid(character, target_coord) )
			character.move(_board[coord.RawCoord], _board[target_coord.RawCoord]);
	}

	public static bool isAttackValid(Character attacker, Character target)
	{
		return target != null
				&& attacker.Owner != target.Owner
				&& attacker.Coord.squareDistance(target.Coord) <= attacker.Range
				&& attacker.isFlagUp(Character.e_flags.Can_attack)
				&& (isCharacterProvoked(attacker) == false
					|| target.isFlagUp(Character.e_flags.Provoke))
				&& (target.isFlagUp(Character.e_flags.StrategicUnit) == false
					|| isCharacterGuarded(attacker) == false);
	}

	public static bool isSymmetricTeleportValid(Coord coord_target, Coord symmetry_origin)
	{
		Coord coord_after_teleport = symmetry_origin + symmetry_origin - coord_target;

		return coord_target != symmetry_origin
		    && symmetry_origin.isInsideBoard()
			&& Board.getCharacter(symmetry_origin) != null
			&& coord_after_teleport.isInsideBoard()
			&& !(Board.getCharacter(coord_after_teleport) is General);
	}

	private static bool isMovementValid(Character character, Coord target_coord)
	{
		return character.isFlagUp(Character.e_flags.Can_move)
				&& Pathing.findLengthPathToTiles(character)[target_coord.RawCoord] != 0
				&& isCharacterTackled(character) == false;
	}

	public static bool isThereAlliedCharacterInRange(Player player, Coord center_coord, int max_distance)
	{
		for (int radius = 1; radius <= max_distance; ++radius)
			foreach (Coord test_coord in center_coord.createSafeCenteredCircleCoords(radius))
				if ( isCharacterAllied(player, getCharacter(test_coord)) )
					return true;
		return false;
	}

	public static bool isThereAlliedCharacterInSquareRange(Player player, Coord center_coord, int max_distance)
	{
		for (int radius = 1; radius <= max_distance; ++radius)
			foreach (Coord test_coord in center_coord.createSafeCenteredSquareCoords(radius))
				if ( isCharacterAllied(player, getCharacter(test_coord)) )
					return true;
		return false;
	}

	public static bool isCharacterIsolated(Character character)
	{
		return !isThereAlliedCharacterInSquareRange(character.Owner, character.Coord, max_distance: 1);
	}

	public static bool isCharacterProvoked(Character character)
	{
		foreach (Coord test_coord in character.Coord.createSafeCenteredSquareCoords(radius: 1))
		{
			if ( isCharacterEnnemy(character.Owner, getCharacter(test_coord))
				 && _board[test_coord.RawCoord].Character.isFlagUp(Character.e_flags.Provoke) )
				return true;
		}
		return false;
	}

	public static bool isCharacterTackled(Character character)
	{
		foreach (Coord test_coord in character.Coord.createSafeCenteredSquareCoords(radius: 1))
		{
			if ( isCharacterEnnemy(character.Owner, getCharacter(test_coord))
				 && _board[test_coord.RawCoord].Character.isFlagUp(Character.e_flags.Tackle) )
				return true;
		}
		return false;
	}

	public static bool isCharacterGuarded(Character attacker)
	{
		foreach (Coord test_coord in attacker.Coord.createSafeCenteredSquareCoords(radius: 1))
		{
			if ( isCharacterEnnemy(attacker.Owner, getCharacter(test_coord))
				 && !getCharacter(test_coord).isFlagUp(Character.e_flags.StrategicUnit) )
				return true;
		}
		return false;
	}

	public static bool isCharacterAllied(Player player, Character character)
	{
		return character != null && character.Owner == player;
	}

	public static bool isCharacterEnnemy(Player player, Character character)
	{
		return character != null && character.Owner != player;
	}

	public static bool isEnnemyGeneral(Player player, Character character)
	{
		return Board.isCharacterEnnemy(player, character) && character is General;
	}

	public static bool isMyTurn(Player player) { return _players[_current_player] == player; }

	public static Player getEnnemyPlayer() { return _players[1 - _current_player]; } // Todo: Add argument player and get the other one
	public static Player getCurrentPlayer() { return _players[_current_player]; }

	public static Tile        getTile(Coord coord) { return _board[coord.RawCoord]; }
	public static SpecialTile getSpecialTile(Coord coord, Player player) { return _board[coord.RawCoord].getSpecialTile(player); }
	public static Character   getCharacter(Coord coord) { return _board[coord.RawCoord].Character; }

	public static IEnumerable<Character> getCharactersInRange(Func<int, IEnumerable<Coord>> getSafeCoords, int radius)
	{
		foreach (Coord coord in getSafeCoords(radius))
		{
			Character character = Board.getCharacter(coord);
			if ( character )
				yield return character;
		}
	}
}