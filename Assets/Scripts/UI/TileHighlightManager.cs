using UnityEngine;
using System;
using System.Collections.Generic;

public static class TileHighlightManager
{
	private static Character		_hovered;
	private static Character		_dragged;
	private static Predicate<Tile>	_func_is_target_valid;

	public static void onHoverCharacter(Character character)
	{
		_hovered = character;
		updateTilesHighlight();
	}

	public static void outHoverCharacter()
	{
		_hovered = null;
		updateTilesHighlight();
	}

	public static void onBeginDragCharacter(Character character)
	{
		_dragged = character;
		updateTilesHighlight();
	}

	public static void onEndDragCharacter()
	{
		_dragged = null;
		updateTilesHighlight();
	}

	public static void startHighlightingValidTargets(Predicate<Tile> func_is_target_valid)
	{
		_func_is_target_valid = func_is_target_valid;
		updateTilesHighlight();
	}

	public static void stopHighlightingValidTargets()
	{
		_func_is_target_valid = null;
		updateTilesHighlight();
	}

	private static void updateTilesHighlight()
	{
		int[] length_path_to_tiles = new int[Board.nb_columns * Board.nb_lines];

		if ( _func_is_target_valid == null )
		{
			if ( _dragged != null)
				length_path_to_tiles = Pathing.findLengthPathToTiles(_dragged);
			else if ( _hovered != null )
				length_path_to_tiles = Pathing.findLengthPathToTiles(_hovered);
		}

		foreach (Tile tile in Board._board)
		{
			tile.setHighlightType(Tile.e_highlight.None);

			if ( _func_is_target_valid != null && _func_is_target_valid(tile) == true )
				tile.setHighlightType(Tile.e_highlight.Valid_target);
			else if ( _dragged != null )
				setHighlightUsingDraggedCharacter(tile, length_path_to_tiles);
			else if ( _hovered != null )
				setHighlightUsingHoveredCharacter(tile, length_path_to_tiles);
		}
	}

	private static void setHighlightUsingDraggedCharacter(Tile tile, int[] length_path_to_tiles)
	{
		if ( Board.isCharacterAllied(Board.getCurrentPlayer(), _dragged) == false )
			return;

		if ( _dragged.isFlagUp(Character.e_flags.Can_move)
			&& length_path_to_tiles[tile.Coord.RawCoord] > 0 )
		{
			tile.setHighlightType(Tile.e_highlight.Allied_movement);
		}
		if ( Board.isAttackValid(_dragged, tile.Character) )
			tile.setHighlightType(Tile.e_highlight.Valid_target);
	}

	private static void setHighlightUsingHoveredCharacter(Tile tile, int[] length_path_to_tiles)
	{
		if ( ( _hovered.isFlagUp(Character.e_flags.Can_move)
				|| Board.isCharacterAllied(Board.getCurrentPlayer(), _hovered) == false )
			&& length_path_to_tiles[tile.Coord.RawCoord] > 0 )
		{
			if ( Board.isCharacterAllied(Board.getCurrentPlayer(), _hovered) )
				tile.setHighlightType(Tile.e_highlight.Allied_movement);
			else
				tile.setHighlightType(Tile.e_highlight.Ennemy_movement);
		}
	}
}