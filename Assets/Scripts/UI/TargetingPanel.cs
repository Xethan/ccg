using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class TargetingPanel : MonoBehaviour, IPointerClickHandler
{
	public static TargetingPanel	_instance;
	private Predicate<GameObject>	_is_target_valid;
	private Action<GameObject>		_do_effect;

	void Awake()
	{
		_instance = this;
		this.gameObject.SetActive(false);
	}

	public static void doTargetingAbility(Predicate<GameObject> is_target_valid, Action<GameObject> do_effect)
	{
		if ( Board.game_started == false )
			return;  // Useful to ignore targeting abilities on summon during test setups

		EventQueue.instance.newEvent(_instance._doTargetingAbility,
									 new Dictionary<string, object> {
									 {"is_target_valid", is_target_valid},
									 {"do_effect", do_effect}
								 });
	}

	private void _doTargetingAbility(Dictionary<string, object> kwargs)
	{
		_is_target_valid = kwargs["is_target_valid"] as Predicate<GameObject>;
		_do_effect = kwargs["do_effect"] as Action<GameObject>;

		_instance.gameObject.SetActive(true);
		_instance.transform.SetAsLastSibling();
		Predicate<Tile> is_target_tile_valid = (Tile tile) =>
		{
			return _is_target_valid(tile.gameObject);
		};
		TileHighlightManager.startHighlightingValidTargets(is_target_tile_valid);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
		{
			List<RaycastResult> hits = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventData, hits);
			foreach (RaycastResult hit in hits)
			{
				if ( _is_target_valid(hit.gameObject) )
				{
					_do_effect(hit.gameObject);
					TileHighlightManager.stopHighlightingValidTargets();
					_instance.gameObject.SetActive(false);
					EventQueue.instance.endEvent();
					break;
				}
			}
		}
	}

	public void skip()
	{
		TileHighlightManager.stopHighlightingValidTargets();
		_instance.gameObject.SetActive(false);
		EventQueue.instance.endEvent();
	}
}