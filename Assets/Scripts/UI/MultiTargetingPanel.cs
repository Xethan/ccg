using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class MultiTargetingPanel : MonoBehaviour, IPointerClickHandler
{
    public static MultiTargetingPanel                         _instance;
    private List<Func<GameObject, List<GameObject>, bool>>    _are_targets_valid;
    private Action<List<GameObject>>                          _do_effect;
    private List<GameObject>                                  _selected_targets;

    void Awake()
    {
        _instance = this;
        this.gameObject.SetActive(false);
    }

    public static void doTargetingAbility(
        List<Func<GameObject, List<GameObject>, bool>> are_targets_valid,
        Action<List<GameObject>> do_effect
    ) {
        EventQueue.instance.newEvent(_instance._doTargetingAbility,
                                     new Dictionary<string, object> {
                                        {"are_targets_valid", are_targets_valid},
                                        {"do_effect", do_effect}
                                     });
    }

    private void _doTargetingAbility(Dictionary<string, object> kwargs)
    {
        _are_targets_valid = kwargs["are_targets_valid"] as List<Func<GameObject, List<GameObject>, bool>>;
        _do_effect = kwargs["do_effect"] as Action<List<GameObject>>;
        _selected_targets = new List<GameObject>();

        _instance.gameObject.SetActive(true);
        _instance.transform.SetAsLastSibling();
        _update_state();

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if ( eventData.button == PointerEventData.InputButton.Left )
        {
            List<RaycastResult> hits = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, hits);
            foreach (RaycastResult hit in hits)
            {
                if ( _are_targets_valid[_selected_targets.Count](hit.gameObject, _selected_targets) )
                {
                    _selected_targets.Add(hit.gameObject);
                    _update_state();
                    break;
                }
            }
        }
    }

    private void _update_state()
    {

        if ( _selected_targets.Count < _are_targets_valid.Count )
        {
            Predicate<Tile> is_target_tile_valid = (Tile tile) =>
            {
                return _are_targets_valid[_selected_targets.Count](tile.gameObject, _selected_targets);
            };
            TileHighlightManager.startHighlightingValidTargets(is_target_tile_valid);
        }
        else
        {
            _do_effect(_selected_targets);
            TileHighlightManager.stopHighlightingValidTargets();
            _instance.gameObject.SetActive(false);
            EventQueue.instance.endEvent();
        }
    }

    public void skip()
    {
        TileHighlightManager.stopHighlightingValidTargets();
        _instance.gameObject.SetActive(false);
        EventQueue.instance.endEvent();
    }
}