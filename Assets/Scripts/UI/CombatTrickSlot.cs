using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System; // Math
using System.Collections.Generic;

public class CombatTrickSlot : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject _spell_container;
    [SerializeField] private GameObject _panel_mana_cost_ui;
    [SerializeField] private Text       _remaining_cooldown;
    [SerializeField] private GameObject _green_dot_is_available;

    private PanelCombatTricks           _panel_combat_tricks;
    private CombatTrick                 _combat_trick;
    private ManaCostUI                  _mana_cost_ui;
    private bool                        _is_available = false;
    private int                         _delay = 2;
    private const int                   _delay_between_refreshments = 3;

    public void initialize(PanelCombatTricks panel_combat_tricks, CombatTrick combat_trick)
    {
        _panel_combat_tricks = panel_combat_tricks;
        _combat_trick = combat_trick;
        _combat_trick.transform.SetParent(_spell_container.transform, false);
        updateUI();

        _mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
        _mana_cost_ui.transform.SetParent(_panel_mana_cost_ui.transform, false);
        _mana_cost_ui.updateUI(_combat_trick.ManaCost);
    }

    public void reduceCooldown(int nb_turns)
    {
        _delay = Math.Max(_delay - nb_turns, 0);
        if ( _delay == 0 )
        {
            _is_available = true;
            _delay = _delay_between_refreshments;
        }
        updateUI();
    }

    public void endTurn()
    {
        reduceCooldown(1);
    }

    private void updateUI()
    {
        _green_dot_is_available.SetActive(_is_available);
        _remaining_cooldown.text = _delay.ToString();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _combat_trick.onHover(Board.instance.CardHoverPanel);;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _combat_trick.outHover();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if ( eventData.button == PointerEventData.InputButton.Right )
            RelatedCardsPanel.instance.openRelatedCardsPanel(_combat_trick.CardStats.RelatedCards,
                                                             _combat_trick.CardStats.RelatedTiles);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if ( _is_available && _panel_combat_tricks.CombatTrickPlayedThisTurn == false )
            TileHighlightManager.startHighlightingValidTargets(_combat_trick.canPlay);
    }

    public void OnDrag(PointerEventData eventData) {}

    public void OnEndDrag(PointerEventData eventData)
    {
        if ( !_is_available || _panel_combat_tricks.CombatTrickPlayedThisTurn )
            return;
        TileHighlightManager.stopHighlightingValidTargets();

        List<RaycastResult> hits = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, hits);
        foreach (RaycastResult hit in hits)
        {
            Tile tile = hit.gameObject.GetComponent<Tile>();
            if ( tile != null )
            {
                if ( _combat_trick.play(tile) == true )
                {
                    Trigger.activate(Trigger.e_type.Combat_trick, source : _combat_trick);
                    _is_available = false;
                    _panel_combat_tricks.CombatTrickPlayedThisTurn = true;
                    updateUI();
                }
                break;
            }
        }
    }
}