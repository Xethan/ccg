using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class PanelKeywordsUI : MonoBehaviour
{
    private static Dictionary<string, string> keywords_2_description = new Dictionary<string, string> {
        {"Provoke", "Ennemies near this character cannot attack another non-Provoke character."},
        {"Tackle", "Ennemies near this character cannot move."},
        {"Strategic Unit", "Ennemies cannot attack Strategic Units if they have other possible attack targets."},
        {"Trample", "Move: Fight target ennemy Unit that you can kill with an attack and move to its tile."},
        {"Backstab", "Attacks from the 3 tiles behind an ennemy are Backstab attacks."},
        {"Flanking", "Attacks from the tile above or under an ennemy are Flanking attacks."},
        {"Lightning", "When attacking, deal lightning damage to ennemy units near the target (range: square of size 1)"},
        {"Hunter", ""},
        {"Tough", ""},
        {"Regeneration", ""},
        {"Corrosion", ""},
        {"Weakness", ""},
        {"Fleeting", ""},
        {"Lifesteal", ""},
        {"Stun", ""},
        {"Link", ""},
        {"Shell", ""},
    };

    public void initialize(string text)
    {
        MatchCollection keyword_matches = Regex.Matches(text, @"<b>(.*?)</b>");
        foreach (Match match in keyword_matches)
        {
            string keyword_name = match.Groups[1].Value;
            if ( keywords_2_description.ContainsKey(keyword_name) )
            {
                KeywordUI keyword_ui = Tools.instantiateFromPath("Game UI/Keyword UI").GetComponent<KeywordUI>();
                keyword_ui.updateUI( keyword_name, keywords_2_description[keyword_name] );
                keyword_ui.transform.SetParent(this.transform, false);
            }
        }
    }
}