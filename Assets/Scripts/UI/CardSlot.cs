using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;
using System.Collections.Generic;

public class CardSlot : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField]
	private GameObject	_card_container;
	[SerializeField]
	private GameObject	_panel_mana_cost_ui;

	private ManaCostUI	_mana_cost_ui;
	private Card		_card;

	public Card Card
	{
		get { return _card; }
		set
		{
			_card = value;

			if ( _card == null )
				_mana_cost_ui.displayUI(false);
			else
			{
				_card.transform.SetParent(_card_container.transform, false);
				_card.gameObject.SetActive(true);
				updateUI();
				_mana_cost_ui.displayUI(true);
			}
		}
	}

	void Awake()
	{
		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_panel_mana_cost_ui.transform, false);
	}

	public bool isSlotEmpty()
	{
		return _card == null;
	}

	public void updateUI()
	{
		_mana_cost_ui.updateUI(_card.Owner.ManaReserve.getManaCostAfterModifs(_card, _card.ManaCost.raw_cost));
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if ( _card != null )
			_card.onHover(Board.instance.CardHoverPanel);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if ( _card != null )
			_card.outHover();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Right )
			RelatedCardsPanel.instance.openRelatedCardsPanel(_card.CardStats.RelatedCards,
				                                             _card.CardStats.RelatedTiles);
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if ( _card != null )
		{
			Board.getCurrentPlayer().ManaReserve.onBeginDragCard(_card);
			TileHighlightManager.startHighlightingValidTargets(_card.canPlay);
		}
	}

	public void OnDrag(PointerEventData eventData) {}

	public void OnEndDrag(PointerEventData eventData)
	{
		if ( _card == null )
			return;

		Board.getCurrentPlayer().ManaReserve.onEndDragCard();
		TileHighlightManager.stopHighlightingValidTargets();

		List<RaycastResult> hits = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventData, hits);
		foreach (RaycastResult hit in hits)
		{
			Tile tile = hit.gameObject.GetComponent<Tile>();
			FactionManaUI faction_mana_ui = hit.gameObject.GetComponent<FactionManaUI>();

			if ( tile != null )
				onEndDragToTile(tile);
			else if ( faction_mana_ui != null )
				onEndDragToManaUI(faction_mana_ui);
		}
	}

	private void onEndDragToTile(Tile tile)
	{
		if ( _card.canPlay(tile) == true )
		{
			Card card_to_play = Card;
			Card = null;
			Assert.IsTrue(card_to_play.play(tile));
		}
	}

	private void onEndDragToManaUI(FactionManaUI faction_mana_ui)
	{
		if ( faction_mana_ui.consume(_card) )
		{
			_card.destroy();
			Card = null;
		}
	}
}