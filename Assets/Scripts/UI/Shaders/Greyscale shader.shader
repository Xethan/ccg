Shader "Sprites/Greyscale shader"
{
    Properties
    {
        [PerRendererData] _MainTex ("Texture", 2D) = "white" {}
        [PerRendererData] _Greyscale ("Greyscale", Range(0,1)) = 0

        [HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
    }

    SubShader
    {
        Tags { "Queue" = "Transparent" }

        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM

            #pragma vertex SpriteVert
            #pragma fragment frag

            #include "UnitySprites.cginc"

            half _Greyscale;

            fixed4 frag(v2f input) : SV_Target
            {
                fixed4 c = SampleSpriteTexture(input.texcoord);
                c.rgb = lerp(c.rgb, dot(c.rgb, float3(0.3, 0.59, 0.11)), _Greyscale);
                return c;
            }

            ENDCG

        }
    }

    Fallback "Sprites/Default"
}
