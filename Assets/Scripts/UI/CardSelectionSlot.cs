using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardSelectionSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	[SerializeField] private GameObject			_red_cross;
	[SerializeField] private GameObject			_replaced_text;
	[SerializeField] private GameObject			_selected_indicator;

	[SerializeField] private GameObject			_card_container;
	[SerializeField] private GameObject			_card_hover_panel;

	private CardSelectionPanel	_card_selection_panel;
	private	Card				_card;
	private bool				_selected;
	private bool				_selection_is_replacement;

	public bool Selected
	{
		get { return _selected; }
		set
		{
			_selected = value;

			if ( _selection_is_replacement )
			{
				_red_cross.SetActive(_selected);
				_replaced_text.SetActive(_selected);
			}
			else
			{
				_selected_indicator.SetActive(_selected);
			}
		}
	}

	public void initialize(CardSelectionPanel card_selection_panel)
	{
		_card_selection_panel = card_selection_panel;
	}

	public void setSelectionType(bool selection_is_replacement)
	{
		_selection_is_replacement = selection_is_replacement;
		_selected = false;
		_red_cross.SetActive(false);
		_replaced_text.SetActive(false);
		_selected_indicator.SetActive(false);
	}

	public Card Card
	{
		get { return _card; }
		set
		{
			_selected = false;
			_red_cross.SetActive(false);
			_replaced_text.SetActive(false);
			_selected_indicator.SetActive(false);
			if ( _card != null )
				_card.outHover();
			_card = value;
			_card.transform.SetParent(_card_container.transform, false);
			_card.gameObject.SetActive(true);
			_card.onHover(_card_hover_panel);
		}
	}

	public void OnPointerEnter(PointerEventData eventData) {}

	public void OnPointerExit(PointerEventData eventData) {}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
			_card_selection_panel.onClick(this);
		if ( eventData.button == PointerEventData.InputButton.Right )
			RelatedCardsPanel.instance.openRelatedCardsPanel(_card.CardStats.RelatedCards,
				                                             _card.CardStats.RelatedTiles);
	}
}