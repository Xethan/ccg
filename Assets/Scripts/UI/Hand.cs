using UnityEngine;
using System.Collections.Generic;

public class Hand : MonoBehaviour
{
	[SerializeField]
	private CardSlot	_card_slot_prefab;
	private CardSlot[]	_cards = new CardSlot[Board.max_cards_in_hand];

	public IEnumerable<CardSlot> iterateOverCardSlots()
	{
		foreach (CardSlot card_slot in _cards)
			if ( card_slot.Card )
				yield return card_slot;
	}

	void Awake()
	{
		instantiateHandUI();
	}

	private void instantiateHandUI()
	{
		for (int i = 0; i < _cards.Length; ++i)
		{
			_cards[i] = GameObject.Instantiate(_card_slot_prefab) as CardSlot;
			_cards[i].transform.SetParent(this.transform, false);
			_cards[i].gameObject.name = "Card Slot " + (i + 1);
		}
	}

	public void draw(Card card)
	{
		foreach (CardSlot slot in _cards)
		{
			if ( slot.isSlotEmpty() == true )
			{
				slot.Card = card;
				break;
			}
		}
		// Main pleine --> Défausser la carte
	}
}