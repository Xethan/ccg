using UnityEngine;
using UnityEngine.UI;

public class KeywordUI : MonoBehaviour
{
	[SerializeField] private Text   _name;
	[SerializeField] private Text	_description;

	public void updateUI(string name, string description)
	{
		_name.text = name;
		_description.text = description.Replace("<br>", "\n");
	}
}