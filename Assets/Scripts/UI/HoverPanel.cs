using UnityEngine;

public class HoverPanel : MonoBehaviour
{
	public static float		width_to_height_ratio = 1.25f;
	private bool			did_resize = false;

	/* This class will redimension its gameObject to have the correct proportions for a Card UI */
	void Update()
	{
		if ( did_resize == true )
			return;

		Rect rect = gameObject.GetComponent<RectTransform>().rect;
		if ( rect.width != 0 && rect.height != 0 )
		{
			Tools.initializeGridLayoutParameters(this.gameObject, 1, 1, width_to_height_ratio, 1.0f, 0.0f);
			did_resize = true;
		}
	}
}