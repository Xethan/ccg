using UnityEngine;
using System.Collections.Generic; // Lists

public class RelatedCardsPanel : MonoBehaviour
{
    [SerializeField] private CardStatsUI _card_stats_ui_prefab;
    [SerializeField] private GameObject  _panel_cards_ui;

    public static RelatedCardsPanel      instance;

    private List<CardStatsUI>            _cards_ui = new List<CardStatsUI>();
    private List<SpecialTile>            _tiles_ui = new List<SpecialTile>();

    void Awake()
    {
        instance = this;
        this.gameObject.SetActive(false);
    }

    public void openRelatedCardsPanel(List<string> related_cards_prefab_path, List<string> related_tiles_prefab_path)
    {
        this.gameObject.SetActive(true);
        this.transform.SetAsLastSibling();
        foreach (string related_card_prefab_path in related_cards_prefab_path)
        {
            CardStatsUI card_stats_ui = GameObject.Instantiate(_card_stats_ui_prefab) as CardStatsUI;
            card_stats_ui.transform.SetParent(_panel_cards_ui.transform, false);
            CardStats related_card_stats = Tools.instantiateFromPath(related_card_prefab_path + " Stats").GetComponent<CardStats>();
            card_stats_ui.initialize(related_card_stats);
            _cards_ui.Add(card_stats_ui);
        }
        foreach (string related_tile_prefab_path in related_tiles_prefab_path)
        {
            SpecialTile special_tile = Tools.instantiateFromPath(related_tile_prefab_path).GetComponent<SpecialTile>();
            special_tile.transform.SetParent(_panel_cards_ui.transform, false);
            special_tile.instantiateHoverUI();
            special_tile.onHover(_panel_cards_ui);
            special_tile.gameObject.SetActive(false);
            _tiles_ui.Add(special_tile);
        }
    }

    public void closePanel()
    {
        foreach (CardStatsUI card_ui in _cards_ui)
            GameObject.Destroy(card_ui.gameObject);
        _cards_ui.Clear();


        foreach (SpecialTile special_tile in _tiles_ui)
        {
            special_tile.outHover();
            GameObject.Destroy(special_tile.gameObject);
        }
        _tiles_ui.Clear();

        this.gameObject.SetActive(false);
    }
}