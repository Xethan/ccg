using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActivatedAbilitySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	[SerializeField] private Text	_cost;
	[SerializeField] private Text	_name;

	private	ActivatedAbility		_activated_ability;

	public void initialize(ActivatedAbility activated_ability)
	{
		_activated_ability = activated_ability;
		_cost.text = activated_ability.Cost;
		_name.text = activated_ability.Name;
	}

	public void OnPointerEnter(PointerEventData eventData) {}

	public void OnPointerExit(PointerEventData eventData) {}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
			_activated_ability.activate();
	}
}