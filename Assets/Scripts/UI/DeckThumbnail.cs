using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class DeckThumbnail : MonoBehaviour
{
	[SerializeField] private Text	_deck_name;
	[SerializeField] private Image	_deck_emblem;
	[SerializeField] private Text	_nb_cards;

	public void updateUI(SerializableDeck deck)
	{
		if ( deck._list_factions != null )
		{
			_deck_emblem.sprite = new Factions(deck._list_factions).DeckEmblem;
			_deck_emblem.gameObject.SetActive(true);
		}
		else
			_deck_emblem.gameObject.SetActive(false);

		if ( _deck_name != null )
			_deck_name.text = deck._name;
		if ( _nb_cards != null )
			_nb_cards.text = deck._nb_cards + " / 40 cards";
	}
}