using UnityEngine;
using UnityEngine.UI;

public class BuffUI : MonoBehaviour
{
	[SerializeField] private Text	_description;

	public void updateUI(string description)
	{
		_description.text = description.Replace("<br>", "\n");
	}
}