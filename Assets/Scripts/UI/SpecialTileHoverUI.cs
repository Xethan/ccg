using UnityEngine;
using UnityEngine.UI;

public class SpecialTileHoverUI : MonoBehaviour
{
	[SerializeField] private Image		_sprite;
	[SerializeField] private Text		_name;
	[SerializeField] private Text		_text;

	public void updateUI(Sprite card_sprite, string name, string text)
	{
		_sprite.sprite = card_sprite;
		_name.text = name;
		_text.text = text.Replace("<br>", "\n");
	}
}