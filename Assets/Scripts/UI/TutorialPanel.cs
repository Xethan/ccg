using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class Tutorial
{
    public virtual string Title { get; }
    public virtual string Content { get; }
}

public class TutorialNode
{
    public Tutorial tutorial;
    public TutorialNode parent = null;
    public List<TutorialNode> children;
    public List<TutorialNode> siblings;

    public TutorialNode(Tutorial tutorial, List<TutorialNode> children)
    {
        this.tutorial = tutorial;
        this.parent = null;
        this.children = children;
        this.siblings = new List<TutorialNode>();

        foreach (TutorialNode child in children)
        {
            child.parent = this;
            foreach (TutorialNode sibling in children)
                if ( sibling != child )
                    child.siblings.Add(sibling);
        }
    }
}

public class TutorialPanel : MonoBehaviour
{
    [SerializeField] private Text _title;
    [SerializeField] private Text _content;
    [SerializeField] private Button _parent_button;
    [SerializeField] private GameObject _children_panel;
    [SerializeField] private GameObject _siblings_panel;
    [SerializeField] private Button _button_prefab;

    [SerializeField] private GameObject _vertical_panel;
    [SerializeField] private GameObject _horizontal_panel_prefab;

    private Dictionary<int, GameObject> _horizontal_panels = new Dictionary<int, GameObject>();

    private List<Button> _buttons_children = new List<Button>();
    private List<Button> _buttons_siblings = new List<Button>();

    private TutorialNode _tutorial_node = new TutorialNode(
            new TutorialBase(),
            new List<TutorialNode> {

                new TutorialNode(
                    new TutorialMana(),
                    new List<TutorialNode> {

                        new TutorialNode(
                            new TutorialTest(),
                            new List<TutorialNode>()
                        ),
                        new TutorialNode(
                            new TutorialTest(),
                            new List<TutorialNode>()
                        ),
                    }
                ),
                new TutorialNode(
                    new TutorialGenerals(),
                    new List<TutorialNode>()
                ),
                new TutorialNode(
                    new TutorialMoveAndAttack(),
                    new List<TutorialNode>()
                ),
                new TutorialNode(
                    new TutorialDeckCreation(),
                    new List<TutorialNode> {

                        new TutorialNode(
                            new TutorialTest(),
                            new List<TutorialNode>()
                        ),
                    }
                ),
                new TutorialNode(
                    new TutorialPlayCards(),
                    new List<TutorialNode> {

                        new TutorialNode(
                            new TutorialTest(),
                            new List<TutorialNode>()
                        ),
                        new TutorialNode(
                            new TutorialTest(),
                            new List<TutorialNode>()
                        ),
                    }
                )
            }
        );

    public void Start()
    {
        updateUI();
        constructButtonsTree(_tutorial_node);
    }

    private void updateUI()
    {
        _title.text = _tutorial_node.tutorial.Title;
        _content.text = _tutorial_node.tutorial.Content;
        // Highlight current node in tree
    }

    private void constructButtonsTree(TutorialNode node, int level=0, GameObject parent_panel=null)
    {
        if ( parent_panel == null )
        {
            parent_panel = GameObject.Instantiate(_horizontal_panel_prefab) as GameObject;
            parent_panel.transform.SetParent(_vertical_panel.transform, false);
        }

        Button new_button = GameObject.Instantiate(_button_prefab).GetComponent<Button>();
        new_button.gameObject.transform.SetParent(parent_panel.transform, false);
        initializeButton(new_button, node);

        GameObject horizontal_panel_children = GameObject.Instantiate(_horizontal_panel_prefab) as GameObject;
        horizontal_panel_children.transform.SetParent(_getHorizontalPanel(level+1).transform, false);
        foreach (TutorialNode child in node.children)
            constructButtonsTree(child, level+1, horizontal_panel_children);
    }

    private GameObject _getHorizontalPanel(int level)
    {
        if ( _horizontal_panels.ContainsKey(level) )
            return _horizontal_panels[level];

        GameObject new_horizontal_panel = GameObject.Instantiate(_horizontal_panel_prefab);
        new_horizontal_panel.transform.SetParent(_vertical_panel.transform, false);
        _horizontal_panels[level] = new_horizontal_panel;
        return new_horizontal_panel;
    }

    private void initializeButton(Button button, TutorialNode tutorial_node)
    {
        button.gameObject.SetActive(true);
        button.GetComponentInChildren<Text>().text = tutorial_node.tutorial.Title;
        button.GetComponent<Button>().onClick.AddListener(() => { goToSelectedTutorialNode(tutorial_node); });
    }

    public void goToSelectedTutorialNode(TutorialNode tutorial_node)
    {
        _tutorial_node = tutorial_node;
        updateUI();
    }
}

// ###### Contents of the tutorial panels ######

public class TutorialTest : Tutorial
{
    public override string Title { get { return "Test"; } }
    public override string Content { get { return "Lorem ipsum"; } }
}

public class TutorialBase : Tutorial
{
    public override string Title { get { return "Règles de base"; } }
    public override string Content { get { return
@"Le jeu prend place sur un plateau de 9x7 cases.
Les joueurs jouent chacun leur tour.

Chaque joueur possède deux <b>généraux</b> qui sont déjà sur le plateau au début de la partie, ainsi qu'un deck de 40 cartes dont 5 sont piochées au début.
Un joueur remporte la partie lorsqu'il tue un général ennemi ou si un de ses <b>généraux</b> passe niveau 3.

Chaque unité peut se déplacer une fois par tour de deux cases et attaquer une unité ennemie sur une des 8 cases adjacentes.
Chaque joueur commence avec deux manas, peut défausser une fois par tour une carte de sa main pour gagner de la <b>mana</b> et pioche deux cartes à la fin de chaque tour."; } }
}

public class TutorialMana : Tutorial
{
    public override string Title { get { return "Mana"; } }
    public override string Content { get { return
@"Pour jouer des cartes il faut payer leur coût en mana.
Chaque carte a un coût en mana neutre et un coût en mana de faction.
Chaque joueur commence avec 2 mana neutre et 1 mana de la faction de chacun de ses <b>généraux</b>.

A la fin de son tour, tout mana inutilisé va dans la <b>Réserve de Mana supplémentaire</b>.
Au début de son tour, on récupère la mana utilisée au tour précédent.

Une fois par tour, on peut défausser une carte de sa main pour agrandir sa réserve de mana, on dit qu'on la consomme.
Lorsqu'on consomme une carte, on gagne toujours 1 mana neutre.
Lorsqu'on consomme une carte mono-faction, on gagne également 1 mana de cette faction.
Lorsqu'on consomme une carte multi-faction, on choisit pour quelle faction on gagne 1 mana."; } }
}

public class TutorialGenerals : Tutorial
{
    public override string Title { get { return "Généraux"; } }
    public override string Content { get { return
@"Chaque deck contient 2 généraux, qui commencent la partie directement sur le terrain.
Les généraux se comportent comme des unités normales pour les déplacements et les attaques.
Si un des deux généraux d'un joueur meurt, il perd la partie.

Chaque général possède un pouvoir de général, qui coûte un mana neutre et un mana de la faction du général.
Le sort du premier général est disponible au tour 3, celui du deuxième général au tour 4.
Chaque pouvoir est ensuite de nouveau disponible tous les 2 tours.

Chaque général possède aussi une quête, qui lui permet de passer au niveau 2 puis au niveau 3.
Passer au niveau 2 profère à chaque général un bonus qui lui est propre.
Passer au niveau 3 vous fait gagner la partie sur le champ."; } }
}

public class TutorialMoveAndAttack : Tutorial
{
    public override string Title { get { return "Déplacement et attaque"; } }
    public override string Content { get { return
@"Chaque unité peut se déplacer puis attaquer à chaque tour. Attaquer en premier empêche de se déplacer après.

Les déplacements ont une portée de 2 cases.
Les unités peuvent passer à travers des unités alliées mais pas des unités ennemies.

Une attaque peut cibler n'importe quelle unité ennemie sur une des 8 cases adjacentes à l'unité.
L'unité attaquante inflige autant de dégâts qu'elle a d'attaque à l'unité attaquée, puis l'unité attaquée fait de même sur l'unité attaquante."; } }
}

public class TutorialDeckCreation : Tutorial
{
    public override string Title { get { return "Création de decks"; } }
    public override string Content { get { return
@"Chaque deck doit contenir 2 généraux, 2 CombatTrick et 40 cartes.
Un deck peut contenir un maximum de 3 exemplaires de chaque carte."; } }
}

public class TutorialPlayCards : Tutorial
{
    public override string Title { get { return "Jouer des cartes"; } }
    public override string Content { get { return
@"Pour jouer une carte, il faut payer son coût en mana.
Les créatures peuvent être invoquées sur les cases adjacentes à un de vos personnages.
Chaque sort a ses propres conditions de ciblage."; } }
}