using UnityEngine;
using System.Collections.Generic;

public class DeckTracker : MonoBehaviour
{
	[SerializeField]
	private GameObject							_panel_cards;
	[SerializeField]
	private DeckTrackerCard						_deck_tracker_card_prefab;

	private Dictionary<string, DeckTrackerCard>	_cards = new Dictionary<string, DeckTrackerCard>();

	public void updateUI(List<Card> deck)
	{
		foreach (DeckTrackerCard deck_tracker_card in _cards.Values)
			GameObject.Destroy(deck_tracker_card.gameObject);
		_cards.Clear();

		foreach (Card card in deck)
		{
			if ( _cards.ContainsKey(card.CardStats.Name) )
				_cards[card.CardStats.Name].addCopy();
			else
			{
				DeckTrackerCard deck_tracker_card = GameObject.Instantiate(_deck_tracker_card_prefab) as DeckTrackerCard;
				deck_tracker_card.transform.SetParent(_panel_cards.transform, false);
				deck_tracker_card.initialize(card);
				_cards[card.CardStats.Name] = deck_tracker_card;
			}
		}
		orderCards();
	}

	private void orderCards()
	{
		List<DeckTrackerCard> ordered_card_list = new List<DeckTrackerCard>(_cards.Values);

		ordered_card_list.Sort(delegate(DeckTrackerCard deck_tracker_card_1, DeckTrackerCard deck_tracker_card_2)
		{
			CardStats card_stats_1 = deck_tracker_card_1.Card.CardStats;
			CardStats card_stats_2 = deck_tracker_card_2.Card.CardStats;

			if ( card_stats_1.ManaCost.Converted != card_stats_2.ManaCost.Converted )
				return card_stats_1.ManaCost.Converted - card_stats_2.ManaCost.Converted;
			else
				return card_stats_1.Name.CompareTo(card_stats_2.Name);
		});
		for (int i = 0; i < ordered_card_list.Count; ++i)
		{
			DeckTrackerCard deck_tracker_card = _cards[ordered_card_list[i].Card.CardStats.Name];
			deck_tracker_card.transform.SetSiblingIndex(i);
		}
	}
}