using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CardStatsUI : MonoBehaviour
{
    protected CardStats         _card_stats;

    [SerializeField]
    private CardHoverUI         _card_ui_prefab;
    private CardHoverUI         _card_ui;

    [SerializeField]
    private CharacterHoverUI    _character_ui_prefab;
    private CharacterHoverUI    _character_ui;

    [SerializeField]
    private ManaCostUI          _mana_cost_ui_prefab;
    private ManaCostUI          _mana_cost_ui;

    public CardStats CardStats { get { return _card_stats; } }

    public void initialize(CardStats card_stats)
    {
        _card_stats = card_stats;
        _card_stats.transform.SetParent(this.transform, false);

        _card_ui = GameObject.Instantiate(_card_ui_prefab) as CardHoverUI;
        _card_ui.transform.SetParent(this.transform, false);
        _card_ui.updateUI(_card_stats.Sprite, _card_stats.Factions, _card_stats.Name, _card_stats.Text);

        if ( _card_stats.CardType == CardStats.e_card_type.Unit
             || _card_stats.CardType == CardStats.e_card_type.General )
        {
            _character_ui = GameObject.Instantiate(_character_ui_prefab) as CharacterHoverUI;
            _character_ui.transform.SetParent(_card_ui.transform, false);
            _character_ui.updateUI(_card_stats.Attack, _card_stats.Attack, _card_stats.Health, _card_stats.Health, _card_stats.Health, 0);
        }

        if ( _card_stats.CardType == CardStats.e_card_type.Unit
            || _card_stats.CardType == CardStats.e_card_type.Spell
            || card_stats.CardType == CardStats.e_card_type.Artifact )
        {
            _mana_cost_ui = GameObject.Instantiate(_mana_cost_ui_prefab) as ManaCostUI;
            _mana_cost_ui.transform.SetParent(_card_ui.PanelManaCostUI.transform, false);
            _mana_cost_ui.updateUI(_card_stats.ManaCost);
        }
    }
}