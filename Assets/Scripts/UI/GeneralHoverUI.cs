using UnityEngine;
using UnityEngine.UI;

public class GeneralHoverUI : MonoBehaviour
{
	[SerializeField] private Text	_level;
	[SerializeField] private Text	_xp;

	public void updateUI(int level, int xp, int[] xp_to_level_up)
	{
		_level.text = level.ToString();
		if ( level < xp_to_level_up.Length )
			_xp.text = xp.ToString() + " / " + xp_to_level_up[level].ToString() + " xp";
	}
}