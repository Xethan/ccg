using UnityEngine;
using UnityEngine.UI;

public class CardHoverUI : MonoBehaviour
{
	[SerializeField] private Image				_border_left;
	[SerializeField] private Image				_border_right;
	[SerializeField] private Image				_colored_background;
	[SerializeField] private Image				_middle_banner_left;
	[SerializeField] private Image				_middle_banner_right;
	[SerializeField] private GameObject 		_panel_mana_cost_ui;
	[SerializeField] private Image				_sprite;
	[SerializeField] private Image				_faction_emblem;
	[SerializeField] private Text				_name;
	[SerializeField] private Text				_card_text;
	[SerializeField] private GameObject			_panel_buffs_ui;
	[SerializeField] private PanelKeywordsUI	_panel_keywords_ui;

	public GameObject PanelManaCostUI { get { return _panel_mana_cost_ui; } }
	public GameObject PanelBuffsUI { get { return _panel_buffs_ui; } }

	public void updateUI(Sprite card_sprite, Factions factions, string name, string card_text)
	{
		_border_left.color = FactionManaUI.factions_color[factions.FirstFaction];
		_border_right.color = FactionManaUI.factions_color[factions.SecondFaction];
		if ( factions.FirstFaction == factions.SecondFaction )
			_colored_background.color = _getColor(factions.FirstFaction, opacity:0.15f);
		_middle_banner_left.color = _getColor(factions.FirstFaction, opacity:0.75f);
		_middle_banner_right.color = _getColor(factions.SecondFaction, opacity:0.75f);
		_sprite.sprite = card_sprite;
		_faction_emblem.sprite = factions.Emblem;
		_name.text = name;
		_card_text.text = card_text.Replace("<br>", "\n");
		_panel_keywords_ui.initialize(card_text);
	}

	private Color _getColor(int faction, float opacity=1.0f)
	{
		return new Color(
			FactionManaUI.factions_color[faction].r,
			FactionManaUI.factions_color[faction].g,
			FactionManaUI.factions_color[faction].b,
			opacity
		);
	}
}