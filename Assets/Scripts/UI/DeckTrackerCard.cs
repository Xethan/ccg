using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DeckTrackerCard : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] private GameObject	_panel_mana_cost_ui;
	[SerializeField] private Text		_card_name;
	[SerializeField] private Image		_card_image;
	[SerializeField] private Text		_text_nb_copies;

	private ManaCostUI					_mana_cost_ui;

	private Card						_card;
	private int							_nb_copies = 1;

	public Card Card { get { return _card; } }
	public int NbCopies { get { return _nb_copies; } }

	public void initialize(Card card)
	{
		_card = card;
		_card_name.text = card.CardStats.Name;
		_card_image.sprite = card.CardStats.Sprite;

		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_panel_mana_cost_ui.transform, false);
		_mana_cost_ui.updateUI(card.ManaCost);

		updateNbCopiesText();
	}

	public void addCopy()
	{
		_nb_copies += 1;
		updateNbCopiesText();
	}

	private void updateNbCopiesText()
	{
		_text_nb_copies.text = _nb_copies.ToString();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Right )
			RelatedCardsPanel.instance.openRelatedCardsPanel(_card.CardStats.RelatedCards,
				                                             _card.CardStats.RelatedTiles);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_card.onHover(Board.instance.CardHoverPanel);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_card.outHover();
	}
}