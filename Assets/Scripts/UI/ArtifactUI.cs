using UnityEngine;
using UnityEngine.UI;

public class ArtifactUI : MonoBehaviour
{
	[SerializeField] private Text	_name;
	[SerializeField] private Text	_durability;
	[SerializeField] private Text	_description;

	public void updateUI(string name, int durability, int max_durability, string description)
	{
		_name.text = name;
		_durability.text = durability.ToString() + "/" + max_durability.ToString() + " durability";
		_description.text = description;
	}
}