using UnityEngine;
using System; // Action
using System.Collections; // Coroutines
using System.Collections.Generic; // Lists

public class CardSelectionPanel : MonoBehaviour
{
	[SerializeField]
	private CardSelectionSlot				_card_selection_slot_prefab;
	[SerializeField]
	private GameObject						_panel_card_slots;

	public static CardSelectionPanel		instance;
	private int								_min_nb_cards_to_select;
	private int								_max_nb_cards_to_select;

	private List<CardSelectionSlot>			_card_slots = new List<CardSelectionSlot>();
	private Action<List<CardSelectionSlot>>	_resolve_selection;

	void Awake() { instance = this; }

	public void startCardSelection(List<Card> cards, int min_nb_cards_to_select, int max_nb_cards_to_select,
		                           Action<List<CardSelectionSlot>> resolve_selection, bool selection_is_replacement=false)
	{
		EventQueue.instance.newEvent(_startCardSelection,
			                         new Dictionary<string, object> {
			                         	{"cards", cards},
			            				{"min_nb_cards_to_select", min_nb_cards_to_select.ToString()},
			            				{"max_nb_cards_to_select", max_nb_cards_to_select.ToString()},
			            				{"resolve_selection", resolve_selection},
			            				{"selection_is_replacement", selection_is_replacement}
			            			 });
	}

	private void _startCardSelection(Dictionary<string, object> kwargs)
	{
		List<Card> cards = kwargs["cards"] as List<Card>;
		string tmp_min_nb_cards_to_select = kwargs["min_nb_cards_to_select"] as string;
		int min_nb_cards_to_select = Int32.Parse(tmp_min_nb_cards_to_select);
		string tmp_max_nb_cards_to_select = kwargs["max_nb_cards_to_select"] as string;
		int max_nb_cards_to_select = Int32.Parse(tmp_max_nb_cards_to_select);
		Action<List<CardSelectionSlot>> resolve_selection = kwargs["resolve_selection"] as Action<List<CardSelectionSlot>>;
		bool selection_is_replacement = (bool) kwargs["selection_is_replacement"];

		this.gameObject.SetActive(true);
		this.transform.SetAsLastSibling();
		_min_nb_cards_to_select = min_nb_cards_to_select;
		_max_nb_cards_to_select = max_nb_cards_to_select;
		_resolve_selection = resolve_selection;
		int i = 0;
		for (; i < cards.Count; ++i)
		{
			if (i >= _card_slots.Count)
			{
				_card_slots.Add(GameObject.Instantiate(_card_selection_slot_prefab) as CardSelectionSlot);
				_card_slots[i].transform.SetParent(_panel_card_slots.transform, false);
				_card_slots[i].initialize(this);
			}
			else
				_card_slots[i].gameObject.SetActive(true);
			_card_slots[i].setSelectionType(selection_is_replacement);
			_card_slots[i].Card = cards[i];
		}
		while (i < _card_slots.Count)
		{
			GameObject.Destroy(_card_slots[i].gameObject);
			_card_slots.RemoveAt(i);
		}
	}

	public void onClick(CardSelectionSlot clicked_card_slot)
	{
		if ( clicked_card_slot.Selected == true )
		{
			clicked_card_slot.Selected = false;
		}
		else if ( _max_nb_cards_to_select == 1 )
		{
			foreach (CardSelectionSlot card_slot in _card_slots)
				card_slot.Selected = false;
			clicked_card_slot.Selected = true;
		}
		else
		{
			int nb_cards_selected = 0;

			for (int i = 0; i < _card_slots.Count; ++i)
				if ( _card_slots[i].Selected == true )
					nb_cards_selected += 1;

			if ( nb_cards_selected < _max_nb_cards_to_select )
				clicked_card_slot.Selected = true;
		}
	}

	private bool isSelectionValid()
	{
		int nb_cards_selected = 0;

		foreach (CardSelectionSlot card_slot in _card_slots)
		{
			if (card_slot.Selected == true)
				nb_cards_selected += 1;
		}
		return nb_cards_selected >= _min_nb_cards_to_select;
	}

	public void validateSelection()
	{
		if (isSelectionValid() != true)
			return;

		foreach (CardSelectionSlot card_slot in _card_slots)
			card_slot.Card.outHover();
		_resolve_selection(_card_slots);
		this.gameObject.SetActive(false);
		EventQueue.instance.endEvent();
	}
}