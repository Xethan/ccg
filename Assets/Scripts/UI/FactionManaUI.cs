using UnityEngine;
using UnityEngine.UI;

public class FactionManaUI : MonoBehaviour
{
	public static Color[] factions_color = {
		Color.HSVToRGB(0f, 0.87f, 0.47f),
		Color.HSVToRGB(120f / 360, 0.87f, 0.47f),
		Color.HSVToRGB(300f / 360, 0.87f, 0.47f),
		Color.HSVToRGB(60f / 360, 0.87f, 0.47f),
		Color.HSVToRGB(180f / 360, 0.3f, 0.9f),
	};

	[SerializeField] private Color		_base_color;
	[SerializeField] private Color		_color_on_valid_drag;

	[SerializeField] private Image		_background_image;
	[SerializeField] private Image		_mana_symbol;
	[SerializeField] private Text		_mana_text;

	private ManaReserve					_mana_reserve;
	private int							_faction;

	public void initialize(ManaReserve mana_reserve, int faction)
	{
		_mana_reserve = mana_reserve;
		_faction = faction;
		_mana_symbol.color = FactionManaUI.factions_color[_faction];
	}

	public void updateUI(int current_mana, int max_mana)
	{
		_mana_text.text = current_mana.ToString() + " / " + max_mana.ToString();
	}

	public void onBeginDragCard(Card card)
	{
		if ( card.Factions.isFromFaction(_faction) )
			_background_image.color = _color_on_valid_drag;
	}

	public void onEndDragCard()
	{
		_background_image.color = _base_color;
	}

	public bool consume(Card card)
	{
		return _mana_reserve.cardDraggedToFactionManaUI(card.Factions, _faction);
	}
}