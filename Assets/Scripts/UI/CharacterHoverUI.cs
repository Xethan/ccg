using UnityEngine;

public class CharacterHoverUI : MonoBehaviour
{
	[SerializeField] private CharacterUI	_character_ui;

	public void updateUI(int attack, int base_attack, int health, int base_health, int max_health, int armor)
	{
		_character_ui.updateUI(attack, base_attack, health, base_health, max_health, armor);
	}
}