using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ManaCostUI : MonoBehaviour
{
	[SerializeField] private GameObject	_mana_cost_layout_group;
	[SerializeField] private Text		_neutral_mana;
	[SerializeField] private Image		_faction_mana_prefab;

	private List<Image>					_faction_mana_indicators = new List<Image>();

	public void updateUI(ManaCost mana_cost)
	{
		foreach (Image image in _faction_mana_indicators)
			GameObject.Destroy(image.gameObject);
		_faction_mana_indicators.Clear();

		_neutral_mana.text = mana_cost.raw_cost[0].ToString();
		for (int i_faction = 1; i_faction < mana_cost.raw_cost.Length; ++i_faction)
		{
			for (int j = 0; j < mana_cost.raw_cost[i_faction]; ++j)
			{
				Image new_image = GameObject.Instantiate(_faction_mana_prefab) as Image;
				new_image.transform.SetParent(_mana_cost_layout_group.transform, false);
				new_image.color = FactionManaUI.factions_color[i_faction];
				_faction_mana_indicators.Add(new_image);
			}
		}
	}

	public void displayUI(bool should_display)
	{
		_mana_cost_layout_group.SetActive(should_display);
	}
}