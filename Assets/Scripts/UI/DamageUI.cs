using UnityEngine;
using UnityEngine.UI;
using System.Collections; // Coroutines

public class DamageUI : MonoBehaviour
{
	[SerializeField] private Image	_image;
	[SerializeField] private Text	_damage_text;

	private Color					_start_color = new Color32(255, 255, 255, 255);
	private Color					_end_color = new Color32(255, 255, 255, 0);
	private readonly float			_fixed_opacity_time = 0.5f;
	private readonly float			_decreasing_opacity_time = 0.0f;

	public void initialize(int amount)
	{
		_damage_text.text = "-" + amount.ToString();
		StartCoroutine(coroutineUIDisplay());
	}

	private IEnumerator coroutineUIDisplay()
	{
		yield return new WaitForSeconds(_fixed_opacity_time);

		float start_time = Time.time;
		float current_time = start_time;

		while ( current_time <= start_time + _decreasing_opacity_time )
		{
			_image.color = Color.Lerp(_start_color, _end_color, (current_time - start_time) / _decreasing_opacity_time);
			_damage_text.color = Color.Lerp(_start_color, _end_color, (current_time - start_time) / _decreasing_opacity_time);
			yield return new WaitForFixedUpdate();
			current_time += Time.deltaTime;
		}
		GameObject.Destroy(this.gameObject);
	}
}