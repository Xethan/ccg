using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System; // Math
using System.Collections.Generic;

public class BattleCrySlot : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] private GameObject	_spell_container;
	[SerializeField] private GameObject	_panel_mana_cost_ui;
	[SerializeField] private Text		_remaining_cooldown;
	[SerializeField] private GameObject	_green_dot_is_available;

	private BattleCry					_battle_cry;
	private ManaCostUI					_mana_cost_ui;
	private int							_initial_delay;
	private const int 					_delay = 2;
	private bool						_can_use = false;
	private int							_current_turn = 1;

	public void initialize(BattleCry battle_cry, int initial_delay)
	{
		_battle_cry = battle_cry;
		_battle_cry.transform.SetParent(_spell_container.transform, false);
		_initial_delay = initial_delay;
		updateUI();

		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_panel_mana_cost_ui.transform, false);
		_mana_cost_ui.updateUI(_battle_cry.ManaCost);
	}

	public void reactivate()
	{
		_can_use = true;
		updateUI();
	}

	public void reduceCooldown(int nb_turns)
	{
		for (int i = 0; i < nb_turns; i++)
		{
			_current_turn += 1;
			if (RemainingCooldown == 0)
				_can_use = true;
			updateUI();
		}
	}

	public void endTurn()
	{
		reduceCooldown(1);
	}

	private void updateUI()
	{
		_green_dot_is_available.SetActive(_can_use == true);
		_remaining_cooldown.text = RemainingCooldown == 0 ? _delay.ToString() : RemainingCooldown.ToString();
	}

	private int RemainingCooldown
	{
		get
		{
			if ( _initial_delay - _current_turn > 0 )
				return _initial_delay - _current_turn;
			else
				return (_current_turn - _initial_delay) % _delay;
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_battle_cry.onHover(Board.instance.CardHoverPanel);;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_battle_cry.outHover();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Right )
			RelatedCardsPanel.instance.openRelatedCardsPanel(_battle_cry.CardStats.RelatedCards,
				                                             _battle_cry.CardStats.RelatedTiles);
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if ( _can_use == true && _battle_cry.Character != null )
			TileHighlightManager.startHighlightingValidTargets(_battle_cry.canPlay);
	}

	public void OnDrag(PointerEventData eventData) {}

	public void OnEndDrag(PointerEventData eventData)
	{
		if ( _can_use == false || _battle_cry.Character == null)
			return;
		TileHighlightManager.stopHighlightingValidTargets();

		List<RaycastResult> hits = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventData, hits);
		foreach (RaycastResult hit in hits)
		{
			Tile tile = hit.gameObject.GetComponent<Tile>();
			if ( tile != null )
			{
				if ( _battle_cry.play(tile) == true )
				{
					Trigger.activate(Trigger.e_type.Battle_cry, source : _battle_cry.Character);
					_can_use = false;
					updateUI();
				}
				break;
			}
		}
	}
}