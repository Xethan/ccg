using UnityEngine;
using UnityEngine.UI;

public class CharacterUI : MonoBehaviour
{
	[SerializeField] private Text	_attack;
	[SerializeField] private Text	_health;
	[SerializeField] private Text	_armor;

	[SerializeField] private Color	_base_color;
	[SerializeField] private Color	_buffed_stat_color;
	[SerializeField] private Color	_nerfed_stat_color;

	public void updateUI(int attack, int base_attack, int health, int base_health, int max_health, int armor)
	{
		updateText(_attack, attack, base_attack, base_attack);
		updateText(_health, health, base_health, max_health);
		_armor.text = armor.ToString();
		_armor.gameObject.SetActive(armor > 0);
	}

	private void updateText(Text text, int stat, int base_stat, int max_stat)
	{
		text.text = stat.ToString();

		if ( stat < max_stat )
			text.color = _nerfed_stat_color;
		else if ( stat > base_stat )
			text.color = _buffed_stat_color;
		else
			text.color = _base_color;
	}
}