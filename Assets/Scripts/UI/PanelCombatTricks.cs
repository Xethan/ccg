using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelCombatTricks : MonoBehaviour
{
    [SerializeField]
    private CombatTrickSlot         _combat_trick_slot_prefab;

    private List<CombatTrickSlot>   _combat_trick_slots = new List<CombatTrickSlot>();
    private bool                    _combat_trick_played_this_turn = false;

    public List<CombatTrickSlot> CombatTrickSlots { get { return _combat_trick_slots;} }
    public bool CombatTrickPlayedThisTurn
    {
        get { return _combat_trick_played_this_turn; }
        set { _combat_trick_played_this_turn = value; }
    }

    public void initialize(List<CombatTrick> combat_tricks)
    {
        for (int i = 0; i < combat_tricks.Count; ++i)
        {
            _combat_trick_slots.Add(GameObject.Instantiate(_combat_trick_slot_prefab) as CombatTrickSlot);
            _combat_trick_slots[i].transform.SetParent(this.transform, false);
            _combat_trick_slots[i].gameObject.name = "Combat Trick Slot " + (i + 1);
            _combat_trick_slots[i].initialize(this, combat_tricks[i]);
        }
    }

    public void setActive()
    {
        if ( _combat_trick_slots.Count > 0 )
            this.gameObject.SetActive(true);
    }

    public void endTurn()
    {
        foreach (CombatTrickSlot slot in _combat_trick_slots)
            slot.endTurn();
        _combat_trick_played_this_turn = false;
    }
}