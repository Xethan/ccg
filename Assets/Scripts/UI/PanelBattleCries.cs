using UnityEngine;
using UnityEngine.UI;

public class PanelBattleCries : MonoBehaviour
{
	[SerializeField]
	private BattleCrySlot	_battle_cry_slot_prefab;

	private BattleCrySlot	_battle_cry_slot_general_1;
	private BattleCrySlot	_battle_cry_slot_general_2;

	public BattleCrySlot BattleCrySlotGeneral1 { get { return _battle_cry_slot_general_1;} }
	public BattleCrySlot BattleCrySlotGeneral2 { get { return _battle_cry_slot_general_2;} }

	public void initialize(BattleCry battle_cry_general_1, BattleCry battle_cry_general_2)
	{
		_battle_cry_slot_general_1 = GameObject.Instantiate(_battle_cry_slot_prefab) as BattleCrySlot;
		_battle_cry_slot_general_1.transform.SetParent(this.transform, false);
		_battle_cry_slot_general_1.initialize(battle_cry_general_1, initial_delay: 3);

		_battle_cry_slot_general_2 = GameObject.Instantiate(_battle_cry_slot_prefab) as BattleCrySlot;
		_battle_cry_slot_general_2.transform.SetParent(this.transform, false);
		_battle_cry_slot_general_2.initialize(battle_cry_general_2, initial_delay: 4);
	}

	public void endTurn()
	{
		_battle_cry_slot_general_1.endTurn();
		_battle_cry_slot_general_2.endTurn();
	}
}