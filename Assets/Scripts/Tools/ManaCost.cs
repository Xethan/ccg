using UnityEngine;
using System.Collections.Generic;

public struct ManaCost
{
	public readonly int[]	raw_cost;

	public ManaCost(int[] mana_cost)
	{
		if ( mana_cost.Length != Factions.nb_factions )
			Debug.Log("Invalid mana cost");
		raw_cost = mana_cost;
	}

	public int Converted
	{
		get
		{
			int converted_mana_cost = 0;

			for (int i = 0; i < raw_cost.Length; ++i)
			{
				if ( raw_cost[i] > converted_mana_cost )
					converted_mana_cost = raw_cost[i];
			}
			return converted_mana_cost;
		}
	}

	public string Description
	{
		get
		{
			if ( raw_cost.Length == 0 )
				return "";

			string description = raw_cost[Factions.neutral].ToString();

			for (int i = 1; i < raw_cost.Length; ++i)
			{
				if ( raw_cost[i] > 0 )
					description += " ";
				for (int j = 0; j < raw_cost[i]; ++j)
					description += Factions.factions_symbol[i];
			}
			return description;
		}
	}
}