using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour
{
	public void onClick(string scene_name)
	{
		SceneManager.LoadScene(scene_name);
	}
}