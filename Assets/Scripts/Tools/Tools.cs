using UnityEngine;
using UnityEngine.UI;
using System;

public class Tools
{
	public static GameObject instantiateFromPath(string prefab_path)
	{
		UnityEngine.Object prefab = Resources.Load(prefab_path);
		if ( prefab == null )
			Debug.Log("Prefab '" + prefab_path + "' not valid.");
		GameObject gameobject = GameObject.Instantiate(prefab) as GameObject;
		return gameobject;
	}

	public static T instantiateCardFromPath<T>(string prefab_path, Player owner) where T : Card
	{
		string card_class_name = prefab_path.Split(new System.Char [] {'/'})[1].Replace(" ", "");
		CardStats card_stats = instantiateFromPath(prefab_path + " Stats").GetComponent<CardStats>();
		T card = instantiateFromPath("Base Card").AddComponent(System.Type.GetType(card_class_name)) as T;
		card.Owner = owner;
		card.initialize(card_stats);
		return card;
	}

	private static T _attachCardToPlayerAndPutInHand<T>(T card, Player player) where T : Card
	{
		card.transform.SetParent(player.transform, false);
		card.gameObject.SetActive(false);
		player.putCardInHand(card);
		return card;
	}

	public static T createCardInPlayerHand<T>(string card_prefab_path, Player player) where T : Card
	{
		T card = instantiateCardFromPath<T>(card_prefab_path, owner:player);
		return _attachCardToPlayerAndPutInHand(card, player);
	}

	public static Card createCopyOfCardInPlayerHand(Card card_to_copy, Player player)
	{
		string card_class_name = card_to_copy.GetType().Name;
		CardStats card_stats = card_to_copy.CardStats;
		Card card = instantiateFromPath("Base Card").AddComponent(System.Type.GetType(card_class_name)) as Card;
		card.Owner = player;
		card.initialize(card_stats);
		return _attachCardToPlayerAndPutInHand<Card>(card, player);
	}

	public static Sprite instantiateSpriteFromPath(string sprite_path)
	{
		Texture2D texture = Resources.Load(sprite_path) as Texture2D;

		if ( texture == null )
			Debug.Log("Sprite '" + sprite_path + "' couldn't be found.");

		Rect rect = new Rect(0, 0, texture.width, texture.height);
		Vector2 pivot = new Vector2(0.5f, 0.5f);
		Sprite sprite = Sprite.Create(texture, rect, pivot);

		if ( sprite == null )
			Debug.Log("Sprite '" + sprite_path + "' couldn't be converted from Texture2D to Sprite.");

		return sprite;
	}

	public static void initializeGridLayoutParameters(
		GameObject gameobject,
		int nb_columns,
		int nb_lines,
		float width_to_height_ratio,
		float percentage_cells,
		float percentage_interior_vs_exterior_spacing
	) {
		GridLayoutGroup layout = gameobject.GetComponent<GridLayoutGroup>();
		Rect rect = gameobject.GetComponent<RectTransform>().rect;
		float cell_width = rect.width / nb_columns;
		float cell_height = rect.height / nb_lines;

		/* Either the height or the width is adjusted to fit the asked proportion */
		if ( cell_width * width_to_height_ratio < cell_height )
			cell_height = cell_width * width_to_height_ratio;
		else
			cell_width = cell_height / width_to_height_ratio;

		layout.constraintCount = nb_columns;
		/* percentage_cells indicates the proportion of the layout occupied by its cells */
		layout.cellSize = new Vector2(cell_width, cell_height) * percentage_cells;

		if ( nb_columns > 1 && nb_lines > 1)
		{
			/* percentage_interior_vs_exterior_spacing indicates the proportion of remaining space allocated into interior as opposed to exterior spaces */
			float spacing_width = (rect.width - (layout.cellSize.x * nb_columns)) * percentage_interior_vs_exterior_spacing / (nb_columns - 1);
			float spacing_height = (rect.height - (layout.cellSize.y * nb_lines)) * percentage_interior_vs_exterior_spacing / (nb_lines - 1);
			layout.spacing = new Vector2(spacing_width, spacing_height);
		}
	}

	public static int initializeGridLayoutParametersFixedNbLines(
		GameObject gameobject,
		int nb_lines,
		float width_to_height_ratio,
		float percentage_cells,
		float percentage_interior_vs_exterior_spacing
	) {
		GridLayoutGroup layout = gameobject.GetComponent<GridLayoutGroup>();
		Rect rect = gameobject.GetComponent<RectTransform>().rect;
		float cell_height = rect.height / nb_lines;
		float cell_width = cell_height / width_to_height_ratio;
		int nb_columns = Mathf.RoundToInt(rect.width / cell_width);

		layout.constraint = GridLayoutGroup.Constraint.FixedRowCount;
		layout.constraintCount = nb_lines;
		/* percentage_cells indicates the proportion of the layout occupied by its cells */
		layout.cellSize = new Vector2(cell_width, cell_height) * percentage_cells;

		if ( nb_columns > 1 && nb_lines > 1)
		{
			/* percentage_interior_vs_exterior_spacing indicates the proportion of remaining space allocated into interior as opposed to exterior spaces */
			float spacing_width = (rect.width - (layout.cellSize.x * nb_columns)) * percentage_interior_vs_exterior_spacing / (nb_columns - 1);
			float spacing_height = (rect.height - (layout.cellSize.y * nb_lines)) * percentage_interior_vs_exterior_spacing / (nb_lines - 1);
			if ( spacing_width > spacing_height / width_to_height_ratio)
				spacing_width = spacing_height / width_to_height_ratio;
			else
				spacing_height = spacing_width * width_to_height_ratio;
			layout.spacing = new Vector2(spacing_width, spacing_height);
		}
		return nb_columns;
	}

	public static void flipImage(Image image)
	{
		image.transform.localRotation = new Quaternion(0, 1, 0, 0);
	}
}