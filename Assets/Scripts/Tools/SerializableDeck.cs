using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SerializableDeck
{
	public enum e_deck_mode
	{
		None = 0,
		Classic = 1,
		CombatTrick = 1 << 1
	};

	public const int				max_nb_generals = 2;
	public const int				max_nb_combat_tricks = 2;

	/* Attributes can't be private or readonly so that the struct can be serialized */
	public string					_name;
	public int						_nb_cards = 0;
	public List<int>				_list_factions = null;
	public List<string>				_generals_prefab_path = new List<string>();
	public List<string>				_combat_tricks_prefab_path = new List<string>();
	public Dictionary<string, int>	_cards_prefab_path = new Dictionary<string, int>();
	public string					_notes;
	public e_deck_mode				_deck_mode;

	public SerializableDeck(string deck_name,
		                    List<CollectionDeckCard> generals,
		                    List<CollectionDeckCard> combat_tricks,
		                    Dictionary<string, CollectionDeckCard> cards,
		                    string notes)
	{
		if ( cards == null )
			return;

		_name = deck_name;
		List<int> list_factions = new List<int>();
		foreach (CollectionDeckCard general in generals)
		{
			list_factions.Add(general.CardStats.Factions.FirstFaction);
			_generals_prefab_path.Add(general.CardStats.PrefabPath);
		}
		foreach (CollectionDeckCard combat_trick in combat_tricks)
		{
			_combat_tricks_prefab_path.Add(combat_trick.CardStats.PrefabPath);
		}
		foreach (CollectionDeckCard card in cards.Values)
		{
			if ( !list_factions.Contains(card.CardStats.Factions.FirstFaction) )
				list_factions.Add(card.CardStats.Factions.FirstFaction);
			if ( !list_factions.Contains(card.CardStats.Factions.SecondFaction) )
				list_factions.Add(card.CardStats.Factions.SecondFaction);
			_cards_prefab_path.Add(card.CardStats.PrefabPath, card.NbCopies);
			_nb_cards += card.NbCopies;
		}
		if ( list_factions.Count > 0 )
			_list_factions = list_factions;
		_notes = notes;
		_deck_mode = get_deck_mode(generals, combat_tricks, _nb_cards, new Factions(list_factions));
	}

	private e_deck_mode get_deck_mode(List<CollectionDeckCard> generals,
				                      List<CollectionDeckCard> combat_tricks,
				                      int nb_cards,
				                      Factions deck_factions)
	{
		e_deck_mode validity = e_deck_mode.None;
		if ( generals.Count == SerializableDeck.max_nb_generals && nb_cards == 40 )
		{
			validity = e_deck_mode.Classic;
			if ( combat_tricks.Count == SerializableDeck.max_nb_combat_tricks
				 && combat_tricks.All(combat_trick =>
				 	                  combat_trick.CardStats.Factions.doShareFaction(deck_factions)) )
				validity |= e_deck_mode.CombatTrick;
		}
		return validity;
	}
}