using UnityEngine;

public abstract class ActivatedAbility
{
    protected Character   _character;

	public abstract string Cost { get; }
	public abstract string Name { get; }

	public abstract bool payCost();
	public abstract void doEffect();

    public ActivatedAbility(Character character)
    {
        _character = character;
    }

	/* For abilities that have effects under certain conditions */
	public virtual void onTrigger(Trigger trigger) {}

    public virtual bool activate()
    {
        if ( !this.payCost() )
            return false;

        this.doEffect();
        Trigger.activate(Trigger.e_type.Activated_ability, source : _character);
        return true;
    }
}