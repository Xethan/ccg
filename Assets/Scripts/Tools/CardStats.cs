using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CardStats : MonoBehaviour
{
	public enum e_card_type
	{
		General,
		Artifact,
		Unit,
		Spell,
		CombatTrick,
	};

	[SerializeField] private Sprite			_sprite;
	[SerializeField] private string			_name;
	[SerializeField] private List<string>   _related_cards_prefab_path;
	[SerializeField] private List<string>   _related_tiles_prefab_path;
	[SerializeField] private List<int>		_raw_factions;
	[SerializeField] private int[]			_raw_mana_cost;
	[SerializeField] private e_card_type	_card_type;
	[SerializeField] private string			_text;
	[SerializeField] private int			_attack;
	[SerializeField] private int			_health;

	private Factions						_factions;
	private ManaCost						_mana_cost;

	public ManaCost ManaCost { get { return _mana_cost; } }
	public Sprite Sprite { get { return _sprite; } }
	public string Name { get { return _name; } }
	public List<string> RelatedCards { get { return _related_cards_prefab_path; } }
	public List<string> RelatedTiles { get { return _related_tiles_prefab_path; } }
	public Factions Factions { get { return _factions; } }
	public e_card_type CardType { get { return _card_type; } }
	public string Text { get { return _text; } }
	public int Attack { get { return _attack; } }
	public int Health { get { return _health; } }
	public string PrefabPath { get { return _factions.Path + "/" + _name; } }

	void Awake()
	{
		_factions = new Factions(_raw_factions);
		_mana_cost = new ManaCost(_raw_mana_cost);
	}
}