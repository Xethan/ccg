using System;

public struct Trigger
{
	public enum e_type
	{
		Activated_ability,
		Armor,
		Artifact,
		Attack,
		Backstab,
		Battle_cry,
		Buff,
		Combat_trick,
		Consume,
		Counter_attack,
		Damage,
		Death,
		Dispell,
		Draw,
		End_turn,
		Flanking,
		Heal,
		Moonrise,
		Movement,
		Play,
		Pull,
		Push,
		Push_damage,
		Spell,
		Start_turn,
		Summon,
		Sunrise,
		Teleportation,
		Tile_proc,
		Trample,
		Xp_gain,
	}

	public readonly e_type		type;
	public readonly GameElement	source;
	public readonly Character	receiver;
	public readonly Character   receiver2;
	public readonly Player		player;
	public readonly Tile		tile;
	public readonly Tile		tile2;
	public readonly int			amount;
	public readonly Modifier    modifier;

	public Trigger(e_type type, GameElement source=null, Character receiver=null, Character receiver2=null, Player player=null,
		           Tile tile=null, Tile tile2=null, int amount=0, Modifier modifier=null)
	{
		this.type = type;
		this.source = source;
		this.receiver = receiver;
		this.receiver2 = receiver2;
		this.player = player;
		this.tile = tile;
		this.tile2 = tile2;
		this.amount = amount;
		this.modifier = modifier;
	}

	public bool Is(string trigger_type)
	{
		return (Trigger.e_type)Enum.Parse(typeof(Trigger.e_type), trigger_type) == this.type;
	}

	public static Trigger activate(e_type type, GameElement source=null, Character receiver=null, Character receiver2=null,
								Player player=null, Tile tile=null, Tile tile2=null, int amount=0,  Modifier modifier=null)
	{
		Trigger trigger = new Trigger(type, source, receiver, receiver2, player, tile, tile2, amount, modifier);
		Trigger.propagate(trigger);
		return trigger;
	}

	public static void propagate(Trigger trigger)
	{
		Board.getCurrentPlayer().onTrigger(trigger);
		Board.getEnnemyPlayer().onTrigger(trigger);
		foreach (Tile current_tile in Board._board)
		{
			Character character = current_tile.Character;
			if ( character != null )
				character.onTrigger(trigger);
			current_tile.onTrigger(trigger);
		}
	}
}