using UnityEngine;
using System;
using System.Collections.Generic;

public class TriggerQueue : MonoBehaviour
{
    public static TriggerQueue  instance;
    private bool                _is_func_running = false;
    private Queue<Trigger>      _triggers = new Queue<Trigger>();
    private Queue<Action>       _callback_functions = new Queue<Action>();

    void Awake()
    {
        instance = this;
    }

    private void checkQueue()
    {
        if ( _is_func_running == true
             || (_triggers.Count == 0
                 && _callback_functions.Count == 0) )
            return;

        _is_func_running = true;

        if ( _triggers.Count > 0 )
        {
            Trigger trigger = _triggers.Dequeue();
            Trigger.propagate(trigger);
        }
        else if ( _callback_functions.Count > 0 )
        {
            Action callback_function = _callback_functions.Dequeue();
            callback_function();
        }

        _is_func_running = false;

        checkQueue();
    }

    public void newTrigger(Trigger trigger, Action callback_function=null)
    {
        _triggers.Enqueue(trigger);
        if ( callback_function != null )
            _callback_functions.Enqueue(callback_function);
        checkQueue();
    }
}