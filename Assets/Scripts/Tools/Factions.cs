using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class Factions
{
	public const int					nb_factions = 6;
	public const int					neutral = 0;
	public const int					faction_1 = 1;
	public const int					faction_2 = 2;
	public const int					faction_3 = 3;
	public const int					faction_4 = 4;

	public static readonly char[]		factions_symbol = {'N', 'G', 'P', 'Y', 'W', '5'};

	public static readonly string[]		_factions_name =
	{
		"Neutrals", "Green", "Purple", "Yellow", "White", "Faction 5"
	};

	private List<int>					_factions;

	public Factions(List<int> factions)
	{
		_factions = factions.Distinct().ToList();
		_factions.Sort();
	}

	public bool isFromFaction(int faction)
	{
		return _factions.Contains(faction);
	}

	public bool doShareFaction(Factions other)
	{
		foreach (int faction in _factions)
		{
			if ( other.isFromFaction(faction) )
				return true;
		}
		return false;
	}

	public string Path
	{
		get
		{
			string faction_path = "";

			for (int i = 0; i < _factions.Count; ++i)
			{
				faction_path += _factions_name[_factions[i]];
				if ( i < _factions.Count - 1 )
					faction_path += " - ";
			}
			return faction_path;
		}
	}

	public Sprite Emblem
	{
		get { return Tools.instantiateSpriteFromPath(this.Path); }
	}

	public Sprite DeckEmblem  // TODO: handle emblems when there is more than 2 factions
	{
		get
		{
			int max_factions = 2;
			string deck_emblem_path = "";

			for (int i = 0; i < _factions.Count; ++i)
			{
				if ( _factions[i] == Factions.neutral)
					continue;

				deck_emblem_path += _factions_name[_factions[i]];
				max_factions -= 1;
				if ( max_factions == 0)
					break;
				if ( i < _factions.Count - 1 )
					deck_emblem_path += " - ";
			}
			return Tools.instantiateSpriteFromPath(deck_emblem_path);
		}
	}

	public int FirstFaction { get { return _factions[0]; } }
	public int SecondFaction
	{
		get { return (_factions.Count == 1) ? _factions[0] : _factions[1]; }
	}
}