using UnityEngine;
using System;
using System.Collections.Generic;

public class EventQueue : MonoBehaviour
{
	public static EventQueue							instance;
	private bool										_is_func_running = false;
	private Queue<Action<Dictionary<string, object>>>	_event_funcs = new Queue<Action<Dictionary<string, object>>>();
	private Queue<Dictionary<string, object>>			_event_funcs_params = new Queue<Dictionary<string, object>>();

	void Awake()
	{
		instance = this;
	}

	private void checkEvent()
	{
		if ( _is_func_running == true || _event_funcs.Count == 0 )
			return;

		Action<Dictionary<string, object>> event_func = _event_funcs.Dequeue();
		Dictionary<string, object> event_func_params = _event_funcs_params.Dequeue();
		_is_func_running = true;
		event_func(event_func_params);
	}

	public void newEvent(Action<Dictionary<string, object>> event_func, Dictionary<string, object> event_func_params)
	{
		_event_funcs.Enqueue(event_func);
		_event_funcs_params.Enqueue(event_func_params);
		checkEvent();
	}

	public void endEvent()
	{
		_is_func_running = false;
		checkEvent();
	}
}