using System.Collections.Generic;

public class Pathing
{
    /*
    ** This function returns an int[board_size] :
    **  - 0 means the corresponding tile is inaccessible
    **  - x (with x > 0) means the tile is accessible by a path of length x
    */
    public static int[] findLengthPathToTiles(Character character)
    {
        int[] accessible_tiles = new int[Board.nb_columns * Board.nb_lines];
        List<Coord> new_coords = new List<Coord>();
        new_coords.Add(character.Coord);
        accessible_tiles = findLengthPathToTilesRecursive(character, accessible_tiles, new_coords, 1, character.Movement);
        /* Removing Allied Characters' coords because you can move through them but not stop on their tiles */
        for (int i = 0; i < accessible_tiles.Length; ++i)
        {
            if ( Board._board[i].Character != null )
                accessible_tiles[i] = 0;
        }
        return accessible_tiles;
    }

    /*
    ** The function starts from the tile of the character whom you want to know all the tiles it can access
    ** For each tile which you know you can access with a path of length x,
    ** you can access all adjacent valid tiles (which you didn't go through already) with a path of length (x + 1)
    ** Repeat the operation until you reach the maximum movement of the unit or all adajacent tiles are invalid
    */
    private static int[] findLengthPathToTilesRecursive(Character character, int[] accessible_tiles, List<Coord> last_coords, int distance_from_start, int movement)
    {
        Coord[] directions = { new Coord(-1, 0), new Coord(1, 0), new Coord(0, -1), new Coord(0, 1) };
        List<Coord> new_coords = new List<Coord>();

        foreach (Coord current_coord in last_coords)
        {
            foreach (Coord direction in directions)
            {
                Coord test_coord = current_coord + direction;

                if ( test_coord.isOutOfBoard() == false
                    && accessible_tiles[test_coord.RawCoord] == 0
                    && movement >= distance_from_start
                    && (Board.getCharacter(test_coord) == null
                        || Board.getCharacter(test_coord).Owner == character.Owner) )
                {
                    accessible_tiles[test_coord.RawCoord] = distance_from_start;
                    if ( new_coords.Contains(test_coord) == false )
                        new_coords.Add(test_coord);
                }
            }
        }
        if ( distance_from_start == movement || new_coords.Count == 0 )
            return accessible_tiles;
        else
            return findLengthPathToTilesRecursive(character, accessible_tiles, new_coords, distance_from_start + 1, movement);
    }

    // Unused function for now
    public static List<Coord> findBestPath(Character character, Coord target_coord)
    {
        // Thanks to the int[], we know that there is a valid path of length x to access target_coord
        int[] length_path_to_tiles = findLengthPathToTiles(character);
        Coord[] directions = { new Coord(-1, 0), new Coord(1, 0), new Coord(0, -1), new Coord(0, 1) };

        List<Coord> path = new List<Coord>(length_path_to_tiles[target_coord.RawCoord]);
        path.Add(target_coord);

        // Starting from the target coord, scan adjacent tiles and add to the path
        //   the only tile that has the right path_length to the character
        for (int distance_to_character = length_path_to_tiles[target_coord.RawCoord] - 1; distance_to_character >= 1; --distance_to_character)
        {
            Coord current_coord = path[path.Count - 1];

            foreach (Coord direction in directions)
            {
                Coord test_coord = current_coord + direction;
                if ( test_coord.isInsideBoard()
                    && length_path_to_tiles[test_coord.RawCoord] == distance_to_character )
                {
                    path.Add(test_coord);
                }
            }
        }

        path.Reverse();
        return path;
    }
}