using System.Collections.Generic;

public static class Test
{
    public static Scenario scenario = null;
}

public class Scenario
{
    public readonly List<CharacterScenario> generals_player_1;
    public readonly List<CharacterScenario> generals_player_2;
    public readonly List<CharacterScenario> characters_player_1;
    public readonly List<CharacterScenario> characters_player_2;
    public readonly List<string> cards_in_hand_prefab_path_player_1;
    public readonly List<string> cards_in_hand_prefab_path_player_2;
    public readonly Dictionary<string, int> cards_prefab_path_in_deck_player_1;
    public readonly Dictionary<string, int> cards_prefab_path_in_deck_player_2;
    public readonly Dictionary<int, int> additional_mana_player_1;
    public readonly Dictionary<int, int> additional_mana_player_2;

    public Scenario(
        List<CharacterScenario> generals_player_1,
        List<CharacterScenario> generals_player_2,
        List<CharacterScenario> characters_player_1,
        List<CharacterScenario> characters_player_2,
        List<string> cards_prefab_path_in_hand_player_1,
        List<string> cards_prefab_path_in_hand_player_2,
        Dictionary<string, int> cards_prefab_path_in_deck_player_1,
        Dictionary<string, int> cards_prefab_path_in_deck_player_2,
        Dictionary<int, int> additional_mana_player_1=null,
        Dictionary<int, int> additional_mana_player_2=null
    ) {
        this.generals_player_1 = generals_player_1;
        this.generals_player_2 = generals_player_2;
        this.characters_player_1 = characters_player_1;
        this.characters_player_2 = characters_player_2;
        this.cards_in_hand_prefab_path_player_1 = cards_prefab_path_in_hand_player_1;
        this.cards_in_hand_prefab_path_player_2 = cards_prefab_path_in_hand_player_2;
        this.cards_prefab_path_in_deck_player_1 = cards_prefab_path_in_deck_player_1;
        this.cards_prefab_path_in_deck_player_2 = cards_prefab_path_in_deck_player_2;
        this.additional_mana_player_1 = additional_mana_player_1;
        this.additional_mana_player_2 = additional_mana_player_2;
    }

    public static void initializeStartingHand(Player player, List<string> cards_in_hand_prefab_path)
    {
        List<Card> cards_in_hand = new List<Card>();
        foreach (string card_prefab_path in cards_in_hand_prefab_path)
        {
            Card card = Tools.instantiateCardFromPath<Card>(card_prefab_path, owner:player);
            card.gameObject.SetActive(false);
            cards_in_hand.Add(card);
        }
        player.initializeStartingHand(cards_in_hand);
    }

    public static void addManaToReserve(ManaReserve mana_reserve, Dictionary<int, int> mana_to_reach)
    {
        if ( mana_to_reach != null )
            foreach (KeyValuePair<int,int> entry in mana_to_reach)
                while ( mana_reserve.ManaMax[entry.Key] < entry.Value )
                    mana_reserve.increaseMana(entry.Key);
    }
}

public struct CharacterScenario
{
    public readonly string prefab_path;
    public readonly Coord coord;

    public CharacterScenario(string prefab_path, Coord coord)
    {
        this.prefab_path = prefab_path;
        this.coord = coord;
    }
}