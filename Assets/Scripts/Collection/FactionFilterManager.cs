using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class FactionFilterManager : MonoBehaviour
{
	[SerializeField]
	private Collection	_collection;
	[SerializeField]
	private Image[]		_buttons;
	private bool[]		_factions_filter = new bool[Factions.nb_factions];

	void Start()
	{
		_factions_filter[Factions.neutral] = true;
		setCollectionFactionFilter();
	}

	public void onClick(int index_faction)
	{
		_factions_filter[index_faction] = !_factions_filter[index_faction];
		setCollectionFactionFilter();
	}

	public void setCurrentDeckFactions(List<CollectionDeckCard> generals, Dictionary<string, CollectionDeckCard> cards)
	{
		if ( generals.Count < 2 )
		{
			for (int i = 0; i < _factions_filter.Length; ++i)
				_factions_filter[i] = true;
		}
		else
		{
			for (int i = 0; i < _factions_filter.Length; ++i)
				_factions_filter[i] = false;
			foreach (CollectionDeckCard general in generals)
				_factions_filter[general.CardStats.Factions.FirstFaction] = true;
			foreach (KeyValuePair<string, CollectionDeckCard> card in cards)
			{
				_factions_filter[card.Value.CardStats.Factions.FirstFaction] = true;
				_factions_filter[card.Value.CardStats.Factions.SecondFaction] = true;
			}
			_factions_filter[Factions.neutral] = true;
		}
		setCollectionFactionFilter();
	}

	private void setCollectionFactionFilter()
	{
		for (int i = 0; i < _factions_filter.Length; ++i)
			_buttons[i].fillCenter = _factions_filter[i];

		Predicate<CardStats> filter = (CardStats card) =>
		{
			for (int i = 0; i < _factions_filter.Length; ++i)
			{
				if ( _factions_filter[i] == false && card.Factions.isFromFaction(i) )
					return false;
			}
			return true;
		};

		_collection.setFilter(filter, Collection.faction_filter);
	}
}