using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CollectionDeckCard : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] private CollectionCard	_collection_card_prefab;
	[SerializeField] private GameObject		_panel_mana_cost_ui;
	[SerializeField] private Text			_card_name;
	[SerializeField] private Image			_card_image;
	[SerializeField] private Text			_text_nb_copies;

	private CollectionDeckBuilder			_deck;
	private ManaCostUI						_mana_cost_ui;

	private CollectionCard					_card_ui;
	private CardStats						_card;
	private int								_nb_copies = 1;

	public CardStats CardStats { get { return _card; } }
	public int NbCopies { get { return _nb_copies; } }

	public void initialize(CollectionDeckBuilder deck, CardStats card)
	{
		_deck = deck;

		_card_ui = GameObject.Instantiate(_collection_card_prefab) as CollectionCard;
		_card_ui.transform.SetParent(this.transform, false);
		_card_ui.initialize(card);
		_card_ui.gameObject.SetActive(false);

		_card = card;
		_card.transform.SetParent(this.transform, false);
		_card_name.text = card.Name;
		_card_image.sprite = card.Sprite;

		_mana_cost_ui = Tools.instantiateFromPath("Game UI/Mana Cost UI").GetComponent<ManaCostUI>();
		_mana_cost_ui.transform.SetParent(_panel_mana_cost_ui.transform, false);
		_mana_cost_ui.updateUI(card.ManaCost);

		updateTextNbCopies();
	}

	public void addCopy()
	{
		_nb_copies += 1;
		updateTextNbCopies();
	}

	private void removeCopy()
	{
		_nb_copies -= 1;
		if ( _nb_copies == 0 )
		{
			_card_ui.transform.SetParent(this.transform, false);
			_card_ui.gameObject.SetActive(false);
			_deck.removeCard(this);
			GameObject.Destroy(this.gameObject);
		}
		updateTextNbCopies();
		_deck.updateUI();
	}

	private void updateTextNbCopies()
	{
		_text_nb_copies.text = _nb_copies.ToString();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
			removeCopy();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_card_ui.transform.SetParent(_deck.CardHoverPanel.transform, false);
		_card_ui.gameObject.SetActive(true);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_card_ui.transform.SetParent(this.transform, false);
		_card_ui.gameObject.SetActive(false);
	}

	public void OnBeginDrag(PointerEventData eventData) {}

	public void OnDrag(PointerEventData eventData) {}

	public void OnEndDrag(PointerEventData eventData)
	{
		List<RaycastResult> hits = new List<RaycastResult>();
		bool did_hit_deck = false;

		EventSystem.current.RaycastAll(eventData, hits);
		foreach (RaycastResult hit in hits)
		{
			CollectionDeckManager collection_deck_manager = hit.gameObject.GetComponent<CollectionDeckManager>();

			if ( collection_deck_manager != null )
			{
				did_hit_deck = true;
				break;
			}
		}
		if ( did_hit_deck == false )
			removeCopy();
	}
}