using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System; // Exceptions
using Newtonsoft.Json;

public class CollectionDeckManager : MonoBehaviour
{
	public static CollectionDeckManager				instance;
	public static readonly string					json_path = "json.txt";

	[SerializeField] private CollectionDeckInfo		_deck_info_prefab;
	[SerializeField] private ScrollRect				_scroll_view_decks;
	[SerializeField] private GameObject				_panel_decks;
	[SerializeField] private CollectionDeckBuilder	_deck_ui;

	private CollectionDeckInfo						_current_deck;
	private List<CollectionDeckInfo>				_deck_infos = new List<CollectionDeckInfo>();

	void Awake()
	{
		instance = this;

		string decks_json = "[]";
		/* If the file can't be accessed, the user just won't be able to use his old decks */
		try
		{
			decks_json = File.ReadAllText(CollectionDeckManager.json_path);
		}
		catch (Exception exception)
		{
			Debug.Log(exception);
		}
		List<SerializableDeck> serializable_decks = JsonConvert.DeserializeObject<List<SerializableDeck>>(decks_json);
		for (int i = 0; i < serializable_decks.Count; ++i)
		{
			CollectionDeckInfo deck_info = GameObject.Instantiate(_deck_info_prefab) as CollectionDeckInfo;
			deck_info.transform.SetParent(_panel_decks.transform, false);
			deck_info.Deck = serializable_decks[i];
			_deck_infos.Add(deck_info);
		}
	}

	public void openDeck(CollectionDeckInfo deck)
	{
		if ( deck == null )
			_deck_ui.loadDeck(new SerializableDeck(null, null, null, null, null));
		else
			_deck_ui.loadDeck(deck.Deck);

		_current_deck = deck;
		_deck_ui.gameObject.SetActive(true);
		_scroll_view_decks.gameObject.SetActive(false);
	}

	public void newDeck()
	{
		openDeck(null);
	}

	public void saveDeck(SerializableDeck serializable_deck)
	{
		if ( _current_deck == null )
			saveNewDeck(serializable_deck);
		else
			_current_deck.Deck = serializable_deck;

		writeJsonToFile();
	}

	private void saveNewDeck(SerializableDeck serializable_deck)
	{
		CollectionDeckInfo new_deck_info = GameObject.Instantiate(_deck_info_prefab) as CollectionDeckInfo;

		new_deck_info.transform.SetParent(_panel_decks.transform, false);
		new_deck_info.Deck = serializable_deck;

		_deck_infos.Add(new_deck_info);
	}

	public void closeDeck()
	{
		_deck_ui.gameObject.SetActive(false);
		_scroll_view_decks.gameObject.SetActive(true);
		_current_deck = null;
	}

	public void destroyDeck(CollectionDeckInfo deck)
	{
		_deck_infos.Remove(deck);
		writeJsonToFile();
	}

	public void addCard(CardStats card_stats)
	{
		if ( _deck_ui.gameObject.activeSelf == true )
			_deck_ui.addCard(card_stats);
	}

	private void writeJsonToFile()
	{
		List<SerializableDeck> serializable_decks = new List<SerializableDeck>();
		JsonSerializer serializer = new JsonSerializer();
		serializer.Formatting = Formatting.Indented;

		foreach (CollectionDeckInfo deck_info in _deck_infos)
			serializable_decks.Add(deck_info.Deck);

		using ( StreamWriter stream_writer = new StreamWriter(json_path) )
		using ( JsonWriter writer = new JsonTextWriter(stream_writer) )
		{
			serializer.Serialize(writer, serializable_decks);
		}
	}
}