using UnityEngine;
using UnityEngine.UI;
using System;

public class TextFilterManager : MonoBehaviour
{
	[SerializeField] private Collection	_collection;
	[SerializeField] private InputField	_input_field;

	void Awake()
	{
		_input_field.onValueChanged.AddListener(setCollectionFilter);
	}

	public void setCollectionFilter(string text)
	{
		Predicate<CardStats> filter = (CardStats card) =>
		{
			// Case insensitive Contains()
			return card.Name.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0
				|| card.Text.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0;
		};

		_collection.setFilter(filter, Collection.card_text_filter);
	}
}