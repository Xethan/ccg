using UnityEngine;
using UnityEngine.UI;

public class CollectionDeckBuilderInfo : MonoBehaviour
{
	[SerializeField] private DeckThumbnail	_deck_thumbnail;

	// Mana Curve UI

	public void updateUI(SerializableDeck deck)
	{
		_deck_thumbnail.updateUI(deck);
	}
}