using UnityEngine;
using UnityEngine.UI;
using System; // Predicates
using System.Collections.Generic;

public class Collection : MonoBehaviour
{
	private int						_nb_cards_per_page = 8;
	private int						_nb_cards_per_line = 4;
	private int						_nb_lines = 2;
	public const int				faction_filter = 0;
	public const int				card_type_filter = 1;
	public const int				card_text_filter = 2;
	public const int				mana_cost_filter = 3;

	[SerializeField]
	private CollectionCard			_collection_card_prefab;
	[SerializeField]
	private List<CardStats>			_cards_prefab;

	private List<CollectionCard>	_cards = new List<CollectionCard>();
	private List<CollectionCard>	_cards_after_filters = new List<CollectionCard>();
	private int						_current_page = 0;

	private Predicate<CardStats>[]	_filters = new Predicate<CardStats>[4];

	public void setFilter(Predicate<CardStats> filter, int filter_id)
	{
		_filters[filter_id] = filter;

		_cards_after_filters.Clear();
		for (int i = 0; i < _cards.Count; ++i)
		{
			CollectionCard card = _cards[i];
			if ( shouldShow(card.CardStats) )
				_cards_after_filters.Add(card);
		}

		_current_page = 0;
		hideCards();
		showCardsCurrentPage();
	}

	void Awake()
	{
		List<CardStats> cards_stats = new List<CardStats>();
		foreach (CardStats card_prefab in _cards_prefab)
		{
			CardStats card_stats = GameObject.Instantiate(card_prefab) as CardStats;
			card_stats.transform.SetParent(this.transform, false);
			cards_stats.Add(card_stats);
		}
		/* Generals sorted by faction, then Cards sorted by mana cost and then by name */
		cards_stats.Sort(delegate(CardStats card1, CardStats card2)
		{
			if ( card1.CardType == CardStats.e_card_type.General
				|| card2.CardType == CardStats.e_card_type.General )
			{
				if ( (int)card1.CardType == (int)card2.CardType )
					return card1.Factions.FirstFaction - card2.Factions.FirstFaction;
				else
					return (int)card1.CardType - (int)card2.CardType;
			}
			if ( card1.ManaCost.Converted != card2.ManaCost.Converted )
				return card1.ManaCost.Converted - card2.ManaCost.Converted;
			return card1.Name.CompareTo(card2.Name);
		});
		foreach (CardStats card_stats in cards_stats)
		{
			CollectionCard card = GameObject.Instantiate(_collection_card_prefab) as CollectionCard;
			card.transform.SetParent(this.transform, false);
			card.initialize(card_stats);
			card.gameObject.SetActive(false);
			_cards.Add(card);
		}

		_cards_after_filters.AddRange(_cards);
		for (int i = 0; i < _filters.Length; ++i)
			setFilter(null, i);
	}

	void Start()
	{
		_nb_cards_per_line = Tools.initializeGridLayoutParametersFixedNbLines(
			this.gameObject,
			_nb_lines,
			HoverPanel.width_to_height_ratio,
			percentage_cells:0.9f,
			percentage_interior_vs_exterior_spacing:0.5f
		);
		_nb_cards_per_page = _nb_cards_per_line * _nb_lines;
		showCardsCurrentPage();
	}

	private void hideCards()
	{
		for (int i = 0; i < _cards.Count; ++i)
			_cards[i].gameObject.SetActive(false);
	}

	private void showCardsCurrentPage()
	{
		int i = 0;
		int nb_cards_found = 0;

		while (nb_cards_found < _nb_cards_per_page
				&& getCardIndex(i) < _cards_after_filters.Count)
		{
			CollectionCard card = _cards_after_filters[getCardIndex(i)];
			if ( shouldShow(card.CardStats) )
			{
				card.gameObject.SetActive(true);
				++nb_cards_found;
			}
			++i;
		}
	}

	private bool shouldShow(CardStats card)
	{
		for (int i = 0; i < _filters.Length; ++i)
		{
			if ( _filters[i] != null && _filters[i](card) == false )
				return false;
		}
		return true;
	}

	public void previousPage()
	{
		hideCards();
		if ( _current_page > 0 )
			_current_page -= 1;
		showCardsCurrentPage();
	}

	public void nextPage()
	{
		hideCards();
		if ( (_current_page + 1) * _nb_cards_per_page < _cards_after_filters.Count )
			_current_page += 1;
		showCardsCurrentPage();
	}

	private int getCardIndex(int index_on_page)
	{
		return _current_page * _nb_cards_per_page + index_on_page;
	}
}