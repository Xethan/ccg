using UnityEngine;
using UnityEngine.UI;
using System; // Predicate
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class CollectionDeckBuilder : MonoBehaviour
{
	[SerializeField] private Collection					_collection;
	[SerializeField] private CollectionDeckManager		_deck_manager;
	[SerializeField] private FactionFilterManager		_faction_filter_manager;

	[SerializeField] private CollectionDeckBuilderInfo	_deck_builder_info;
	[SerializeField] private InputField					_input_field_deck_name;
	[SerializeField] private InputField					_input_field_deck_notes;
	[SerializeField] private ScrollRect					_scroll_rect;
	[SerializeField] private GameObject					_panel_cards;
	[SerializeField] private HoverPanel					_deck_card_hover_panel;
	[SerializeField] private CollectionDeckCard			_collection_deck_card_prefab;

	private const int									_max_nb_copies_per_deck = 3;

	private List<CollectionDeckCard>					_generals = new List<CollectionDeckCard>();
	private List<CollectionDeckCard>					_combat_tricks = new List<CollectionDeckCard>();
	private Dictionary<string, CollectionDeckCard>		_cards = new Dictionary<string, CollectionDeckCard>();

	private SerializableDeck SerializableDeck
	{
		get { return new SerializableDeck(_input_field_deck_name.text, _generals, _combat_tricks, _cards, _input_field_deck_notes.text); }
	}
	public HoverPanel CardHoverPanel { get { return _deck_card_hover_panel; } }

	public void addCard(CardStats card)
	{
		string card_name = card.Name;

		if ( _input_field_deck_notes.gameObject.activeSelf == true )
			return;

		if ( card.CardType == CardStats.e_card_type.General )
		{
			_generals.Add(createDeckCard(card.PrefabPath));
			setCollectionFilter();
		}
		else if ( card.CardType == CardStats.e_card_type.CombatTrick )
		{
			_combat_tricks.Add(createDeckCard(card.PrefabPath));
			setCollectionFilter();
		}
		else if ( _cards.ContainsKey(card_name) )
		{
			if ( _cards[card_name].NbCopies < _max_nb_copies_per_deck)
				_cards[card_name].addCopy();
		}
		else
			_cards[card_name] = createDeckCard(card.PrefabPath);

		orderCards();
		updateUI();
	}

	public void removeCard(CollectionDeckCard card)
	{
		if ( card.CardStats.CardType == CardStats.e_card_type.General )
		{
			_generals.Remove(card);
			setCollectionFilter();
		}
		else if ( card.CardStats.CardType == CardStats.e_card_type.CombatTrick )
		{
			_combat_tricks.Remove(card);
			setCollectionFilter();
		}
		else
			_cards.Remove(card.CardStats.Name);
	}

	public void updateUI()
	{
		_deck_builder_info.updateUI(this.SerializableDeck);
	}

	public void toggleNotesDisplay()
	{
		_input_field_deck_notes.gameObject.SetActive(!_input_field_deck_notes.gameObject.activeSelf);
	}

	public void loadDeck(SerializableDeck deck)
	{
		if ( deck._name != null )
			_input_field_deck_name.text = deck._name;
		if ( deck._notes != null )
			_input_field_deck_notes.text = deck._notes;

		foreach (string prefab_path in deck._generals_prefab_path)
			_generals.Add(createDeckCard(prefab_path));
		foreach (string prefab_path in deck._combat_tricks_prefab_path)
			_combat_tricks.Add(createDeckCard(prefab_path));

		foreach (KeyValuePair<string, int> card in deck._cards_prefab_path)
		{
			CollectionDeckCard deck_card = createDeckCard(card.Key);
			_cards[deck_card.CardStats.Name] = deck_card;
			for (int i = 1; i < card.Value; ++i)
				_cards[deck_card.CardStats.Name].addCopy();
		}

		_scroll_rect.verticalNormalizedPosition = 1.0f;
		orderCards();
		updateUI();
		setCollectionFilter();
	}

	private void setCollectionFilter()
	{
		Predicate<CardStats> filter;
		filter = (CardStats card) =>
		{
			if ( card.CardType == CardStats.e_card_type.General )
				return _generals.Count < SerializableDeck.max_nb_generals
				       && !_generals.Any(general => general.CardStats.Name == card.Name);
			else if ( card.CardType == CardStats.e_card_type.CombatTrick )
				return _combat_tricks.Count < SerializableDeck.max_nb_combat_tricks
				       && !_combat_tricks.Any(combat_trick => combat_trick.CardStats.Name == card.Name);
			else if ( card.CardType == CardStats.e_card_type.Unit
				      || card.CardType == CardStats.e_card_type.Spell
				      || card.CardType == CardStats.e_card_type.Artifact )
				return true;
			else
				return false;
		};
		_faction_filter_manager.setCurrentDeckFactions(_generals, _cards);
		_collection.setFilter(filter, Collection.card_type_filter);
	}

	public void saveDeck()
	{
		SerializableDeck serializable_deck = this.SerializableDeck;

		if ( _input_field_deck_name.text != "" )
		{
			_deck_manager.saveDeck(serializable_deck);
			closeDeck();
		}
	}

	public void closeDeck()
	{
		_input_field_deck_name.text = "";
		_input_field_deck_notes.text = "";
		_input_field_deck_notes.gameObject.SetActive(false);

		foreach (CollectionDeckCard general_deck_card in _generals)
			GameObject.Destroy(general_deck_card.gameObject);
		foreach (CollectionDeckCard combat_trick_deck_card in _combat_tricks)
			GameObject.Destroy(combat_trick_deck_card.gameObject);
		foreach (CollectionDeckCard card in _cards.Values)
			GameObject.Destroy(card.gameObject);
		_generals.Clear();
		_combat_tricks.Clear();
		_cards.Clear();

		_deck_manager.closeDeck();
		_collection.setFilter(null, Collection.card_type_filter);
	}

	private CollectionDeckCard createDeckCard(string card_prefab_path)
	{
		GameObject gameobject = Tools.instantiateFromPath(card_prefab_path + " Stats");
		CardStats card = gameobject.GetComponent<CardStats>();

		CollectionDeckCard collection_deck_card = GameObject.Instantiate(_collection_deck_card_prefab) as CollectionDeckCard;
		collection_deck_card.transform.SetParent(_panel_cards.transform, false);
		collection_deck_card.initialize(this, card);
		return collection_deck_card;
	}

	private void orderCards()
	{
		List<CollectionDeckCard> cards = new List<CollectionDeckCard>(_cards.Values);

		cards.Sort(delegate(CollectionDeckCard card1, CollectionDeckCard card2)
		{
			if ( card1.CardStats.ManaCost.Converted != card2.CardStats.ManaCost.Converted )
				return card1.CardStats.ManaCost.Converted - card2.CardStats.ManaCost.Converted;
			else
				return card1.CardStats.Name.CompareTo(card2.CardStats.Name);
		});
		for (int i = 0; i < _generals.Count; ++i)
			_generals[i].transform.SetSiblingIndex(i);
		for (int i = 0; i < _combat_tricks.Count; ++i)
			_combat_tricks[i].transform.SetSiblingIndex(_generals.Count + i);
		int sibling_index_offset = _generals.Count + _combat_tricks.Count;
		for (int i = 0; i < cards.Count; ++i)
			_cards[cards[i].CardStats.Name].transform.SetSiblingIndex(sibling_index_offset + i);
	}
}