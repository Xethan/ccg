using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CollectionDeckInfo : MonoBehaviour, IPointerClickHandler
{
	private SerializableDeck	_deck;

	[SerializeField] private DeckThumbnail	_deck_thumbnail;

	public SerializableDeck Deck
	{
		get
		{
			return _deck;
		}
		set
		{
			_deck = value;
			_deck_thumbnail.updateUI(value);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
			CollectionDeckManager.instance.openDeck(this);
	}

	public void onClickTrash()
	{
		CollectionDeckManager.instance.destroyDeck(this);
		GameObject.Destroy(this.gameObject);
	}
}