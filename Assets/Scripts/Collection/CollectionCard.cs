using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CollectionCard : CardStatsUI, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
			CollectionDeckManager.instance.addCard(_card_stats);
		if ( eventData.button == PointerEventData.InputButton.Right )
			RelatedCardsPanel.instance.openRelatedCardsPanel(_card_stats.RelatedCards,
				                                             _card_stats.RelatedTiles);
	}

	public void OnPointerEnter(PointerEventData eventData) {}

	public void OnPointerExit(PointerEventData eventData) {}

	public void OnBeginDrag(PointerEventData eventData) {}

	public void OnDrag(PointerEventData eventData) {}

	public void OnEndDrag(PointerEventData eventData)
	{
		List<RaycastResult> hits = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventData, hits);
		foreach (RaycastResult hit in hits)
		{
			CollectionDeckManager collection_deck_manager = hit.gameObject.GetComponent<CollectionDeckManager>();

			if ( collection_deck_manager != null )
			{
				collection_deck_manager.addCard(_card_stats);
				break;
			}
		}
	}
}