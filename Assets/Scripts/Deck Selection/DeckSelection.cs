using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System; // Exceptions
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class DeckSelection : MonoBehaviour
{
	public static DeckSelection			instance;
	public static SerializableDeck[]	player_decks = new SerializableDeck[2];

	private static readonly int			nb_decks_per_page = 12;
	private static readonly int			nb_decks_per_line = 4;
	private static readonly float		width_to_height_ratio = 0.5f;

	[SerializeField]
	private LobbyDeck					_lobby_deck_prefab;
	[SerializeField]
	private LobbyPlayersInfo			_players_info;

	private List<LobbyDeck>				_decks = new List<LobbyDeck>();
	private int							_current_page = 0;

	void Awake()
	{
		instance = this;
		Tools.initializeGridLayoutParameters(this.gameObject, nb_decks_per_line, nb_decks_per_page / nb_decks_per_line, width_to_height_ratio, 0.9f, 0.75f);
		DeckSelection.player_decks[0] = null;
		DeckSelection.player_decks[1] = null;

		instantiateDecks();
		showDecksCurrentPage();

		Test.scenario = null;
	}

	public void setUpTestGame(string deck_name_player_1, string deck_name_player_2)
	{
		for (int i = 0; i < _decks.Count; ++i)
		{
			if ( _decks[i].Deck._name == deck_name_player_1 )
				playerSelects(_decks[i].Deck, 0);
			if ( _decks[i].Deck._name == deck_name_player_2 )
				playerSelects(_decks[i].Deck, 1);
		}
		play();
	}

	public void setUpScenario(Scenario scenario)
	{
		Test.scenario = scenario;
		SceneManager.LoadScene("Scenes/Main Game");
	}

	public void instantiateDecks()
	{
		string decks_json = "[]";
		/* If the file can't be accessed, the user just won't be able to use his decks */
		try
		{
			decks_json = File.ReadAllText(CollectionDeckManager.json_path);
		}
		catch (Exception exception)
		{
			Debug.Log(exception);
		}
		List<SerializableDeck> savable_decks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SerializableDeck>>(decks_json);
		for (int i = 0; i < savable_decks.Count; ++i)
		{
			LobbyDeck deck = GameObject.Instantiate(_lobby_deck_prefab) as LobbyDeck;
			deck.transform.SetParent(this.transform, false);
			deck.gameObject.SetActive(false);
			deck.initialize(this, savable_decks[i]);
			_decks.Add(deck);
		}
	}

	public void playerSelects(SerializableDeck deck, int player)
	{
		if ( deck._deck_mode != SerializableDeck.e_deck_mode.None )
		{
			DeckSelection.player_decks[player] = deck;
			_players_info.updateUI(player);
		}
	}

	public void play()
	{
		if ( DeckSelection.player_decks[0] != null && DeckSelection.player_decks[1] != null
			&& DeckSelection.player_decks[0]._deck_mode == DeckSelection.player_decks[1]._deck_mode )
			SceneManager.LoadScene("Scenes/Main Game");
	}

	private void hideDecksCurrentPage()
	{
		for (int i = 0; i < DeckSelection.nb_decks_per_page && getDeckIndex(i) < _decks.Count; ++i)
			_decks[getDeckIndex(i)].gameObject.SetActive(false);
	}

	// Parameter bool should_show_or_hide ?
	private void showDecksCurrentPage()
	{
		for (int i = 0; i < DeckSelection.nb_decks_per_page && getDeckIndex(i) < _decks.Count; ++i)
			_decks[getDeckIndex(i)].gameObject.SetActive(true);
	}

	public void previousPage()
	{
		hideDecksCurrentPage();
		if ( _current_page > 0 )
			_current_page -= 1;
		showDecksCurrentPage();
	}

	public void nextPage()
	{
		if ( (_current_page + 1) * DeckSelection.nb_decks_per_page < _decks.Count )
		{
			hideDecksCurrentPage();
			_current_page += 1;
			showDecksCurrentPage();
		}
	}

	private int getDeckIndex(int index_on_page)
	{
		return _current_page * DeckSelection.nb_decks_per_page + index_on_page;
	}
}
