using UnityEngine;
using UnityEngine.UI;

public class LobbyPlayersInfo : MonoBehaviour
{
	[SerializeField] private LobbyPlayerDeckInfo	_player_deck_info_prefab;
	[SerializeField] private GameObject		_panel_player_1_info;
	[SerializeField] private GameObject		_panel_player_2_info;

	private LobbyPlayerDeckInfo[]				_player_deck_info = new LobbyPlayerDeckInfo[2];

	void Awake()
	{
		_player_deck_info[0] = GameObject.Instantiate(_player_deck_info_prefab) as LobbyPlayerDeckInfo;
		_player_deck_info[0].initialize(0);
		_player_deck_info[0].transform.SetParent(_panel_player_1_info.transform, false);

		_player_deck_info[1] = GameObject.Instantiate(_player_deck_info_prefab) as LobbyPlayerDeckInfo;
		_player_deck_info[1].initialize(1);
		_player_deck_info[1].transform.SetParent(_panel_player_2_info.transform, false);
	}

	public void updateUI(int index_player)
	{
		_player_deck_info[index_player].updateUI();
	}
}