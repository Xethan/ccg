using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class LobbyDeck : MonoBehaviour, IPointerClickHandler
{
	[SerializeField] private DeckThumbnail	_deck_thumbnail;
	[SerializeField] private Image			_indicator_invalid_deck;

	private DeckSelection					_decks_panel;
	private SerializableDeck				_deck;

	private readonly Color					_red_color = new Color32(200, 50, 50, 255);
	private readonly Color					_green_color = new Color32(50, 200, 50, 255);

	public SerializableDeck Deck { get { return _deck; } }

	public void initialize(DeckSelection decks_panel, SerializableDeck deck)
	{
		_decks_panel = decks_panel;
		_deck = deck;
		updateUI();
	}

	private void updateUI()
	{
		_deck_thumbnail.updateUI(_deck);

		if ( _deck._deck_mode != SerializableDeck.e_deck_mode.None )
			_indicator_invalid_deck.color = _green_color;
		else
			_indicator_invalid_deck.color = _red_color;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if ( eventData.button == PointerEventData.InputButton.Left )
			_decks_panel.playerSelects(_deck, 0);
		else if ( eventData.button == PointerEventData.InputButton.Right )
			_decks_panel.playerSelects(_deck, 1);
	}
}