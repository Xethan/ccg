using UnityEngine;
using UnityEngine.UI;

public class LobbyPlayerDeckInfo : MonoBehaviour
{
	[SerializeField] private DeckThumbnail	_deck_thumbnail;
	[SerializeField] private Text			_player_name;

	private int								_player_index;

	public void initialize(int player_index)
	{
		_player_index = player_index;
		updateUI();
	}

	public void updateUI()
	{
		SerializableDeck deck = DeckSelection.player_decks[_player_index];
		
		_player_name.text = "Player " + (_player_index + 1).ToString();
		this.gameObject.SetActive(deck != null);
		if ( deck != null )
			_deck_thumbnail.updateUI(deck);
	}
}