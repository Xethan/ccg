using UnityEngine;

public class SandGolemBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.source == _buffed_character )
			ShiftingSandTile.summonOrLevelUpShiftingSand(Board.getTile(trigger.receiver.Coord), _buffed_character.Owner);
	}
}