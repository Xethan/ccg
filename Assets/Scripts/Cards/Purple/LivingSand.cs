using UnityEngine;

public class LivingSand : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(LivingSandBuff));
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && _coord.isNearby(target.Coord);
	}

	private void doEffect(GameObject gameobject)
	{
		ShiftingSandTile.summonOrLevelUpShiftingSand(gameobject.GetComponent<Tile>(), _owner);
	}
}