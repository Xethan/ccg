using UnityEngine;

public class CorrosiveBlade : Unit
{
	protected override void onSummon()
	{		
		receiveModifier(typeof(CorrosiveBladeBuff));
	}
}