using UnityEngine;

public class TimeJailerBuff : Buff
{
	private Unit _jailed_unit;

	protected override string Description
	{
		get
		{
			if ( _jailed_unit is null )
				return "No unit jailed.";
			return _jailed_unit.CardStats.Name + " will be freed when Time Jailer dies.";
		}
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == _buffed_character )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		else if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character && _jailed_unit != null )
		{
			Tile current_tile = Board.getTile(_buffed_character.Coord);
			_jailed_unit.gameObject.SetActive(true);
			_jailed_unit.reactivate(Character.e_flags.Can_move_attack);
			current_tile.setCharacterAfterDeath(_jailed_unit);
		}
		else if ( trigger.type == Trigger.e_type.End_turn && trigger.player != _buffed_character.Owner )
			_buffed_character.receiveDamage(_jailed_unit, _jailed_unit.ManaCost.Converted / 2);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();

		return target != null
				&& target.Character is Unit
				&& target.Character.Coord.isNearby(_buffed_character.Coord);
	}

	private void doEffect(GameObject gameobject)
	{
		Tile target_tile = gameobject.GetComponent<Tile>();

		_jailed_unit = target_tile.Character as Unit;
		_jailed_unit.gameObject.SetActive(false);
		target_tile.Character = null;
		updateUI();
	}
}