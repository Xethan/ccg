using UnityEngine;

public class VoodooDoll : Unit
{
    public override void onTrigger(Trigger trigger)
    {
        if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
            TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
        base.onTrigger(trigger);
    }

    private bool isTargetValid(GameObject gameobject)
    {
        Tile target = gameobject.GetComponent<Tile>();
        return target != null && target.Character is Unit;
    }

    private void doEffect(GameObject gameobject)
    {
        Character target = gameobject.GetComponent<Tile>().Character;
        this.receiveModifier(
            typeof(LinkBuff),
            buff:typeof(CorrosionBuff),
            source:target,
            should_display_buff_ui:true
        );
        target.receiveModifier(
            typeof(LinkBuff),
            buff:typeof(CorrosionBuff),
            source:this,
            should_display_buff_ui:true
        );
    }

}