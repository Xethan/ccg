using UnityEngine;

public class TouchOfGold : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null && target.Character is Unit && _character.Coord.distance(target.Character.Coord) <= 2;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveModifier(typeof(StunnedBuff), param: 1);
	}
}