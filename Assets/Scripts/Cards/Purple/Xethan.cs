using UnityEngine;

public class Xethan : General
{
	protected override string BattleCryPrefabPath { get { return "Purple/Touch Of Gold"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 7, 15}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Start_turn && trigger.player == _owner )
			gainXp(1);

		base.onTrigger(trigger);
	}

	protected override void onLevel2()
	{
		foreach (Tile tile in Board._board)
		{
			Character character = tile.Character;
			if ( character != null && (character == this || character.Owner != _owner) )
				character.receiveModifier(typeof(StunnedBuff), param:1);
		}
	}
}