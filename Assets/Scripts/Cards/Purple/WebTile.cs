using UnityEngine;
using UnityEngine.UI;

public class WebTile : SpecialTile
{
	void Start()
	{
		_special_tile_hover_ui.updateUI(
			this.gameObject.GetComponent<Image>().sprite,
			"Web",
	        "If an ennemy character is on this tile at the start of a turn, "
	        + "it takes 1 more damage from all sources until the end of the turn."
	    );
	}

	public override void startTurn(Player current_player)
	{
		if ( _tile.Character != null && _tile.Character.Owner != _owner )
			_tile.Character.receiveModifier(typeof(WebBuff), should_display_buff_ui:true);
	}
}