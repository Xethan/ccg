using UnityEngine;

public class Chthunafh : Unit
{
	protected override e_types Types { get { return Unit.e_types.Spider; } }

	protected override void onSummon()
	{
		receiveModifier(typeof(ChthunafhBuff));
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}
	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && _coord.isNearby(target.Coord);
	}
	private void doEffect(GameObject gameobject)
	{
		gameobject.GetComponent<Tile>().setSpecialTile("Tiles/Web", this.Owner);
	}
}