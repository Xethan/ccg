using UnityEngine;
using System;
using System.Collections.Generic;

/*** Changelog ***\
[Mai 2023] Changement du pouvoir, spread un ShiftingSand -> créer 3 ShiftingSand
	-> il faut pouvoir utiliser le pouvoir même si on n'a pas pioché de carte ShiftingSand
[?] Changement du pouvoir , créer un ShiftingSand -> spread un ShiftingSand
	-> créer un ShiftingSand n'était pas assez fort
\******************/

public class Zirix : General
{
	protected override string BattleCryPrefabPath { get { return "Purple/Quick Sand"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 4, 16}; } }

	public override void onTrigger(Trigger trigger)
	{
		if (
			trigger.Is("Tile_proc")
			&& trigger.tile.getSpecialTile(_owner) is ShiftingSandTile
		)
			gainXp(1);

		base.onTrigger(trigger);
	}

	protected override void onLevel2()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new ZirixAbility(character:this)
		};
	}
}

public class ZirixAbility : ActivatedAbility
{
	private bool _used_this_turn = false;

	public override string Cost { get { return "0"; } }
	public override string Name {
		get {
			return "Move target allied <b>ShiftingSand</b> up to two tiles away. "
				 + "If moved on another allied <b>ShiftingSand</b>, merge them.";
		}
	}

	public ZirixAbility(Character character) : base(character) {}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.Is("End_turn") )
			_used_this_turn = false;
	}

	public override bool payCost()
	{
		if ( _used_this_turn )
			return false;

		_used_this_turn = true;
		return true;
	}

	public override void doEffect()
	{
		MultiTargetingPanel.doTargetingAbility(
			new List<Func<GameObject, List<GameObject>, bool>>{
				_is_first_target_valid, _is_second_target_valid
			},
			_do_ability_effect
		);
	}

	private bool _is_first_target_valid(GameObject gameobject, List<GameObject> previous_targets)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.getSpecialTile(_character.Owner) is ShiftingSandTile;
	}

	private bool _is_second_target_valid(GameObject gameobject, List<GameObject> previous_targets)
	{
		Tile first_tile = previous_targets[0].GetComponent<Tile>();
		Tile second_tile = gameobject.GetComponent<Tile>();

		return (
			second_tile != null
			&& second_tile != first_tile
			&& second_tile.Coord.distance(first_tile.Coord) <= 2
		);
	}

	private void _do_ability_effect(List<GameObject> targets)
	{
		Tile first_tile = targets[0].GetComponent<Tile>();
		SpecialTile shifting_sand_to_move = first_tile.getSpecialTile(_character.Owner);
		Tile second_tile = targets[1].GetComponent<Tile>();

		if ( second_tile.getSpecialTile(_character.Owner) is ShiftingSandTile second_shifting_sand )
			(shifting_sand_to_move as ShiftingSandTile).merge(second_shifting_sand);

		first_tile.loseSpecialTile(_character.Owner, destroy_tile:false);
		second_tile.assignSpecialTile(shifting_sand_to_move);
		shifting_sand_to_move.Tile = second_tile;
	}
}