using UnityEngine;

public class Omnipotence : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		_owner.draw(3);
	}
}