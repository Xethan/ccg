using UnityEngine;

public class Swapeur : Lieutenant
{
	protected override string BattleCryPrefabPath { get { return "Purple/Swap"; } }
}