using UnityEngine;

public class MindControlBuff : Buff
{
	protected override string Description
	{
		get { return "This unit will die at the end of the turn."; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
			_buffed_character.death();
	}
}