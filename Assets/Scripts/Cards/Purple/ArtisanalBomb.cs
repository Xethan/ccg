using UnityEngine;
using System.Collections.Generic;

public class ArtisanalBomb : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		int center_damage = 7;
		int damage_decrease_by_tile = 1;
		if ( target.Character is Unit && target.Character.Owner != _owner )
			target.Character.receiveDamage(this, center_damage);
		for (int radius = 1; center_damage - radius * damage_decrease_by_tile > 0; radius++)
			doAoEEffect(target.Coord, radius : radius, nb_damage : center_damage - radius * damage_decrease_by_tile);
	}

	private void doAoEEffect(Coord center_coord, int radius, int nb_damage)
	{
		foreach (Character character in Board.getCharactersInRange(center_coord.createSafeCenteredCircleCoords, radius))
			if ( character is Unit && character.Owner != _owner )
				character.receiveDamage(this, nb_damage);
	}
}