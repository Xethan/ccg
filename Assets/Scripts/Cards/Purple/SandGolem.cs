using UnityEngine;

public class SandGolem : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(SandGolemBuff));
	}
}