using UnityEngine;

public class MindImprintBuff : Buff
{
	protected override string Description
	{
		get { return "Create a copy in your hand of the next spell you play that isn't Mind Imprint"; }
	}
	
	public override void onTrigger(Trigger trigger)
	{
		if (
			trigger.type == Trigger.e_type.Spell
			&& !(trigger.source is BattleCry)
			&& !(trigger.source is CombatTrick)
			&& trigger.source.Owner ==_buffed_character.Owner
			&& trigger.source.GetType().Name != "MindImprint"
		) {
			Tools.createCopyOfCardInPlayerHand(trigger.source as Card, _buffed_character.Owner);
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}
}