using UnityEngine;
using System.Collections.Generic;

public class CorrosiveCurse : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		Board.getEnnemyPlayer().Generals[0].receiveModifier(
			typeof(CorrosiveCurseBuff),
			should_display_buff_ui:true
		);
	}
}