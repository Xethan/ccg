using UnityEngine;
using System.Collections.Generic;

public class WebBuff : Buff
{
	protected override string Description
	{
		get { return "Takes 1 more damage from all sources until end of turn."; }
	}

	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character )
			return amount + 1;
		return amount;
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

}