using UnityEngine;
using System; // Predicate / Action

public class QuickSand : BattleCry
{
    public override bool isTargetValid(Tile first_target)
    {
        return _isTargetValid(first_target.gameObject);
    }

    protected override void doEffect(Tile first_target)
    {
        _doEffect(first_target.gameObject);
        TargetingPanel.doTargetingAbility(_isTargetValid, _doEffect);
        TargetingPanel.doTargetingAbility(_isTargetValid, _doEffect);
    }

	private bool _isTargetValid(GameObject target)
    {
        Tile target_tile = target.GetComponent<Tile>();
        return target_tile != null && target_tile.Coord.squareDistance(_character.Coord) <= 2;
    }

    private void _doEffect(GameObject target)
    {
        Tile target_tile = target.GetComponent<Tile>();
        ShiftingSandTile.summonOrLevelUpShiftingSand(target_tile, _owner);
    }
}