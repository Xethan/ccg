using UnityEngine;

public class EmbodimentOfWisdomManaCostModif : ManaCostModif
{
	public override int[] getManaCostAfterModif(System.Object source, int[] mana_cost)
	{
		int[] mana_cost_after_modif = (int[])mana_cost.Clone();
		if ( source is Card && !(source is CombatTrick) && !(source is BattleCry) )
			mana_cost_after_modif[Factions.neutral] -= 1;
		return mana_cost_after_modif;
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == this._source )
			_affected_player.ManaReserve.loseManaCostModif(this);
	}
}