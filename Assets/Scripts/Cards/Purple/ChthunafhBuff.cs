using UnityEngine;
using System.Linq;

public class ChthunafhBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		updateUI();
	}

	public override int getAttackAfterBuff(int attack)
	{
		if ( Board.getSpecialTile(_buffed_character.Coord, _buffed_character.Owner) is WebTile )
			return attack + 2;
		return attack;
	}
}