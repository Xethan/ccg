using UnityEngine;

public class Protector : CombatTrick
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is Unit;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveModifier(
			typeof(TemporaryProvokeBuff),
			param2:2,  // number of turns
			should_display_buff_ui:true
		);
	}
}
