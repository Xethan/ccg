using UnityEngine;

public class Thaka : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character is Unit;
	}

	private void doEffect(GameObject gameobject)
	{
		Character target = gameobject.GetComponent<Tile>().Character;
		this.receiveModifier(typeof(LinkBuff), buff:typeof(LightningBuff), param:2, source:target);
		target.receiveModifier(typeof(LinkBuff), buff:typeof(LightningBuff), param:2, source:this);
	}

}