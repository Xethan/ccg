using UnityEngine;

public class CorrosiveCurseBuff : GlobalCharacterModifier
{
	private int _nb_summons_affected_remaining = 3;

	protected override string Description
	{ get { return "The next "  + _nb_summons_affected_remaining.ToString() + " non-token units you summon will receive Corrosion."; } }

	public override void onTrigger(Trigger trigger)
	{
		if (
			trigger.type == Trigger.e_type.Summon
			&& trigger.receiver.Owner == _buffed_character.Owner
			&& !(trigger.receiver is Token)
		) {
			trigger.receiver.receiveModifier(typeof(CorrosionBuff), should_display_buff_ui:true);
			_nb_summons_affected_remaining -= 1;
			if ( _nb_summons_affected_remaining == 0 )
			{
				_buffed_character.loseModifier(this);
				GameObject.Destroy(this);
			}
			else
			{
				updateUI();
			}
		}
	}
}