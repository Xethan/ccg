using UnityEngine;

public class RewindBuff : Buff
{
	private Coord	_initial_position;
	private int		_turns_remaining = 2;

	protected override string Description
	{
		get
		{
			if ( _initial_position is null ) // _initial_position is not yet set
				return "";

			return "This character will go back to tile "
					+ _initial_position.Description
					+ " at the start of Rewind's owner next turn.";
		}
	}

	void Start()
	{
		_initial_position = _buffed_character.Coord;
		updateUI();
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Start_turn )
		{
			_turns_remaining -= 1;
			if ( _turns_remaining == 0 )
			{
				_buffed_character.teleport(_source, _initial_position);
				_buffed_character.loseModifier(this);
				GameObject.Destroy(this);
			}
		}
	}
}