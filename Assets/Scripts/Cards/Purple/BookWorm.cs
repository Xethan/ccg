using UnityEngine;
using System;
using System.Collections.Generic;

public class BookWorm : Unit
{
	protected override void onSummon()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new BookWormAbility(character:this)
		};
	}
}

public class BookWormAbility : ActivatedAbility
{
	private readonly int[]	_ability_mana_cost = {2, 0, 1, 0, 0, 0};
	private bool			_used_this_turn = false;

	public override string Cost { get { return "2 B"; } }
	public override string Name { get { return "Discard a card: Get +2/+4"; } }

	public BookWormAbility(Character character) : base(character) {}

	public override void onTrigger(Trigger trigger)
	{
		if (trigger.type == Trigger.e_type.End_turn)
			_used_this_turn = false;
	}

	public override bool payCost()
	{
		return _used_this_turn == false && _character.Owner.ManaReserve.payManaCost(this, _ability_mana_cost);
	}

	public override void doEffect()
	{
		_used_this_turn = true;
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			CardSlot card_slot = gameobject.GetComponent<CardSlot>();
			return card_slot != null && card_slot.Card != null;
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			gameobject.GetComponent<CardSlot>().Card.destroy();
			gameobject.GetComponent<CardSlot>().Card = null;
			_character.receiveModifier(typeof(BookWormBuff));
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}