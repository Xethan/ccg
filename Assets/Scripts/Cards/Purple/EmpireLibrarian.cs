using UnityEngine;

public class EmpireLibrarian : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(EmpireLibrarianBuff), should_display_buff_ui:true);
	}
}