using UnityEngine;
using System; // Math

public class SunsetParagon : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
		{
			for (int i = _coord.X - 1; i <= _coord.X + 1; ++i)
			{
				for (int j = _coord.Y - 1; j <= _coord.Y + 1; ++j)
				{
					doEffect(new Coord(i, j));
				}
			}
		}
		base.onTrigger(trigger);
	}

	private void doEffect(Coord target_coord)
	{
		if ( target_coord.isOutOfBoard() || target_coord == _coord )
			return;

		Character target_character = Board.getCharacter(target_coord);

		if ( target_character != null && target_character is Unit )
		{
			target_character.receiveDamage(
				this,
				Convert.ToInt32( // could not manage to find something simpler that actually works
					Math.Round(
						Math.Round(1.5f * target_character.CardStats.ManaCost.Converted, 1),
						MidpointRounding.AwayFromZero
					)
				)
			);
		}
	}
}