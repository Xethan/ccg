using UnityEngine;

public class TimeJailer : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(TimeJailerBuff));
	}
}