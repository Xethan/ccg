using UnityEngine;

public class ProtectorOfTheSand : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(ProvokeBuff));
		receiveModifier(typeof(ProtectorOfTheSandBuff));
	}
}