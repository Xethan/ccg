using UnityEngine;
using System;
using System.Collections.Generic;

public class Gothkas : Unit
{
    protected override e_types Types { get { return Unit.e_types.Spider; } }

	protected override void onSummon()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new GothkasActivatedAbility(character:this)
		};
	}
}

public class GothkasActivatedAbility : ActivatedAbility
{
	private bool _used_this_turn = true;  // not usable the first turn

	public override string Cost { get { return "0"; } }
	public override string Name { get { return "Stun target adjacent unit and put a <b>Web</b> under it."; } }

	public GothkasActivatedAbility(Character character) : base(character) {}

	public override void onTrigger(Trigger trigger)
	{
		if (trigger.type == Trigger.e_type.End_turn)
			_used_this_turn = false;
	}

	public override bool payCost()
	{
		return _used_this_turn == false;
	}

	public override void doEffect()
	{
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			return target != null
				&& target.Character is Unit
				&& target.Coord.distance(_character.Coord) == 1;
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			target.Character.receiveModifier(typeof(StunnedBuff), param : 1);
			target.setSpecialTile("Tiles/Web", _character.Owner);
			_used_this_turn = true;
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}