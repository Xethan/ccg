using UnityEngine;
using System.Collections.Generic;

public class BombDrops : Spell
{
	private List<Tile>	_previous_targets = new List<Tile>();

	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		_doBombEffect(target.gameObject);
		TargetingPanel.doTargetingAbility(_isTargetValid, _doBombEffect);
		TargetingPanel.doTargetingAbility(_isTargetValid, _doBombEffect);
	}

	private bool _isTargetValid(GameObject gameobject)
	{
		Tile target_tile = gameobject.GetComponent<Tile>();
		return target_tile != null && !_previous_targets.Contains(target_tile);
	}

	private void _doBombEffect(GameObject gameobject_target)
	{
		Tile target = gameobject_target.GetComponent<Tile>();
		_previous_targets.Add(target);
		if ( target.Character is Unit && target.Character.Owner != _owner )
			target.Character.receiveDamage(this, 4);

		foreach (Character Character in Board.getCharactersInRange(target.Coord.createSafeCenteredCircleCoords, radius:1))
			if ( Character is Unit && Character.Owner != _owner )
				Character.receiveDamage(this, 3);
	}
}