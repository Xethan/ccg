using UnityEngine;
using System.Linq;

public class KagnothBuff : Buff
{
	private bool on_web_tile = false;
	private Modifier _modifier = null;
	
	public override void onTrigger(Trigger trigger)
	{
		if (!on_web_tile && Board.getSpecialTile(_buffed_character.Coord, _buffed_character.Owner) is WebTile)
		{
			on_web_tile = true;
			_modifier = _buffed_character.receiveModifier(typeof(LightningBuff), param : 2);
		}
		if (on_web_tile && !(Board.getSpecialTile(_buffed_character.Coord, _buffed_character.Owner) is WebTile))
		{
			on_web_tile = false;
			_buffed_character.loseModifier(_modifier);
			GameObject.Destroy(_modifier);
		}
		updateUI();
	}
}