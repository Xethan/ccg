using UnityEngine;
using System; // Predicate / Action

public class Swap : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return target != null && target.Character is Unit && target.Character.Owner == _owner;
	}

	protected override void doEffect(Tile first_target)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();

			return second_target != null && second_target.Character is Unit;
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			first_target.Character.teleport(this, second_target.Coord);
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}