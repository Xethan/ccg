using UnityEngine;

public class EmbodimentOfWisdomBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _buffed_character.Owner)
			_buffed_character.Owner.draw(1);
	}
}