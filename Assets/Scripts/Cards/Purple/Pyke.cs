using UnityEngine;
using System;

public class Pyke : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null
				&& target.Character == null
				&& (target.Coord.X == _coord.X || target.Coord.Y == _coord.Y)
				&& 2 <= _coord.distance(target.Coord)
				&& _coord.distance(target.Coord) <= 4;
	}

	private void doEffect(GameObject gameobject)
	{
		Tile target_tile = gameobject.GetComponent<Tile>();
		Coord dash = target_tile.Coord - _coord;
		for (int i = 1; i < _coord.distance(target_tile.Coord); i++)
		{
			Coord passed_through_coord = _coord + (dash.unitaryCoord() * i);
			Tile passed_through_tile = Board.getTile(passed_through_coord);
			if ( passed_through_tile.Character is Unit && passed_through_tile.Character.Owner != _owner )
				passed_through_tile.Character.receiveModifier(typeof(StunnedBuff), param:1);
		}
		teleport(this, target_tile.Coord);
	}
}