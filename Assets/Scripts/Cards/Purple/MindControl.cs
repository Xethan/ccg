using UnityEngine;

public class MindControl : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is Unit && target.Character.Owner != _owner;
	}

	protected override void doEffect(Tile target)
	{
		Character character = target.Character;

		character.Owner = _owner;
		character.reactivate(Character.e_flags.Can_attack);
		character.exhaust(Character.e_flags.Can_move);
		character.receiveModifier(typeof(MindControlBuff), should_display_buff_ui:true);
	}
}