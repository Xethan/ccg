using UnityEngine;

public class EmbodimentOfWisdom : Unit
{
	protected override void onSummon()
	{
		Owner.ManaReserve.receiveManaCostModif(typeof(EmbodimentOfWisdomManaCostModif), source : this);
		receiveModifier(typeof(EmbodimentOfWisdomBuff));
	}
}