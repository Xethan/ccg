using UnityEngine;

public class ChainReactionBuff : Buff
{
	protected override string Description
	{
		get { return "If it dies this turn, explode dealing 3 damage to all nearby characters and give them this effect."; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character )
		{
			foreach (
				Character character in Board.getCharactersInRange(
					_buffed_character.Coord.createSafeCenteredSquareCoords, radius:1
				)
			) {
				if ( character is Unit )
				{
					character.receiveModifier(typeof(ChainReactionBuff));
					character.receiveDamage(_buffed_character, 3);
				}
			}
		}

		if ( trigger.type == Trigger.e_type.End_turn )
			_buffed_character.loseModifier(this);
	}
}