using UnityEngine;

public class BookWormBuff : Buff
{
	public override int getAttackAfterBuff(int attack)
	{
		return attack + 2;
	}

	public override int getHealthAfterBuff(int health)
	{
		return health + 4;
	}

	public override int getMaxHealthAfterBuff(int max_health)
	{
		return max_health + 4;
	}
}