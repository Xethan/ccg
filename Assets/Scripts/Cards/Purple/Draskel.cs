using UnityEngine;

public class Draskel : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(CorrosiveDamageBuff));
	}
}