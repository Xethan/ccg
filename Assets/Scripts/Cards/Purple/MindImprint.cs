using UnityEngine;

public class MindImprint : CombatTrick
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		_owner.Generals[0].receiveModifier(typeof(MindImprintBuff), should_display_buff_ui:true);
	}
}