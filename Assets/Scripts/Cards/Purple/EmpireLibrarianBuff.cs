using UnityEngine;
using System.Collections; // Coroutines
using System.Collections.Generic;

public class EmpireLibrarianBuff : Buff
{
	private List<Card>	_cards = new List<Card>();
	private	int			_nb_times_ability_done = 0;

	protected override string Description
	{
		get
		{
			if ( _cards.Count == 0 )
				return "No cards will be put into your hand when this unit dies.";

			string description = "A " + _cards[0].CardStats.Name;
			if ( _cards.Count == 2 )
				description += " and a " + _cards[1].CardStats.Name;
			description += " will be put into your hand when this unit dies.";
			return description;
		}
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == _buffed_character )
			StartCoroutine(doTargetingAbility());
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character )
		{
			foreach ( Card card in _cards )
				_buffed_character.Owner.putCardInHand(card);
		}
	}

	private IEnumerator doTargetingAbility()
	{
		if ( _nb_times_ability_done < 2 )
		{
			yield return null;
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		}
		_nb_times_ability_done += 1;
	}

	private bool isTargetValid(GameObject gameobject)
	{
		return gameobject.GetComponent<DeckTrackerCard>() != null;
	}

	private void doEffect(GameObject gameobject)
	{
		DeckTrackerCard deck_tracker_card = gameobject.GetComponent<DeckTrackerCard>();
		Card card_to_stock = deck_tracker_card.Card;

		_cards.Add(card_to_stock);
		_buffed_character.Owner.removeCardFromDeck(card_to_stock);
		updateUI();
		StartCoroutine(doTargetingAbility());
	}
}