using UnityEngine;
using System; // Predicate / Action

public class SpiderWeb : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is Unit;
	}

	protected override void doEffect(Tile target)
	{		
		target.Character.receiveModifier(typeof(StunnedBuff), param : 1);
		target.setSpecialTile("Tiles/Web", this.Owner);
	}
}
