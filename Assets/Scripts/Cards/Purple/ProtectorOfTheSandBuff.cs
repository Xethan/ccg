using UnityEngine;

public class ProtectorOfTheSandBuff : Buff
{
	public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
	{
		return flags & ~Character.e_flags.Can_attack;
	}
}