using UnityEngine;

public class DancingBlades : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
		{
			int x_direction = _owner.isPlayerOne ? 1 : -1;
			Coord target_coord = _coord + new Coord(x_direction, 0);
			if ( target_coord.isInsideBoard() )
			{
				Character target_character = Board.getCharacter(target_coord);
				if ( target_character is Unit )
					target_character.receiveDamage(this, 4);
			}
		}
		base.onTrigger(trigger);
	}
}