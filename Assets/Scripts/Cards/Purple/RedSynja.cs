using UnityEngine;
using System;
using System.Collections.Generic;

public class RedSynja : Unit
{
	protected override void onSummon()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new RedSynjaActivatedAbility(character:this)
		};
	}
}

public class RedSynjaActivatedAbility : ActivatedAbility
{
	private int _nb_damages_stocked = 0;

	public override string Cost { get { return "Use " + _nb_damages_stocked + " charges"; } }
	public override string Name { get { return "Deal as many damage to target character in range 2"; } }

	public RedSynjaActivatedAbility(Character character) : base(character) {}

	public override void onTrigger(Trigger trigger)
	{
		if (trigger.Is("Damage")
			&& trigger.receiver is General
			&& trigger.receiver.Owner == _character.Owner
		)
			_nb_damages_stocked += trigger.amount;
	}

	public override bool payCost()
	{
		return _nb_damages_stocked > 0;
	}

	public override void doEffect()
	{
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			return target != null
				&& target.Character is Unit
				&& target.Coord.distance(_character.Coord) <= 2;
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			Character target_character = gameobject.GetComponent<Tile>().Character;
			target_character.receiveDamage(_character, _nb_damages_stocked);
			_nb_damages_stocked = 0;
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}