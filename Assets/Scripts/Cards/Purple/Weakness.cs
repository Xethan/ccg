using UnityEngine;

public class Weakness : CombatTrick
{
    public override bool isTargetValid(Tile target)
    {
        return target.Character is Unit;
    }

    protected override void doEffect(Tile target)
    {
        target.Character.receiveModifier(typeof(PermanentWeaknessBuff));
    }
}