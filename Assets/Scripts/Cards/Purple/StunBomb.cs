using UnityEngine;
using System.Collections.Generic;

public class StunBomb : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		if ( target.Character is Unit && target.Character.Owner != _owner )
		{
			target.Character.receiveModifier(typeof(StunnedBuff), param:1);
			target.Character.receiveModifier(typeof(CorrosionBuff), should_display_buff_ui:true);
		}
		foreach (Character character in Board.getCharactersInRange(target.Coord.createSafeCenteredSquareCoords, radius:1))
		{
			if ( character is Unit && character.Owner != _owner )
			{
				character.receiveModifier(typeof(StunnedBuff), param:1);
				character.receiveModifier(typeof(CorrosionBuff), should_display_buff_ui:true);
			}
		}
	}
}