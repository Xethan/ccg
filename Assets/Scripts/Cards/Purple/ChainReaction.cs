using UnityEngine;
using System.Collections.Generic;

public class ChainReaction : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		doEffectAtTargetCoord(target.Coord);
		foreach ( Coord target_coord in target.Coord.createSafeCenteredSquareCoords(radius: 1) )
			doEffectAtTargetCoord(target_coord);
	}

	private void doEffectAtTargetCoord(Coord target_coord)
	{
		Character target_character = Board.getCharacter(target_coord);
		if ( target_character is Unit )
		{
			target_character.receiveModifier(typeof(ChainReactionBuff), should_display_buff_ui:true);
			target_character.receiveDamage(this, 3);
		}
	}
}