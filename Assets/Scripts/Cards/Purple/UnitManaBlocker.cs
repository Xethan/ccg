using UnityEngine;

public class UnitManaBlocker : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			Board.getEnnemyPlayer().ManaReserve.receiveManaCostModif(typeof(UnitManaBlockerManaCostModif));
		base.onTrigger(trigger);
	}
}