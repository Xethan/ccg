using UnityEngine;

public class Scryer : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			_owner.scry(3);
		base.onTrigger(trigger);
	}
}