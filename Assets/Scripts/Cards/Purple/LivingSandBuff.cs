using UnityEngine;

public class LivingSandBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character )
			ShiftingSandTile.summonOrLevelUpShiftingSand(Board.getTile(_buffed_character.Coord), _buffed_character.Owner);
	}
}