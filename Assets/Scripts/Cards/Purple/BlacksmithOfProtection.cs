using UnityEngine;

public class BlacksmithOfProtection : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(ProvokeBuff));
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		CardSlot card_slot = gameobject.GetComponent<CardSlot>();
		return card_slot != null && card_slot.Card is Unit;
	}

	private void doEffect(GameObject gameobject)
	{
		CardSlot card_slot = gameobject.GetComponent<CardSlot>();
		card_slot.Card.receiveModifier(
			typeof(ProvokeBuff),
			should_display_buff_ui:true
		);
	}
}