using UnityEngine;

public class UnitManaBlockerManaCostModif : ManaCostModif
{
	public override int[] getManaCostAfterModif(System.Object source, int[] mana_cost)
	{
		int[] mana_cost_after_modif = (int[])mana_cost.Clone();
		if ( source is Unit )
			mana_cost_after_modif[Factions.neutral] += 1;
		return mana_cost_after_modif;
	}

	public override void manaCostWasPayed(System.Object source, int[] mana_cost)
	{
		if ( source is Unit )
			_affected_player.ManaReserve.loseManaCostModif(this);
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _affected_player )
			_affected_player.ManaReserve.loseManaCostModif(this);
	}
}