using UnityEngine;

public class Rewind : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveModifier(
			typeof(RewindBuff),
			source:this,
			should_display_buff_ui:true
		);
	}
}