using UnityEngine;

public class MasterOfTheSand : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
		{
			for (int x = _coord.X - 1; x <= _coord.X + 1; ++x)
			{
				for (int y = _coord.Y - 1; y <= _coord.Y + 1; ++y)
				{
					Coord test_coord = new Coord(x, y);
					if ( test_coord != _coord && test_coord.isOutOfBoard() == false )
						ShiftingSandTile.summonOrLevelUpShiftingSand(Board.getTile(test_coord), _owner);
				}
			}
		}
		base.onTrigger(trigger);
	}
}