using UnityEngine;

public class CorrosiveBladeBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Attack )
		{
			if ( trigger.source == _buffed_character) { doEffect(trigger.receiver); }
			if ( trigger.receiver == _buffed_character) { doEffect(trigger.source as Character); }
		}
	}

	private void doEffect(Character affected_character)
	{
		if ( affected_character is Unit affected_unit )
			affected_unit.receiveModifier(typeof(CorrosionBuff), should_display_buff_ui:true);
		else if ( affected_character is General affected_general )
			affected_general.receiveModifier(typeof(TemporaryBuff), buff:typeof(VulnerableBuff), param:1, param2:3);
	}
}