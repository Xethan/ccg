using UnityEngine;

public class SecondFist : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character is Unit && _coord.isNearby(target.Coord);
	}

	private void doEffect(GameObject gameobject)
	{
		gameobject.GetComponent<Tile>().Character.receiveModifier(
			typeof(TemporaryIncreasedAttackBuff),
			param:3, // bonus attack
			param2:1, // number of turns
			should_display_buff_ui:true
		);
	}
}