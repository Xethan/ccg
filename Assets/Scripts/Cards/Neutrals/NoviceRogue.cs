using UnityEngine;
using System; // Predicate / Action
using System.Collections; // Coroutines

public class NoviceRogue : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character is General && target.Character.Owner == _owner;
	}

	private void doEffect(GameObject gameobject)
	{
		StartCoroutine(choseSecondTarget(gameobject));
	}

	private IEnumerator choseSecondTarget(GameObject gameobject)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject second_gameobject) =>
		{
			Tile second_target = second_gameobject.GetComponent<Tile>();
			return second_target != null && second_target.Character != null;
		};

		Action<GameObject> card_effect = (GameObject second_gameobject) =>
		{
			Tile second_target = second_gameobject.GetComponent<Tile>();
			gameobject.GetComponent<Tile>().Character.receiveDamage(this, 2);
			second_target.Character.receiveDamage(this, 4);
		};

		yield return null;
		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}