using UnityEngine;

public class HealthKit : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveHeal(7);
	}
}