using UnityEngine;

public class HealingShaman : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}
	
	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character != null;
	}

	private void doEffect(GameObject gameobject)
	{
		gameobject.GetComponent<Tile>().Character.receiveHeal(4);
	}
}