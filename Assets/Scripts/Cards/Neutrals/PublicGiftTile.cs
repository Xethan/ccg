using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PublicGiftTile : SpecialTile
{
	protected override string Name { get { return "Public Gift"; } }
	protected override string Description
	{
		get
		{
			return "If a unit starts its turn on this tile, "
			       + "consume the tile to add one mana to the reserve of its owner.";
		}
	}

	public override void startTurn(Player current_player)
	{
		if ( _tile.Character is Unit && !(_tile.Character is Token) && _tile.Character.Owner == current_player )
		{
			_tile.Character.Owner.ManaReserve.receiveManaInReserve(1);
			_tile.loseSpecialTile(null);
		}
	}
}