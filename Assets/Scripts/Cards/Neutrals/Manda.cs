using UnityEngine;

public class Manda : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character is General && target.Character.Owner == _owner;
	}

	private void doEffect(GameObject gameobject)
	{
		General general = gameobject.GetComponent<Tile>().Character as General;
		if ( general == _owner.Generals[0] )
			_owner.PanelBattleCries.BattleCrySlotGeneral1.reduceCooldown(1);
		else
			_owner.PanelBattleCries.BattleCrySlotGeneral2.reduceCooldown(1);
	}
}