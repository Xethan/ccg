using UnityEngine;

public class TemporaryProvokeBuff : Buff
{
	protected override string Description
	{
		get { return "Provoke (prevent all nearby ennemies from moving or attacking any other target) for "
		              + _param2.ToString() + " turn" + (_param2 > 1 ? "s" : "") + "."; }
	}
	
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_param2 -= 1;
			if (_param2 <= 0)
			{
				_buffed_character.loseModifier(this);
				GameObject.Destroy(this);
			}
			updateUI();
		}
	}

	public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
	{
		return flags | Character.e_flags.Provoke;
	}
}