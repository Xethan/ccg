using UnityEngine;

public class RegenerationBuff : Buff
{
	protected override string Description { get { return "Regeneration " + _param; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _buffed_character.Owner )
			_buffed_character.receiveHeal(_param);
	}
}