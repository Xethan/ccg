using UnityEngine;

public class LifestealBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.source == _buffed_character )
		{
			General general_to_heal = null;
			foreach (General general in _buffed_character.Owner.Generals)
				if ( general_to_heal == null || general.Health < general_to_heal.Health )
					general_to_heal = general;
			general_to_heal.receiveHeal(trigger.amount);
		}
	}
}