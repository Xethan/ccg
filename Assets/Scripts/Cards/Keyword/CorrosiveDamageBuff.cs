using UnityEngine;

public class CorrosiveDamageBuff : Buff
{
    public override void onTrigger(Trigger trigger)
    {
        if ( trigger.type == Trigger.e_type.Damage && trigger.source == _buffed_character && trigger.receiver is Unit)
            trigger.receiver.receiveModifier(typeof(CorrosionBuff), should_display_buff_ui:true);
    }
}