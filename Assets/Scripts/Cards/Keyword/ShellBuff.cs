using UnityEngine;
using System;

public class ShellBuff : Buff
{
	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character )
			return Math.Min(amount, _param);
		return amount;
	}
}