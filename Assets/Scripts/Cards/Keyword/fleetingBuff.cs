using UnityEngine;

public class FleetingBuff : Buff
{
	protected override string Description
	{
		get { return "If this card is still in your hand at the end of the turn, it will be destroyed"; }
	}

	public override void onTriggerInHand(Trigger trigger, CardSlot card_slot)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			(_source as Card).destroy();
			card_slot.Card = null;
		}
	}

	public override void onTrigger(Trigger trigger)
	{
		if (trigger.type == Trigger.e_type.Play && trigger.source == _source )
		{
			(_source as Card).loseModifier(this);
			GameObject.Destroy(this);
		}
	}
}