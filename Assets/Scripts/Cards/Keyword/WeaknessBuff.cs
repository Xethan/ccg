using UnityEngine;

public class WeaknessBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack - 1;
	}
}