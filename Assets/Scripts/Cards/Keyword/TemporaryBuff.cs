using UnityEngine;

public class TemporaryBuff : Buff
{
    private Modifier _modifier = null;

    public override void onTrigger(Trigger trigger)
    {
        if ( trigger.Is("Buff") && trigger.modifier == this)
            _modifier = _buffed_character.receiveModifier(_buff, param:_param);

        if ( trigger.type == Trigger.e_type.End_turn )
        {
            _param2 -= 1;
            if (_param2 <= 0) { doDestroy(); }
            updateUI();
        }

        if ( trigger.Is("Death") && trigger.receiver == this._source )
            doDestroy();
    }

    private void doDestroy()
    {
        _buffed_character.loseModifier(_modifier);
        GameObject.Destroy(_modifier);

        _buffed_character.loseModifier(this);
        GameObject.Destroy(this);
    }
}