using UnityEngine;

public class StunnedBuff : Buff
{
	protected override string Description
	{
		get { return "Stunned for " + _param + " turn" + (_param > 1 ? "s" : "") + "."; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _buffed_character.Owner )
		{
			_param -= 1;
			if ( _param <= 0 )
			{
				_buffed_character.loseModifier(this);
				GameObject.Destroy(this);
			}
			updateUI();
		}
	}

	public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
	{
		return flags & ~Character.e_flags.Can_move_attack;
	}
}