using UnityEngine;

/*** Changelog ***\
[Mai 2023] Dégâts autour de l'attaquant -> Dégâts autour de la cible : Plus thématique
\******************/

public class LightningBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Attack && trigger.source == _buffed_character )
			foreach (Character character in Board.getCharactersInRange(trigger.receiver.Coord.createSafeCenteredSquareCoords, radius:1))
				if ( character.Owner != _buffed_character.Owner)
					character.receiveDamage(_buffed_character, _param);
	}
}