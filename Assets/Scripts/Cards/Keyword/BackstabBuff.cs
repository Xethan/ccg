using UnityEngine;
using System;

public class BackstabBuff : Buff
{
	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if (
			trigger.type == Trigger.e_type.Attack
			&& trigger.source == _buffed_character
			&& isBackstabAttack(_buffed_character, trigger.receiver)
		)
			return amount + _param;
		return amount;
	}

	public static bool isBackstabAttack(Character attacker, Character target)
	{
		int x_diff = attacker.Owner.isPlayerOne ? -1 : 1;
		return (
			target.Coord.X - attacker.Coord.X == x_diff
			&& Math.Abs(target.Coord.Y - attacker.Coord.Y) <= 1
		);
	}
}