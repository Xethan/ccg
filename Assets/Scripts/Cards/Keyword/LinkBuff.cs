using UnityEngine;

public class LinkBuff : Buff
{
	private Modifier _modifier = null;

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.Is("Death") && trigger.receiver == this._source )
		{
			_buffed_character.loseModifier(_modifier);
			GameObject.Destroy(_modifier);

			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}

		if ( trigger.Is("Buff") && trigger.modifier == this)
			_modifier = _buffed_character.receiveModifier(_buff, param : _param);
	}
}