using UnityEngine;

public class TemporaryIncreasedAttackBuff : Buff
{
	protected override string Description
	{
		get
		{
			return "+" + _param.ToString() + " attack for " + _param2.ToString() + " turn" + (_param2 > 1 ? "s" : "") + ".";
		}
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_param2 -= 1;
			if (_param2 <= 0)
			{
				_buffed_character.loseModifier(this);
				GameObject.Destroy(this);
			}
			updateUI();
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack + _param;
	}

}