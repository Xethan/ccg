using UnityEngine;

public class IncreasedRangeBuff : Buff
{
	protected override string Description { get { return "+" + _param.ToString() + " range."; } }

	public override int getRangeAfterBuff(int range)
	{
		return range + _param;
	}
}