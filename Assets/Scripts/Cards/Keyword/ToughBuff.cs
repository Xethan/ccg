using UnityEngine;

public class ToughBuff : Buff
{
	protected override string Description { get { return "Tough " + _param; } }

	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character )
			return amount - _param;
		return amount;
	}
}