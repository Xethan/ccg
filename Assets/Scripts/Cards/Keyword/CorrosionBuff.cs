using UnityEngine;

public class CorrosionBuff : Buff
{
	private int	_damage_taken_increase = 1;
	private bool _increase_corrosion = false;

	protected override string Description
	{
		get { return "Damaged taken is increased by " + _damage_taken_increase.ToString() + ". Increases by 1 at the end of your turns."; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn)
		{
			if ( _increase_corrosion == true )
			{
				_damage_taken_increase += 1;
				_increase_corrosion = false;
				updateUI();
			}
			else
			{
				_increase_corrosion = true;
			}
		}
	}

	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character )
			return amount + _damage_taken_increase;
		return amount;
	}
}