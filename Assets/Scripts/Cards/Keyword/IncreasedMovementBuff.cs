using UnityEngine;

public class IncreasedMovementBuff : Buff
{
	public override int getMovementAfterBuff(int movement)
	{
		return movement + _param;
	}
}