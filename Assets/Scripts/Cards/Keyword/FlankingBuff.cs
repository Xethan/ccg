using UnityEngine;
using System;

public class FlankingBuff : Buff
{
	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if (
            trigger.type == Trigger.e_type.Attack
            && trigger.source == _buffed_character
            && isFlankingAttack(_buffed_character, trigger.receiver)
        )
			return amount + _param;
		return amount;
	}

    public static bool isFlankingAttack(Character attacker, Character target)
    {
        return (
            target.Coord.X == attacker.Coord.X
            && Math.Abs(target.Coord.Y - attacker.Coord.Y) == 1
        );
    }
}