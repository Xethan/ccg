using UnityEngine;

public class ProvokeBuff : Buff
{
	protected override string Description
	{
		get { return "Provoke (prevent all nearby ennemies from attacking any other target)"; }
	}

	public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
	{
		return flags | Character.e_flags.Provoke;
	}
}