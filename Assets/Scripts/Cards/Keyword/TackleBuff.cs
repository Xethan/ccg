using UnityEngine;

public class TackleBuff : Buff
{
    protected override string Description
    {
        get { return "Tackle (prevent all nearby ennemies from moving)"; }
    }

    public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
    {
        return flags | Character.e_flags.Tackle;
    }
}