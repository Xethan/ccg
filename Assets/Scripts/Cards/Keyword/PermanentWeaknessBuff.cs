using UnityEngine;

public class PermanentWeaknessBuff : Buff
{
    public override int getAttackAfterBuff(int attack)
	{
		return attack - 1;
	}
}