using UnityEngine;

public class IncreasedAttackBuff : Buff
{
	protected override string Description { get { return "+" + _param.ToString() + " attack."; } }

	public override int getAttackAfterBuff(int attack)
	{
		return attack + _param;
	}
}