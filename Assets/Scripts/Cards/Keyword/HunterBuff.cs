using UnityEngine;

public class HunterBuff : Buff
{
	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.source == _buffed_character && _buffed_character.Health >= trigger.receiver.Health)
			return amount + _param;
		return amount;
	}
}