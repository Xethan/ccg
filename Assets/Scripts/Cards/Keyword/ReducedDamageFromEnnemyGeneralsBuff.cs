using UnityEngine;

public class ReducedDamageFromEnnemyGeneralsBuff : Buff
{
	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character
			 && trigger.source is General && trigger.source.Owner != _buffed_character.Owner )
			return amount - _param;
		return amount;
	}
}