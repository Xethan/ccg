using UnityEngine;

public class IncreasedHealthBuff : Buff
{
	protected override string Description { get { return "+" + _param.ToString() + " Health."; } }

	public override int getHealthAfterBuff(int health)
	{
		return health + _param;
	}

	public override int getMaxHealthAfterBuff(int max_health)
	{
		return max_health + _param;
	}
}