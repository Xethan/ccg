using UnityEngine;

public class RunAndHitBuff : Buff
{
	protected override string Description
	{
		get { return "+1 attack and +2 movement until end of turn."; }
	}

	void Start()
	{
		_buffed_character.receiveArmor(3);
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack + 1;
	}

	public override int getMovementAfterBuff(int movement)
	{
		return movement + 2;
	}
}