using UnityEngine;

public class RunAndHit : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is General;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveModifier(typeof(RunAndHitBuff), should_display_buff_ui:true);
	}
}