using UnityEngine;

public class SyllneerBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Start_turn )
			updateUI();
	}

	public override int getAttackAfterBuff(int attack)
	{
		return Board.isMyTurn(_buffed_character.Owner) ? attack : attack + 1;
	}

	public override int getMovementAfterBuff(int movement)
	{
		return movement + 1;
	}
}