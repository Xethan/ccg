using UnityEngine;

public class Bomb : Token
{
	protected override void onSummon()
	{
		receiveModifier(typeof(BombBuff), should_display_buff_ui:true);
	}
}