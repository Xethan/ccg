using UnityEngine;
using System.Linq;

public class SandDevil : Token
{
	protected override void onSummon()
	{
		_flags |= e_flags.Rush;
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
			if ( !_owner.Generals.Any(x => x is Azir && x.Level == 2) )
				death();

		base.onTrigger(trigger);
	}
}