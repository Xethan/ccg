using UnityEngine;

public class BombBuff : Buff
{
	private int	_charges = 2;

	protected override string Description
	{
		get { return "When it dies, explode for " + _charges.ToString() + " - 2 * distance to the Bomb damage."; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			increaseCharges(2);
			_buffed_character.receiveDamage(_buffed_character, 2);
		}

		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character )
		{
			for (int radius = 1; radius <= _charges / 2; ++radius)
				foreach (Character character in Board.getCharactersInRange(_buffed_character.Coord.createSafeCenteredCircleCoords, radius))
				{
					int damage = _charges - 2 * (radius - 1);
					character.receiveDamage(_buffed_character, damage);
				}
		}
	}

	private void increaseCharges(int amount)
	{
		_charges += amount;
		updateUI();
	}
}