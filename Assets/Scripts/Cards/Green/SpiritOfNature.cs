using UnityEngine;

public class SpiritOfNature : Unit
{
	protected override void onSummon()
	{
		receiveGlobalModifier(typeof(SpiritOfNatureBuff), source:this);
	}
}