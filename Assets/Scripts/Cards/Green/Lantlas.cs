using UnityEngine;

public class Lantlas : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(LantlasBuff));
	}
}