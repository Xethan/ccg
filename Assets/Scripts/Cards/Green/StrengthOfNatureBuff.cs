using UnityEngine;

public class StrengthOfNatureBuff : Buff
{
	public override int getAttackAfterBuff(int attack)
	{
		return attack + 1;
	}

	public override int getHealthAfterBuff(int health)
	{
		return health + 2;
	}

	public override int getMaxHealthAfterBuff(int max_health)
	{
		return max_health + 2;
	}
}