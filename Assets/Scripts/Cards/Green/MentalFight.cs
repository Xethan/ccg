using UnityEngine;
using System; // Predicate / Action

public class MentalFight : CombatTrick
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character && target.Character.Owner == _owner;
	}

	protected override void doEffect(Tile target)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();

			return second_target != null && second_target.Character is Unit;
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			target.Character.fight(second_target.Character);
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}
