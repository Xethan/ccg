using UnityEngine;

public class VertheniusBuff : Buff
{
	int _attack_buff = 0;
	int _health_buff = 0;

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon || trigger.type == Trigger.e_type.Consume )
		{
			if (_buffed_character.Owner.ManaReserve.ManaMax[1] >= 5)
			{
				_attack_buff = 2;
				_health_buff = 5;
				updateUI();
			}
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack + _attack_buff;
	}
	public override int getHealthAfterBuff(int health)
	{
		return health + _health_buff;
	}
	public override int getMaxHealthAfterBuff(int max_health)
	{
		return max_health + _health_buff;
	}
}
