using UnityEngine;
using System.Collections.Generic;

public class Trampler : Unit
{
	protected override void onSummon()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new Trample(character:this)
		};
	}
}
