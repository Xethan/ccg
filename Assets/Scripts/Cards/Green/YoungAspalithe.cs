using UnityEngine;
using System;
using System.Collections.Generic;

public class YoungAspalithe : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(YoungAspalitheBuff));
	}
}