using UnityEngine;

public class NurtureBuff : Buff
{
	public override int getHealthAfterBuff(int health)
	{
		return health + 2;
	}

	public override int getMaxHealthAfterBuff(int max_health)
	{
		return max_health + 2;
	}
}