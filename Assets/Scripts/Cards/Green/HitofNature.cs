using UnityEngine;
using System; // Predicate / Action

public class HitofNature : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is General && target.Character.Owner == _owner;
	}

	protected override void doEffect(Tile target)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();

			return second_target != null && second_target.Character is Unit && second_target.Character.Owner != _owner;
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			second_target.Character.receiveDamage(target.Character, Convert.ToInt32(Math.Round(Math.Round(1.5f * target.Character.Attack, 1), MidpointRounding.AwayFromZero)));
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}
