using UnityEngine;

public class Stompy6 : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(ToughBuff), param : 2);
	}
}