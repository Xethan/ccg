using UnityEngine;
using System.Collections.Generic;

public class ArmorOfNature : CombatTrick
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null;
	}

	protected override void doEffect(Tile target)
	{
        target.Character.receiveModifier(typeof(TemporaryBuff), buff:typeof(TrampleBuff), param2:1);
	}
}
