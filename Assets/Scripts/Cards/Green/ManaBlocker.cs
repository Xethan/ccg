using UnityEngine;

public class ManaBlocker : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			Board.getEnnemyPlayer().ManaReserve.receiveManaCostModif(typeof(ManaBlockerManaCostModif));
		base.onTrigger(trigger);
	}
}