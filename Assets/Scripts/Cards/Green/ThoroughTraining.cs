using UnityEngine;

public class ThoroughTraining : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		General general = _character as General;
		general.receiveModifier(typeof(IncreasedAttackBuff), param:general.Level);
	}
}