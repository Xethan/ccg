using UnityEngine;

public class Stompy5 : Unit
{
	protected override void onSummon()
	{
			receiveModifier(typeof(ToughBuff), param : 1);
			receiveModifier(typeof(RegenerationBuff), param : 2);
	}
}