using UnityEngine;

public class ThoroughTrainingBuff : Buff
{
	public override int getAttackAfterBuff(int attack)
	{
		return attack + 1;
	}
}