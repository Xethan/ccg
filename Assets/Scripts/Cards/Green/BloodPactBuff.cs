using UnityEngine;

public class BloodPactBuff : Buff
{
	private bool _first_part = true;

	protected override string Description
	{
		get { return (_first_part ? "+1 attack" : "Vulnerable 1") + "until end of turn."; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			if ( _first_part == true )
			{
				_first_part = false;
				updateUI();
			}
			else
			{
				_buffed_character.loseModifier(this);
				GameObject.Destroy(this);
			}
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return _first_part == true ? attack + 1 : attack;
	}

	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character && _first_part == false )
			return amount + 1;
		return amount;
	}
}