using UnityEngine;
using System.Collections.Generic;

public class ClawsOfNature : Artifact
{
    protected override int BaseDurability { get { return 9; } }
    protected override string Name { get { return "Claws of Nature"; } }
    protected override string Description { get { return "+3 attack"; } }

    public override bool isTargetValid(Tile target)
    {
        return target.Character != null;
    }

    protected override List<Modifier> createModifiers(Character equipped_character)
    {
        return new List<Modifier> {
            equipped_character.createModifier(typeof(IncreasedAttackBuff), param:3)
        };
    }
}