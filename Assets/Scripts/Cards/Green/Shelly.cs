using UnityEngine;

public class Shelly : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(ShellBuff), param : 4);
	}
}