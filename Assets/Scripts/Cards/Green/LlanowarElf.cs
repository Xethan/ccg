using UnityEngine;

public class LlanowarElf : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			Owner.ManaReserve.changeNbConsumesLeft(1);
		base.onTrigger(trigger);
	}
}