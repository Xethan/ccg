using UnityEngine;
using System.Collections.Generic;

public class SpectralBlade : Artifact
{
    protected override int BaseDurability { get { return 9; } }
    protected override string Name { get { return "Spectral Blade"; } }
    protected override string Description { get { return "+2 attack.<br>Heal for 3 when killing an ennemy unit."; } }

    public override bool isTargetValid(Tile target)
    {
        return target.Character != null;
    }

    protected override List<Modifier> createModifiers(Character equipped_character)
    {
        return new List<Modifier> {
            equipped_character.createModifier(typeof(SpectralBladeBuff))
        };
    }
}
