using UnityEngine;

public class EmbodimentOfSavageryBuff : GlobalCharacterModifier
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == this._source )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

	public override int getAttackAfterBuff(Character character, int attack)
	{
		return character.Owner != _buffed_character.Owner ? attack - 1 : attack;
	}
}