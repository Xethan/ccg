using UnityEngine;

public class AspalitheTrainer : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(AspalitheTrainerBuff));
	}
}