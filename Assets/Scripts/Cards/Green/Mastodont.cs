using UnityEngine;
using System.Collections.Generic;

public class Mastodont : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(MastodontBuff));
		_activated_abilities = new List<ActivatedAbility>
		{
			new Trample(character:this)
		};
	}
}
