using UnityEngine;

public class AngerOfNature : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(AngerOfNatureBuff));
	}
}