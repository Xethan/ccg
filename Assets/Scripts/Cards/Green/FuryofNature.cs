using UnityEngine;
using System; // Predicate / Action

public class FuryofNature : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is Unit;
	}

	protected override void doEffect(Tile target)
	{		
		target.Character.receiveModifier(
			typeof(TemporaryIncreasedAttackBuff),
			param:3,  // bonus attack
			param2:2,  // number of turns
			should_display_buff_ui:true
		);
		target.Character.receiveArmor(3);
	}
}
