using UnityEngine;

public class ElderAspalitheBuff : Buff
{
	private readonly int[]	_cost_ability = {3, 1, 0, 0, 0, 0};

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Activated_ability && trigger.receiver == _buffed_character )
		{
			ManaReserve mana_reserve = _buffed_character.Owner.ManaReserve;

			if ( mana_reserve.payManaCost(this, _cost_ability) )
				TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		}
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character == null && _buffed_character.Coord.isNearby(target.Coord);
	}

	private void doEffect(GameObject gameobject)
	{
		string prefab_path = Factions._factions_name[1] + "/Young Aspalithe";
		Tile target = gameobject.GetComponent<Tile>();

		Character young_aspalithe = Tools.instantiateFromPath(prefab_path).GetComponent<Character>();
		young_aspalithe.Owner = _buffed_character.Owner;
		young_aspalithe.summon(target);
	}
}