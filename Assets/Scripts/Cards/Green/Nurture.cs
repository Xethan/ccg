using UnityEngine;

public class Nurture : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		foreach (Character character in Board.getCharactersInRange(_character.Coord.createSafeCenteredSquareCoords, radius:1))
			if ( character is Unit && character.Owner == _owner )
				character.receiveModifier(typeof(NurtureBuff));
	}
}