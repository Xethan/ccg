using UnityEngine;

public class Angryofaspal : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(AngryofaspalBuff));
	}
}