using UnityEngine;
using UnityEngine.UI;

public class StrengthOfNatureTile : SpecialTile
{
	protected override string Name { get { return "Strength of Nature"; } }
	protected override string Description
	{
		get { return "If an ally ends its turn on this tile, consume it to give the ally +1/+2"; }
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && _tile.Character is Unit && _tile.Character.Owner == _owner )
		{
			_tile.Character.receiveModifier(typeof(StrengthOfNatureBuff));
			_tile.loseSpecialTile(_owner);
		}
	}
}