using UnityEngine;

public class BlacksmithOfWar : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		CardSlot card_slot = gameobject.GetComponent<CardSlot>();
		return card_slot != null && card_slot.Card is Unit;
	}

	private void doEffect(GameObject gameobject)
	{
		CardSlot card_slot = gameobject.GetComponent<CardSlot>();
		int[] mana_cost = card_slot.Card.ManaCost.raw_cost;
		mana_cost[Factions.neutral] += 1;
		mana_cost[Factions.faction_1] += 1;
		card_slot.Card.ManaCost = new ManaCost(mana_cost);
		card_slot.updateUI();
		card_slot.Card.receiveModifier(typeof(IncreasedHealthBuff), param:3);
		card_slot.Card.receiveModifier(typeof(IncreasedAttackBuff), param:1);
		card_slot.Card.receiveModifier(
			typeof(ToughBuff),
			param:1,
			should_display_buff_ui:true
		);
	}
}