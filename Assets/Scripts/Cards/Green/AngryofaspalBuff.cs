using UnityEngine;
using System.Linq;

public class AngryofaspalBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		updateUI();
	}

	public override int getAttackAfterBuff(int attack)
	{
		return _buffed_character.Owner.Generals.Max(general => general.Attack);
	}
}