public class SpectralBladeBuff : Buff
{
    public override int getAttackAfterBuff(int amount)
    {
        return amount + 2;
    }

    public override void onTrigger(Trigger trigger)
    {
        if (
            trigger.type == Trigger.e_type.Damage
            && trigger.source == _buffed_character
            && trigger.receiver.Health <= 0
        )
            _buffed_character.receiveHeal(3);
    }
}