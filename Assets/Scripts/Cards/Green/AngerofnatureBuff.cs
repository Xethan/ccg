using UnityEngine;

public class AngerOfNatureBuff : Buff
{
	private int _attack_buff = 0;

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Attack && trigger.source == _buffed_character )
		{
			_attack_buff += 2;
			updateUI();
		}
		if ( trigger.type == Trigger.e_type.Attack && trigger.receiver == _buffed_character )
		{
			_attack_buff -= 1;
			updateUI();
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack + _attack_buff;
	}
}