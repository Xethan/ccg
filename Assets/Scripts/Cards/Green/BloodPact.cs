using UnityEngine;
using System.Collections.Generic;

public class BloodPact : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		foreach (Tile tile in Board._board)
		{
			Character character = tile.Character;
			if (
				Board.isCharacterAllied(_owner, character)
				&& (character == _character || character is Unit)
			)
				character.receiveModifier(typeof(BloodPactBuff), should_display_buff_ui:true);
		}
	}
}