using UnityEngine;

public class Zersycc : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();

		return target != null
			&& target.Character is Unit
			&& target.Character.Coord.isNearby(this.Coord)
			&& target.Character.Health <= this.Attack;
	}

	private void doEffect(GameObject gameobject)
	{
		Character target = gameobject.GetComponent<Tile>().Character;

		target.receiveDamage(this, this.Attack);
		this.receiveDamage(target, target.Attack);
	}
}