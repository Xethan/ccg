using UnityEngine;

public class ManaBlockerManaCostModif : ManaCostModif
{
	public override int[] getManaCostAfterModif(System.Object source, int[] mana_cost)
	{
		int[] mana_cost_after_modif = (int[])mana_cost.Clone();
		if ( source is Card && !(source is CombatTrick) && !(source is BattleCry) )
		{
			if ( mana_cost_after_modif[Factions.faction_1] > 0 )
				mana_cost_after_modif[Factions.faction_1] += 1;
			if ( mana_cost_after_modif[Factions.faction_2] > 0 )
				mana_cost_after_modif[Factions.faction_2] += 1;
			if ( mana_cost_after_modif[Factions.faction_3] > 0 )
				mana_cost_after_modif[Factions.faction_3] += 1;
		}
		return mana_cost_after_modif;
	}

	public override void manaCostWasPayed(System.Object source, int[] mana_cost)
	{
		if ( source is Card && !(source is CombatTrick) && !(source is BattleCry) )
			_affected_player.ManaReserve.loseManaCostModif(this);
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _affected_player )
			_affected_player.ManaReserve.loseManaCostModif(this);
	}
}