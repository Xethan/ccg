using UnityEngine;
using System.Collections.Generic;

public class Berserk : General
{
	protected override string BattleCryPrefabPath { get { return "Green/Blood Pact"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 4, 16}; } }

	private bool _has_reset_move_and_attack_this_turn = false;

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && !(trigger.receiver is Token)
				 && trigger.receiver.Owner == _owner && !Board.isMyTurn(_owner) )
			gainXp(1);
		
		if ( trigger.type == Trigger.e_type.Attack && trigger.source == this && _level == 2
			 && _has_reset_move_and_attack_this_turn == false )
		{
			reactivate(Character.e_flags.Can_move_attack);
			_has_reset_move_and_attack_this_turn = true;
		}

		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _owner )
			_has_reset_move_and_attack_this_turn = false;

		base.onTrigger(trigger);
	}
}