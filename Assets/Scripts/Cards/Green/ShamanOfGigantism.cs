using UnityEngine;
using System.Collections.Generic;

public class ShamanOfGigantism : Unit
{
	protected override void onSummon()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new Trample(character:this)
		};
		TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		CardSlot card_slot = gameobject.GetComponent<CardSlot>();
		return card_slot != null && card_slot.Card is Unit;
	}

	private void doEffect(GameObject gameobject)
	{
		CardSlot card_slot = gameobject.GetComponent<CardSlot>();
		card_slot.Card.receiveModifier(typeof(TrampleBuff), should_display_buff_ui:true);
	}
}