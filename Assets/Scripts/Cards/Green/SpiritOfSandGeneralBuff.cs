using UnityEngine;

public class SpiritOfSandGeneralBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if(Board.isMyTurn(_buffed_character.Owner) && trigger.type == Trigger.e_type.End_turn)
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

	public override int getMovementAfterBuff(int movement)
	{
		return movement + 1;
	}
}