using UnityEngine;

public class HeraldOfWar : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
		{
			int nb_units_to_buff = 2;
			foreach ( Card card in _owner.iterateOverDeck() )
			{
				if ( card is Unit )
				{
					(card as Unit).receiveModifier(typeof(IncreasedHealthBuff), param : 2);
					nb_units_to_buff -= 1;
					if ( nb_units_to_buff == 0 )
						break;
				}
			}
		}

		base.onTrigger(trigger);
	}
}