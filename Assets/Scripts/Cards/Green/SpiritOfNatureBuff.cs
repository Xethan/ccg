using UnityEngine;

public class SpiritOfNatureBuff : GlobalCharacterModifier
{
	private bool isAffected(Character character)
	{
		return character is General && character.Owner == _buffed_character.Owner;
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == this._source )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

	public override int getAttackAfterBuff(Character character, int attack)
	{
		return isAffected(character) ? attack + 1 : attack;
	}

}