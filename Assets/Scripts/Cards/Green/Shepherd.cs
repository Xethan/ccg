using UnityEngine;
using System.Collections.Generic;

public class Shepherd : General
{
	private int _max_nb_allied_units_at_start_of_turn = 0;

	protected override string BattleCryPrefabPath { get { return "Green/Nurture"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 4, 10}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Start_turn && trigger.player == _owner )
		{
			int nb_allied_units = countNbAlliedUnitsOnBoard();
			if ( nb_allied_units > _max_nb_allied_units_at_start_of_turn)
			{
				gainXp(nb_allied_units - _max_nb_allied_units_at_start_of_turn);
				_max_nb_allied_units_at_start_of_turn = nb_allied_units;
			}
		}
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver.Owner == _owner && _level == 2 )
				trigger.receiver.receiveModifier(typeof(IncreasedHealthBuff), param : 2);

		base.onTrigger(trigger);
	}

	private int countNbAlliedUnitsOnBoard()
	{
		int nb_allied_units = 0;
		for (int x = 0; x < Board.nb_columns; ++x)
		{
			for (int y = 0; y < Board.nb_lines; ++y)
			{
				Character character = Board.getCharacter(new Coord(x, y));
				if ( character is Unit && !(character is Token) && character.Owner == _owner )
					nb_allied_units += 1;
			}
		}
		return nb_allied_units;
	}
}