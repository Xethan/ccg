using UnityEngine;

public class NaturalEnergy : CombatTrick
{
	private readonly int[]	_mana_boost = {1, 1, 0, 0, 0, 0};

	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		_owner.ManaReserve.increaseManaUntilEndOfTurn(_mana_boost);
	}
}