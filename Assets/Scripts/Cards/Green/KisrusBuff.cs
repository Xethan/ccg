using UnityEngine;
using System.Linq;

public class KisrusBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Attack && trigger.source.Owner == _buffed_character.Owner && trigger.source is Unit)
		{
			Unit source = trigger.source as Unit;
			source.receiveModifier(typeof(IncreasedHealthBuff), param : 2);
		}
	}

}