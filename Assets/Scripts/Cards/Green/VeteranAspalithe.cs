using UnityEngine;

public class VeteranAspalithe : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(VeteranAspalitheBuff));
	}
}