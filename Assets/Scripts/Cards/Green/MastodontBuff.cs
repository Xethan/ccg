using UnityEngine;

public class MastodontBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Trample && trigger.source == _buffed_character)
			foreach (Character character in Board.getCharactersInRange(trigger.tile.Coord.createSafeCenteredCircleCoords, radius:1))
				character.receiveDamage(_buffed_character, 2);
	}
}