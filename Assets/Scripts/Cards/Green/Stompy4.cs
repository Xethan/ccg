using UnityEngine;

public class Stompy4 : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(HunterBuff), param : 3);
	}
}