using UnityEngine;

public class StrengthOfNature : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		foreach (General general in _owner.Generals)
			general.receiveModifier(
				typeof(TemporaryIncreasedAttackBuff),
				param:3, // bonus attack
				param2:1, // number of turns
				should_display_buff_ui:true
			);
	}
}