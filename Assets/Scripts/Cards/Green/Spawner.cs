using UnityEngine;
using System;
using System.Collections;

public class Spawner : Unit
{
	private string _token_prefab_path = "Tokens/Puppy";

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return target != null && target.Character == null && _coord.isNearby(target.Coord);
	}

	private void doEffect(GameObject gameobject)
	{
		StartCoroutine(choseSecondTarget(gameobject));
	}

	private IEnumerator choseSecondTarget(GameObject gameobject)
	{
		Tile first_target = gameobject.GetComponent<Tile>();

		Predicate<GameObject> isSecondTargetValid = (GameObject second_gameobject) =>
		{
			Tile second_target = second_gameobject.GetComponent<Tile>();
			return ( second_target != null && second_target.Character == null
				     && second_target != first_target && _coord.isNearby(second_target.Coord) );
		};

		Action<GameObject> card_effect = (GameObject second_gameobject) =>
		{
			Tile second_target = second_gameobject.GetComponent<Tile>();

			Character token = Tools.instantiateCardFromPath<Character>(_token_prefab_path, _owner);
			token.summon(first_target);

			Character second_token = Tools.instantiateCardFromPath<Character>(_token_prefab_path, _owner);
			second_token.summon(second_target);
		};

		yield return null;
		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}