using UnityEngine;

public class Kirnous : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(AspalitheTrainerBuff));
	}
}