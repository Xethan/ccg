using UnityEngine;

public class Kisrus : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(KisrusBuff));
	}
}