using UnityEngine;

public class AHeal : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveHeal(10);
	}
}