using UnityEngine;

public class YoungAspalitheBuff : Buff
{
	private readonly int[]	_cost_ability = {3, 1, 0, 0, 0, 0};

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Activated_ability && trigger.source == _buffed_character )
		{
			ManaReserve mana_reserve = _buffed_character.Owner.ManaReserve;

			if ( mana_reserve.payManaCost(this, _cost_ability) )
			{
				string prefab_path = Factions._factions_name[1] + "/Veteran Aspalithe";
				Character veteran_aspalithe = Tools.instantiateFromPath(prefab_path).GetComponent<Character>();
				veteran_aspalithe.Owner = _buffed_character.Owner;
				veteran_aspalithe.summon(Board.getTile(_buffed_character.Coord));
				_buffed_character.destroy();
			}
		}
	}
}