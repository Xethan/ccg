using UnityEngine;

public class AspalitheTrainerBuff : Buff
{
	private int _attack_buff = 0;

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.receiver == _buffed_character )
		{
			_attack_buff++;
			updateUI();
		}
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack + _attack_buff;
	}
}