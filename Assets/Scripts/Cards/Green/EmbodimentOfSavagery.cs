using UnityEngine;

public class EmbodimentOfSavagery : Unit
{
	protected override void onSummon()
	{		
		receiveGlobalModifier(typeof(EmbodimentOfSavageryBuff), source:this);
	}
}