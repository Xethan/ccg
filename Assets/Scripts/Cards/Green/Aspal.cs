using UnityEngine;

public class Aspal : General
{
	protected override string BattleCryPrefabPath { get { return "Green/Thorough Training"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 4, 10}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Damage && trigger.source == this
			&& !(trigger.receiver is Token)
			&& trigger.receiver.Health <= 0
			&& Board.isMyTurn(_owner)
			)
			gainXp(1);
		base.onTrigger(trigger);
	}
}