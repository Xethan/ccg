using UnityEngine;

public class IncreasedConsumption : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		_owner.ManaReserve.changeNbConsumesLeft(1);
		_owner.draw(1);
	}
}