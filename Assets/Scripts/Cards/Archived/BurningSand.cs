public class BurningSand : Spell
{
	private string	_sand_storm_tile_prefab_path = "Tiles/Burning Sand";

	public override bool isTargetValid(Tile target)
	{
		return Board.isThereAlliedCharacterInRange(_owner, target.Coord, 2);
	}

	protected override void doEffect(Tile target)
	{
		target.setSpecialTile(_sand_storm_tile_prefab_path, _owner);
	}
}