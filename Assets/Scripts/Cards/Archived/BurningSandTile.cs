using UnityEngine;
using UnityEngine.UI;

public class BurningSandTile : SpecialTile
{
	private int		_nb_turns_left = 2;

	protected override string Name { get { return "Burning Sand"; } }
	protected override string Description
	{
		get
		{
			return "If an ennemy character ends their turn on this tile, they receive 4 damage. "
			       + _nb_turns_left + (_nb_turns_left > 1 ? " uses" : " use") + " left.";
		}
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _owner )
		{
			if ( _tile.Character != null && _tile.Character.Owner != _owner )
				_tile.Character.receiveDamage(_tile.Character, 4);

			_nb_turns_left -= 1;
			if ( _nb_turns_left == 0 )
				_tile.loseSpecialTile(_owner);
			updateUI();
		}

	}
}