using UnityEngine;

public class Grimmali : Unit
{
	protected override void onSummon()
	{
		Card card = Tools.createCardInPlayerHand<Card>("Green - Purple/Grimmali", this.Owner);
		card.receiveModifier(typeof(FleetingBuff), source:card, should_display_buff_ui:true);
	}
}