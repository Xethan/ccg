using UnityEngine;

public class Stomper : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
		{
			for (int x = _coord.X - 2; x <= _coord.X + 2; ++x)
			{
				for (int y = _coord.Y - 2; y <= _coord.Y + 2; ++y)
				{
					Coord test_coord = new Coord(x, y);
					if ( test_coord != _coord && test_coord.isInsideBoard() && Board.getCharacter(test_coord) )
						Board.getCharacter(test_coord).receiveDamage(this, 2 * (3 - _coord.distance(test_coord)));
				}
			}
		}
		base.onTrigger(trigger);
	}
}