using UnityEngine;
using System.Collections.Generic;

public class ChainLightning : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is General && target.Character.Owner == _owner;
	}

	protected override void doEffect(Tile target)
	{
		List<Character> targets = new List<Character>();
		Character target_general = target.Character;

		recursiveAddAdjacentCharactersToTargets(targets, target_general);
		foreach (Character targeted_character in targets)
			if ( targeted_character != target_general )
				targeted_character.receiveDamage(target_general, targets.Count - 1);
	}

	private void recursiveAddAdjacentCharactersToTargets(List<Character> targets, Character center_character)
	{
		foreach (Character character in Board.getCharactersInRange(center_character.Coord.createSafeCenteredCircleCoords, radius:1))
		{
			if ( !targets.Contains(character) )
			{
				targets.Add(character);
				recursiveAddAdjacentCharactersToTargets(targets, character);
			}
		}
	}
}