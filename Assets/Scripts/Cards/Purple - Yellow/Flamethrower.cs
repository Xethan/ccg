using UnityEngine;
using System; // Predicate / Action

public class Flamethrower : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null && target.Character.Owner == _owner;
	}

	protected override void doEffect(Tile first_target)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			return second_target != null && second_target.Coord.distance(first_target.Coord) == 1;
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			Coord[] target_coords = new Coord[4];
			Coord direction_cone = second_target.Coord - first_target.Coord;
			Coord perpendicular_direction = new Coord(1 - Math.Abs(direction_cone.X), 1 - Math.Abs(direction_cone.Y));

			target_coords[0] = second_target.Coord;
			target_coords[1] = second_target.Coord + direction_cone;
			target_coords[2] = target_coords[1] + perpendicular_direction;
			target_coords[3] = target_coords[1] + (perpendicular_direction * -1);

			for (int i = 0; i < target_coords.Length; ++i)
			{
				if ( target_coords[i].isOutOfBoard() == false )
				{
					Character target_character = Board.getCharacter(target_coords[i]);

					if ( target_character != null )
						target_character.receiveDamage(first_target.Character, 5);
				}
			}
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}