using UnityEngine;

public class HeartOfTheSand : Unit
{
	protected override void onSummon()
	{		
		receiveModifier(typeof(HeartOfTheSandBuff));
	}
}