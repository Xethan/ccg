using UnityEngine;

public class PlantTheBomb : Spell
{
	private string	_bomb_prefab_path = "Tokens/Bomb";

	public override bool isTargetValid(Tile target)
	{
		return target.Character == null && Board.isThereAlliedCharacterInRange(_owner, target.Coord, 2);;
	}

	protected override void doEffect(Tile target)
	{
		Character bomb = Tools.instantiateCardFromPath<Character>(_bomb_prefab_path, owner:null);
		bomb.summon(target);
	}
}