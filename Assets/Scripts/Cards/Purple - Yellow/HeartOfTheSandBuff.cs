using UnityEngine;

public class HeartOfTheSandBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player == _buffed_character.Owner )
            foreach (Character character in Board.getCharactersInRange(_buffed_character.Coord.createSafeCenteredSquareCoords, radius:1))
				if ( character.Owner != _buffed_character.Owner )
					character.receiveDamage(_buffed_character, 2);
	}
}