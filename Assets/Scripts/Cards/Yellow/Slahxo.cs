using UnityEngine;

public class Slahxo : Unit
{
	protected override void onSummon()
	{
        foreach (Character character in Board.getCharactersInRange(_coord.createSafeCenteredSquareCoords, radius:1))
            if ( Board.isCharacterAllied(_owner, character) && character is Unit)
                character.reactivate(Character.e_flags.Can_move_attack);
	}
}