using UnityEngine;
using UnityEngine.UI;
using System; // Exceptions

public class ShiftingSandTile : SpecialTile
{
	private int		_level = 2;
	private bool	_starting_level_already_set = false;

	public int Level { get { return _level; } }

	void Start()
	{
		updateUI();
	}

	public void setStartingLevel(int starting_level)
	{
		if ( _starting_level_already_set )
			throw new InvalidOperationException("Starting level already set");

		_level = starting_level;
		_starting_level_already_set = true;
		updateUI();
	}

	public void levelUp()
	{
		_level += 2;
		updateUI();
	}

	public void merge(ShiftingSandTile other)
	{
		_level += other.Level;
		updateUI();
	}

	new private void updateUI()
	{
		_special_tile_hover_ui.updateUI(this.gameObject.GetComponent<Image>().sprite, "Shifting Sand",
			                            "If an ennemy character ends their turn on this tile, they receive "
			                            + _level + " damage and this tile's damage increases by two.");
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn && trigger.player != _owner )
		{
			if ( _tile.Character != null && _tile.Character.Owner != _owner )
			{
				_tile.Character.receiveDamage(this, _level);
				levelUp();
				Trigger.activate(Trigger.e_type.Tile_proc, tile: _tile);
			}
		}

	}

	public static void summonOrLevelUpShiftingSand(Tile tile, Player player)
	{
		SpecialTile special_tile = tile.getSpecialTile(player);
		if ( special_tile is ShiftingSandTile )
		{
			ShiftingSandTile shifting_sand = special_tile as ShiftingSandTile;
			shifting_sand.levelUp();
		}
		else
			tile.setSpecialTile("Tiles/Shifting Sand", player);
	}

	public void spread()
	{
		foreach (
			Coord candidate_coord
			in _tile.Coord.createSafeCenteredCircleCoords(radius:1)
		) {
			Tile tile_to_spread_to = Board.getTile(candidate_coord);

			if (
				tile_to_spread_to.getSpecialTile(_owner)
				is ShiftingSandTile other_shifting_sand
			)
				other_shifting_sand.merge(this);
			else
				(
					tile_to_spread_to.setSpecialTile("Tiles/Shifting Sand", _owner)
					as ShiftingSandTile
				).setStartingLevel(_level);
		}
	}
}