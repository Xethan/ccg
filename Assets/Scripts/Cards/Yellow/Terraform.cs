using UnityEngine;

public class Terraform : Spell
{
    public override bool isTargetValid(Tile target)
    {
        return target.getSpecialTile(_owner) is ShiftingSandTile;
    }

    protected override void doEffect(Tile target)
    {
        (target.getSpecialTile(_owner) as ShiftingSandTile).spread();
    }
}