using UnityEngine;

public class FrenzyOfNature : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.reactivate(Character.e_flags.Can_move_attack);
        target.Character.receiveArmor(3);
	}
}