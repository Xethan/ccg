using UnityEngine;

public class Insec : Unit
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
			TargetingPanel.doTargetingAbility(isTargetValid, doEffect);
		base.onTrigger(trigger);
	}

	private bool isTargetValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return (
			target != null
			&& target.Coord.distance(this.Coord) == 1
			&& Board.isSymmetricTeleportValid(_coord, target.Coord)
		);
	}

	private void doEffect(GameObject gameobject)
	{
		Character target_character = gameobject.GetComponent<Tile>().Character;

		symmetricTeleport(this, target_character.Coord);
		target_character.push(this, target_character.Coord - _coord, 2);
	}
}