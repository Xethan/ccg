using UnityEngine;

public class KhaZix : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(KhaZixBuff));
	}
}