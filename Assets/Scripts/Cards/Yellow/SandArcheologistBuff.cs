using UnityEngine;

public class SandArcheologistBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character )
		{
			Tile tile = Board.getTile(_buffed_character.Coord);
			tile.setSpecialTile("Tiles/Sand Burrow", _buffed_character.Owner);
		}
	}
}