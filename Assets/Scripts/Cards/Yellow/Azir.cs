using UnityEngine;
using System.Collections.Generic;

public class Azir : General
{
	protected override string BattleCryPrefabPath { get { return "Yellow/Expand The Pack"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 4, 16}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Tile_proc && trigger.tile.getSpecialTile(_owner) is SandBurrowTile )
			gainXp(1);

		base.onTrigger(trigger);
	}

	protected override void onLevel2()
	{
		receiveModifier(typeof(AzirBuff));
	}
}