using UnityEngine;

public class WindRunnerRewindBuff : Buff
{
	private Coord	_initial_position;

	protected override string Description
	{
		get
		{
			if ( _initial_position is null )  // _initial_position is not yet set
				return "";

			return "This character will go back to tile "
					+ _initial_position.Description
					+ " at the end of the turn.";
		}
	}

	void Start()
	{
		_initial_position = _buffed_character.Coord;
		updateUI();
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_buffed_character.teleport(_buffed_character, _initial_position);
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}
}