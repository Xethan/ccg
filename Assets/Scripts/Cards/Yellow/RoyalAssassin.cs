using UnityEngine;

public class RoyalAssassin : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(ReducedDamageFromEnnemyGeneralsBuff), param:2);
	}
}