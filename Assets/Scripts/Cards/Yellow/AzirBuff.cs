using UnityEngine;

public class AzirBuff : GlobalCharacterModifier
{
	private bool isAlliedSandDevil(Character character)
	{
		return character is SandDevil && character.Owner == _buffed_character.Owner;
	}

	public override int getAttackAfterBuff(Character character, int attack)
	{
		return isAlliedSandDevil(character) ? attack + 1 : attack;
	}
}