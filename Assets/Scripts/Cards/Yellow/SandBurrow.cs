using UnityEngine;
using System; // Predicate / Action

public class SandBurrow : Spell
{
	private string	_sand_burrow_tile_prefab_path = "Tiles/Sand Burrow";

	public override bool isTargetValid(Tile target)
	{
		return Board.isThereAlliedCharacterInRange(_owner, target.Coord, 2);
	}

	protected override void doEffect(Tile first_target)
	{
		first_target.setSpecialTile(_sand_burrow_tile_prefab_path, _owner);

		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			return ( second_target != null
				     && second_target != first_target
				     && Board.isThereAlliedCharacterInRange(_owner, second_target.Coord, 2) );
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			second_target.setSpecialTile(_sand_burrow_tile_prefab_path, _owner);
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}