using UnityEngine;

public class FourWinds : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character != null;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveDamage(this, 6);
	}
}