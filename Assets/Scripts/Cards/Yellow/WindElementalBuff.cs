using UnityEngine;

public class WindElementalBuff : Buff
{
	private int	_charges = 0;

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == _buffed_character )
			updateUI();
		if ( trigger.type == Trigger.e_type.Push )
		{
			_charges += 1;
			updateUI();
		}
	}

	public override int getMovementAfterBuff(int movement)
	{
		return movement - 2 + _charges;
	}

	public override int getAttackAfterBuff(int attack)
	{
		return attack + _charges;
	}
}