using UnityEngine;
using System.Collections.Generic;
using System;

public class WindRunner : Unit
{
	private const int	_charges_per_turn = 2;
	private int			_charges = _charges_per_turn;

	protected override void onSummon()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new WindRunnerReactivateMoveAbility(wind_runner:this),
			new WindRunnerPushAbility(wind_runner:this),
			new WindRunnerBlinkAbility(wind_runner:this),
			new WindRunnerRewindAbility(wind_runner:this),
			new WindRunnerRechargeAbility(wind_runner:this)
		};
	}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.Is("Start_turn") && trigger.player == _owner )
			_charges = _charges_per_turn;
		base.onTrigger(trigger);
	}

	public bool useCharge()
	{
		if ( _charges > 0 )
		{
			_charges -= 1;
			return true;
		}
		return false;
	}

	public void gainCharge()
	{
		_charges += 1;
	}
}

public class WindRunnerReactivateMoveAbility : ActivatedAbility
{
	private WindRunner	_wind_runner;

	public override string Cost { get { return "1 charge"; } }
	public override string Name { get { return "Reactivate movement"; } }

	public WindRunnerReactivateMoveAbility(WindRunner wind_runner) : base(wind_runner)
	{
		_wind_runner = wind_runner;
	}

	public override bool payCost()
	{
		return _wind_runner.useCharge();
	}

	public override void doEffect()
	{
		_wind_runner.reactivate(Character.e_flags.Can_move);
	}
}

public class WindRunnerPushAbility : ActivatedAbility
{
	private WindRunner	_wind_runner;

	public override string Cost { get { return "1 charge"; } }
	public override string Name { get { return "Push target character 1 tile away"; } }

	public WindRunnerPushAbility(WindRunner wind_runner) : base(wind_runner)
	{
		_wind_runner = wind_runner;
	}

	public override bool payCost()
	{
		return _wind_runner.useCharge();
	}

	public override void doEffect()
	{
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			if ( target != null )
			{
				Coord coord_diff = target.Coord - _character.Coord;
				Coord end_coord = target.Coord + coord_diff;

				return (
					target.Character != null
					&& _wind_runner.Coord.distance(target.Coord) == 1
					&& end_coord.isInsideBoard()
					&& Board.getTile(end_coord).Character == null
				);
			}
			return false;
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			target.Character.push(_wind_runner, target.Coord - _wind_runner.Coord, 1);
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}

public class WindRunnerBlinkAbility : ActivatedAbility
{
	private WindRunner	_wind_runner;

	public override string Cost { get { return "1 charge"; } }
	public override string Name { get { return "Symmetrically teleport with respect to target adjacent character."; } }

	public WindRunnerBlinkAbility(WindRunner wind_runner) : base(wind_runner)
	{
		_wind_runner = wind_runner;
	}

	public override bool payCost()
	{
		return _wind_runner.useCharge();
	}

	public override void doEffect()
	{
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			return (
				target != null
				&& target.Coord.distance(_wind_runner.Coord) == 1
				&& Board.isSymmetricTeleportValid(_wind_runner.Coord, target.Coord)
			);
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			_wind_runner.symmetricTeleport(_wind_runner, target.Coord);
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}

public class WindRunnerRewindAbility : ActivatedAbility
{
	private WindRunner	_wind_runner;

	public override string Cost { get { return "1 charge"; } }
	public override string Name { get { return "Rewind at the end of the turn"; } }

	public WindRunnerRewindAbility(WindRunner wind_runner) : base(wind_runner)
	{
		_wind_runner = wind_runner;
	}

	public override bool payCost()
	{
		return _wind_runner.useCharge();
	}

	public override void doEffect()
	{
		_wind_runner.receiveModifier(typeof(WindRunnerRewindBuff), should_display_buff_ui:true);
	}
}

public class WindRunnerRechargeAbility : ActivatedAbility
{
	private readonly int[]  _ability_mana_cost = {1, 0, 0, 0, 0, 0};

	private WindRunner	_wind_runner;

	public override string Cost { get { return "1"; } }
	public override string Name { get { return "Gain one charge"; } }

	public WindRunnerRechargeAbility(WindRunner wind_runner) : base(wind_runner)
	{
		_wind_runner = wind_runner;
	}

	public override bool payCost()
	{
		return _wind_runner.Owner.ManaReserve.payManaCost(this, _ability_mana_cost);
	}

	public override void doEffect()
	{
		_wind_runner.gainCharge();
	}
}