using UnityEngine;
using System.Collections.Generic;

public class PressureRise : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		foreach (General general in _owner.Generals)
			general.receiveDamage(_character, 3);
		foreach (General general in Board.getEnnemyPlayer().Generals)
			general.receiveDamage(_character, 3);
	}
}