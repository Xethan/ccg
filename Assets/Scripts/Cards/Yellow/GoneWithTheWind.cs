using UnityEngine;

public class GoneWithTheWind : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is Unit && target.Character.Owner == _owner;
	}

	protected override void doEffect(Tile target)
	{
		target.Character.receiveModifier(typeof(GoneWithTheWindBuff), should_display_buff_ui:true);
	}
}