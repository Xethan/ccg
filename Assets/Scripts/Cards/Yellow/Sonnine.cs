using UnityEngine;
using System.Collections.Generic;

public class Sonnine : General
{
	private List<General>	_generals_under_10_hp = new List<General>();

	protected override string BattleCryPrefabPath { get { return "Yellow/Pressure Rise"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 1, 3}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Damage
			 && trigger.receiver is General && trigger.receiver.Owner != _owner )
		{
			if ( trigger.receiver.Health <= 20 && _level == 1 )
				gainXp(1);
			if ( trigger.receiver.Health <= 10 && !_generals_under_10_hp.Contains(trigger.receiver as General) )
			{
				_generals_under_10_hp.Add(trigger.receiver as General);
				gainXp(1);
			}
		}

		base.onTrigger(trigger);
	}

	protected override void onLevel2()
	{
		_movement += 1;
	}
}