using UnityEngine;

public class FuryOfTheDevils : Spell
{
	private string	_sand_devil_prefab_path = "Tokens/Sand Devil";

	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		for (int x = 0; x < Board.nb_columns; ++x)
		{
			for (int y = 0; y < Board.nb_lines; ++y)
			{
				Coord test_coord = new Coord(x, y);
				if ( Board.isCharacterEnnemy(_owner, Board.getCharacter(test_coord)) )
				{
					Coord shift = _owner.isPlayerOne ? new Coord(-1, 0) : new Coord(1, 0);
					if (Board.getCharacter(test_coord + shift) == null)
					{
						Character sand_devil = Tools.instantiateCardFromPath<Character>(_sand_devil_prefab_path, _owner);
						sand_devil.summon(Board.getTile(test_coord + shift));
					}
				}
			}
		}
	}
}