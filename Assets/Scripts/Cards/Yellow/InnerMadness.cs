using UnityEngine;

public class InnerMadness : Spell
{
	public override bool isTargetValid(Tile target)
	{
		return target.Character is General && target.Character.Owner != _owner;
	}

	protected override void doEffect(Tile target)
	{
		Character target_general = target.Character;
		target_general.receiveDamage(target_general, 3);
		foreach (Character character in Board.getCharactersInRange(target_general.Coord.createSafeCenteredSquareCoords, radius:1))
			if ( character.Owner != _owner )
				character.receiveDamage(target_general, 3);
	}
}