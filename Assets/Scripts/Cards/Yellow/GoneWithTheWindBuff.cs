using UnityEngine;

public class GoneWithTheWindBuff : Buff
{
	protected override string Description { get { return "+2 movement until end of turn."; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.End_turn )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}
	}

	public override int getMovementAfterBuff(int movement)
	{
		return movement + 2;
	}
}