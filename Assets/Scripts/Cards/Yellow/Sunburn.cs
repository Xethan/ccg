using UnityEngine;

public class Sunburn : CombatTrick
{
    public override bool isTargetValid(Tile target)
    {
        return target.Character != null;
    }

    protected override void doEffect(Tile target)
    {
        if ( target.Character is Unit affected_unit )
            affected_unit.receiveModifier(typeof(VulnerableBuff), param:2);
        else if ( target.Character is General affected_general )
            affected_general.receiveModifier(typeof(TemporaryBuff), buff:typeof(VulnerableBuff), param:1, param2:1);
    }
}