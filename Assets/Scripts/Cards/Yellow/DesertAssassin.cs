using UnityEngine;

public class DesertAssassin : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(BackstabBuff), param:2);
		receiveModifier(typeof(FlankingBuff), param:1);
		receiveModifier(typeof(IncreasedMovementBuff), param:1);
	}
}