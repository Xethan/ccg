using UnityEngine;

public class WindElemental : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(WindElementalBuff));
	}
}