using UnityEngine;

public class SquallSwordBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( (trigger.type == Trigger.e_type.Attack
			  || trigger.type == Trigger.e_type.Counter_attack)
		     && trigger.source == _buffed_character )
		{
			Coord direction = trigger.receiver.Coord - _buffed_character.Coord;

			// Can't push diagonally
			if ( _buffed_character.Coord.isAligned(trigger.receiver.Coord) )
				trigger.receiver.push(_buffed_character, direction, 2);
		}
	}
}