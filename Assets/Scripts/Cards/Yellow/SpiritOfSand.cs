using UnityEngine;

public class SpiritOfSand : Unit
{
	protected override void onSummon()
	{
		receiveGlobalModifier(typeof(SpiritOfSandBuff), source:this);
	}
}