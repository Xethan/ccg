using UnityEngine;
using UnityEngine.UI;

public class SandBurrowTile : SpecialTile
{
	private string	_sand_devil_prefab_path = "Tokens/Sand Devil";
	private int		_nb_summons_left = 2;

	protected override string Name { get { return "Sand Burrow"; } }
	protected override string Description
	{
		get
		{
			return "At the start of your turn, if this tile is unoccupied, summon a 3/3 Rush "
                    + "that dies at the end of the turn on this tile.\n"
                    + _nb_summons_left + (_nb_summons_left > 1 ? " summons" : " summon") + " left.";
        }
    }

	public override void startTurn(Player current_player)
	{
		if ( current_player != _owner)
			return;

		if ( _tile.Character == null )
		{
			Character sand_devil = Tools.instantiateCardFromPath<Character>(_sand_devil_prefab_path, _owner);
			sand_devil.summon(_tile);

			Trigger.activate(Trigger.e_type.Tile_proc, tile: _tile);

			_nb_summons_left -= 1;
			if ( _nb_summons_left == 0 )
				_tile.loseSpecialTile(_owner);
			updateUI();
		}
	}
}