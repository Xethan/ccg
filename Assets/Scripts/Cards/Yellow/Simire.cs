using UnityEngine;

public class Simire : General
{
	protected override string BattleCryPrefabPath { get { return "Yellow/Blink"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 10, 40}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Backstab && trigger.source.Owner == _owner )
			gainXp(2);

		if ( trigger.type == Trigger.e_type.Flanking && trigger.source.Owner == _owner )
			gainXp(1);

		base.onTrigger(trigger);
	}

	protected override void onLevel2()
	{
		receiveModifier(typeof(SimireBuff));
	}
}