using UnityEngine;
using System; // Predicate / Action

public class Blink : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		if ( target.Character is Unit && target.Character.Owner == _owner )
		{
			foreach (Coord second_target_coord in target.Coord.createSafeCenteredSquareCoords(radius: 1))
				if ( Board.isSymmetricTeleportValid(target.Coord, second_target_coord) )
					return true;
		}
		return false;
	}

	protected override void doEffect(Tile first_target)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			return (
				second_target != null
				&& second_target.Coord.squareDistance(first_target.Coord) == 1
				&& Board.isSymmetricTeleportValid(first_target.Coord, second_target.Coord)
			);
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			first_target.Character.symmetricTeleport(this, second_target.Coord);
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}