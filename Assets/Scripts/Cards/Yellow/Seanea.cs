using UnityEngine;

public class Seanea : Unit
{
	protected override void onSummon()
	{
		receiveModifier(typeof(IncreasedRangeBuff), param:1);
         _flags |= Character.e_flags.StrategicUnit;
	}
}