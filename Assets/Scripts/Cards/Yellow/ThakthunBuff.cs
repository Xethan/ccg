using UnityEngine;
using System;
using System.Linq;

public class ThakthunBuff : GlobalCharacterModifier
{
	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if (
			trigger.Is("Attack")
			&& trigger.source is Unit
			&& (trigger.source as Unit).Is("Spider")
			&& trigger.source.Owner == _buffed_character.Owner
			&& Board.getSpecialTile(
				(trigger.source as Unit).Coord,
				_buffed_character.Owner
			) is WebTile
		) {
			Unit attacker = trigger.source as Unit;

			 if ( BackstabBuff.isBackstabAttack(attacker, trigger.receiver) )
                return amount + 2;

            if ( FlankingBuff.isFlankingAttack(attacker, trigger.receiver) )
                return amount + 1;
		}

		if ( trigger.Is("Death") && trigger.receiver == this._source )
		{
			_buffed_character.loseModifier(this);
			GameObject.Destroy(this);
		}

		return amount;
	}
}