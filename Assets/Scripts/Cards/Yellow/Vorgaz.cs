using UnityEngine;
using System;
using System.Collections.Generic;

public class Vorgaz : General
{
	private bool	_allied_push_done = false;
	private bool	_ennemy_push_without_damage_done = false;
	private bool	_ennemy_push_with_damage_done = false;

	protected override string BattleCryPrefabPath { get { return "Yellow/Fear"; } }
	protected override int[] XpNeededToLevelUp { get { return new int[] {0, 3, 4}; } }

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Push && trigger.source.Owner == _owner )
		{
			if ( _allied_push_done == false && trigger.receiver.Owner == _owner )
			{
				_allied_push_done = true;
				gainXp(1);
			}
			if ( _ennemy_push_without_damage_done == false && trigger.receiver.Owner != _owner
				&& trigger.tile2.Coord.distance(trigger.tile.Coord) == trigger.amount )
			{
				_ennemy_push_without_damage_done = true;
				gainXp(1);
			}
		}
		if ( _ennemy_push_with_damage_done == false
			 && trigger.type == Trigger.e_type.Push_damage
			 && trigger.receiver.Owner != _owner )
		{
			_ennemy_push_with_damage_done = true;
			gainXp(1);
		}

		base.onTrigger(trigger);
	}

	protected override void onLevel2()
	{
		_activated_abilities = new List<ActivatedAbility>
		{
			new VorgazAbility(character:this)
		};
	}
}

public class VorgazAbility : ActivatedAbility
{
	private int	_nb_charges = 2;
	private List<Character>	_pushed_characters_this_turn = new List<Character>();

	public override string Cost { get { return "1 charge"; } }
	public override string Name { get { return "Push adjacent character and take its position if available"; } }

	public VorgazAbility(Character character) : base(character) {}

	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.Is("End_turn") )
		{
			_nb_charges = 2;
			_pushed_characters_this_turn.Clear();
		}
	}

	public override bool payCost()
	{
		if ( _nb_charges > 0 )
		{
			_nb_charges -= 1;
			return true;
		}
		return false;
	}

	public override void doEffect()
	{
		Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			if ( target != null )
			{
				Coord coord_diff = target.Coord - _character.Coord;
				Coord end_coord = target.Coord + coord_diff;
				if ( end_coord.isInsideBoard() )
				{
					Tile end_tile = Board.getTile(end_coord);
					return ( target.Character != null
						     && !_pushed_characters_this_turn.Contains(target.Character)
						     && _character.Coord.distance(target.Coord) == 1
						     && end_tile.Character == null );
				}
			}
			return false;
		};
		Action<GameObject> do_ability_effect = (GameObject gameobject) =>
		{
			Tile target = gameobject.GetComponent<Tile>();
			_pushed_characters_this_turn.Add(target.Character);
			target.Character.push(_character, target.Coord - _character.Coord, 1);
			if ( target.Character == null )
			{
				Board.getTile(_character.Coord).Character = null;
				target.Character = _character;
			}
		};
		TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
	}
}