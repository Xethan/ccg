using UnityEngine;

public class WindBlow : CombatTrick
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		Coord[]	directions = new Coord[4];

		directions[0] = new Coord(-1, 0);
		directions[1] = new Coord(1, 0);
		directions[2] = new Coord(0, 1);
		directions[3] = new Coord(0, -1);

		for (int i = 0; i < directions.Length; ++i)
		{
			Coord target_coord = target.Coord + directions[i];
			if ( target_coord.isOutOfBoard() == false )
			{
				Character character_to_push = Board.getCharacter(target_coord);
				if ( character_to_push != null )
					character_to_push.push(this, directions[i], 1);
			}
		}
	}
}