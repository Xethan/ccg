using UnityEngine;
using System; // Predicate / Action
using System.Collections;

public class TurningWind : Spell
{
	private Character	_first_pusher = null;
	private Character	_pushed_character = null;

	public override bool isTargetValid(Tile target)
	{
		if ( target != null && target.Character != null && target.Character.Owner == _owner )
			foreach (Character _ in Board.getCharactersInRange(target.Coord.createSafeCenteredCircleCoords, radius:1))
				return true;
		return false;
	}

	protected override void doEffect(Tile target)
	{
		_doFirstPart(target.gameObject);
		_first_pusher = target.Character;
		TargetingPanel.doTargetingAbility(_isSecondPusherValid, _doSecondPushEffect);
	}

	private bool _isSecondPusherValid(GameObject gameobject)
	{
		Tile target = gameobject.GetComponent<Tile>();
		return ( _pushed_character != null
				 && target != null
				 && target.Character != null
				 && target.Character.Owner == _owner
				 // Ensure that the first and second pusher are not on the same line or column
				 // so that the second push is perpendicular to the first one
				 && target.Character.Coord.X != _first_pusher.Coord.X
				 && target.Character.Coord.Y != _first_pusher.Coord.Y
				 && target.Character.Coord.distance(_pushed_character.Coord) == 1 );
	}

	private void _doSecondPushEffect(GameObject gameobject)
	{
		Character pusher = gameobject.GetComponent<Tile>().Character;
		_pushed_character.push(pusher, _pushed_character.Coord - pusher.Coord, 2);
	}

	private void _doFirstPart(GameObject pusher_tile_gameobject)
	{
		Tile pusher_tile = pusher_tile_gameobject.GetComponent<Tile>();

		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			return ( second_target != null && second_target.Character != null && pusher_tile.Coord.distance(second_target.Coord) == 1 );
		};
		Action<GameObject> doFirstPushEffect = (GameObject gameobject) =>
		{
			_pushed_character = gameobject.GetComponent<Tile>().Character;
			_pushed_character.push(pusher_tile.Character, _pushed_character.Coord - pusher_tile.Coord, 2);
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, doFirstPushEffect);
	}
}