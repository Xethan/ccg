using UnityEngine;

public class WarriorOfTheSandBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Attack && trigger.source == _buffed_character )
			trigger.receiver.receiveModifier(typeof(WeaknessBuff));
	}
}