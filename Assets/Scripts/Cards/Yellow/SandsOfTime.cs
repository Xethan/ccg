using UnityEngine;
using System.Collections.Generic;

public class SandsOfTime : Spell
{
    public override bool isTargetValid(Tile target)
    {
        return true;
    }

    protected override void doEffect(Tile target)
    {
        List<Character> teleported_characters = new List<Character>();
        for (int radius = 1; radius <= 2; ++radius)
        {
            foreach (Character character in Board.getCharactersInRange(target.Coord.createSafeCenteredSquareCoords, radius:radius))
            {
                if ( !(character is General) && !teleported_characters.Contains(character) )
                {
                    character.symmetricTeleport(this, target.Coord);
                    teleported_characters.Add(character);
                }
            }
        }
    }
}