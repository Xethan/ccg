using UnityEngine;

public class ExpandThePack : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		target.setSpecialTile("Tiles/Sand Burrow", _owner);
	}
}