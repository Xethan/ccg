using UnityEngine;

public class WarriorOfTheSand : Unit
{
	protected override void onSummon()
	{		
		receiveModifier(typeof(WarriorOfTheSandBuff));
	}
}