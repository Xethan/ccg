using UnityEngine;
using System; // Predicate / Action

public class Fear : BattleCry
{
	public override bool isTargetValid(Tile target)
	{
		if ( target != null && target.Character != null && target.Character.Owner == _owner )
		{
			Coord[]	directions = new Coord[4];

			directions[0] = new Coord(-1, 0);
			directions[1] = new Coord(1, 0);
			directions[2] = new Coord(0, 1);
			directions[3] = new Coord(0, -1);

			for (int i = 0; i < directions.Length; ++i)
			{
				Coord target_coord = target.Coord + directions[i];

				if ( target_coord.isOutOfBoard() == false && Board.getCharacter(target_coord) != null )
					return true;
			}
		}
		return false;
	}

	protected override void doEffect(Tile first_target)
	{
		Predicate<GameObject> isSecondTargetValid = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			return second_target != null && second_target.Character != null && first_target.Coord.distance(second_target.Coord) == 1;
		};

		Action<GameObject> card_effect = (GameObject gameobject) =>
		{
			Tile second_target = gameobject.GetComponent<Tile>();
			second_target.Character.push(first_target.Character, second_target.Coord - first_target.Coord, 2);
		};

		TargetingPanel.doTargetingAbility(isSecondTargetValid, card_effect);
	}
}