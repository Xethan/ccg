using UnityEngine;
using System.Collections.Generic;

public class SsyssisBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character 
			 && !Board.isMyTurn(_buffed_character.Owner))
			{
				findGeneralWithLeastHealth(_buffed_character.Owner.Generals).receiveHeal(5);
			}
		if ( trigger.type == Trigger.e_type.Death && trigger.receiver == _buffed_character 
			 && Board.isMyTurn(_buffed_character.Owner))
			{
				findGeneralWithLeastHealth(Board.getEnnemyPlayer().Generals).receiveDamage(_buffed_character, 4);
			}
	}

	private General findGeneralWithLeastHealth(List<General> generals)
	{
		General general_with_least_health = null;
		int min_health = 0;

		foreach (General general in generals)
			if ( general_with_least_health == null || general.Health < min_health )
			{
				general_with_least_health = general;
				min_health = general.Health;
			}
		return general_with_least_health;
	}
}