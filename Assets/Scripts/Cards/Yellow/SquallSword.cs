using UnityEngine;
using System.Collections.Generic;

public class SquallSword : Artifact
{
    protected override int BaseDurability { get { return 5; } }
    protected override string Name { get { return "Squall Sword"; } }
    protected override string Description { get { return "Attacks and counter-attacks push their target 2 tiles away."; } }

    public override bool isTargetValid(Tile target)
    {
        return target.Character != null;
    }

    protected override List<Modifier> createModifiers(Character equipped_character)
    {
        return new List<Modifier> {
            equipped_character.createModifier(typeof(SquallSwordBuff))
        };
    }
}
