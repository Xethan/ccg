using UnityEngine;
using System.Collections.Generic;

public class DaggerOfSand : Artifact
{
	protected override int BaseDurability { get { return 9; } }
    protected override string Name { get { return "Dagger of Sand"; } }
    protected override string Description { get { return "Backstab 2 and +2 movement"; } }

    public override bool isTargetValid(Tile target)
    {
        return target.Character != null;
    }

    protected override List<Modifier> createModifiers(Character equipped_character)
    {
        return new List<Modifier> {
			equipped_character.createModifier(typeof(BackstabBuff), param:2),
			equipped_character.createModifier(typeof(IncreasedMovementBuff), param:2)
		};
    }
}
