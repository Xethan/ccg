using UnityEngine;
using System.Collections.Generic;
using System;

public class Roublabot : Unit
{
    protected override void onSummon()
    {
        receiveModifier(typeof(RoublabotBuff));
        _activated_abilities = new List<ActivatedAbility>
        {
            new RoublabotPushAbility(character:this),
            new RoublabotPullAbility(character:this),
            new RoublabotBlinkAbility(character:this),
        };
    }
}

public class RoublabotPushAbility : ActivatedAbility
{
    private readonly int[]  _ability_mana_cost = {1, 0, 0, 0, 0, 0};
    private List<Character> _already_pushed = new List<Character>();

    public override string Cost { get { return "1"; } }
    public override string Name { get { return "Push target character up to 3 tiles away (can't do damage)"; } }

    public RoublabotPushAbility(Character character) : base(character) {}

    public override bool payCost()
    {
        return _character.Owner.ManaReserve.payManaCost(this, _ability_mana_cost);
    }

    public override void doEffect()
    {
        MultiTargetingPanel.doTargetingAbility(
            new List<Func<GameObject, List<GameObject>, bool>>{_is_first_target_valid, _is_second_target_valid},
            _do_ability_effect);
    }

    private bool _is_first_target_valid(GameObject gameobject, List<GameObject> previous_targets)
    {
        Tile target = gameobject.GetComponent<Tile>();

        if (
            target == null
            || target.Character == null
            || _already_pushed.Contains(target.Character)
            || _character.Coord.distance(target.Coord) != 1
        )
            return false;

        // Verify there is at least one empty tile behind the pushed character
        Coord vector_launcher_target = target.Coord - _character.Coord;
        Coord end_coord = target.Coord + vector_launcher_target;
        return end_coord.isInsideBoard() && Board.getCharacter(end_coord) == null;
    }

    private bool _is_second_target_valid(GameObject gameobject, List<GameObject> previous_targets)
    {
        Tile target_to_push = previous_targets[0].GetComponent<Tile>();
        Tile tile_where_pushed = gameobject.GetComponent<Tile>();

        if ( tile_where_pushed == null
             || !tile_where_pushed.Coord.isAligned(_character.Coord)
             || !tile_where_pushed.Coord.isAligned(target_to_push.Coord)
             || tile_where_pushed.Coord.distance(target_to_push.Coord) > 3
             || tile_where_pushed.Character != null )
            return false;

        // Verify the pushed character will not collide with any other character
        Coord unitary_vector_first_to_second_target = (tile_where_pushed.Coord - target_to_push.Coord).unitaryCoord();
        for (int i = 1; i <= target_to_push.Coord.distance(tile_where_pushed.Coord); ++i)
        {
            Coord coord_to_test = target_to_push.Coord + (unitary_vector_first_to_second_target * i);
            if ( Board.getCharacter(coord_to_test) != null )
                return false;
        }

        return true;
    }

    private void _do_ability_effect(List<GameObject> targets)
    {
        Tile target = targets[0].GetComponent<Tile>();
        int amount = targets[1].GetComponent<Tile>().Coord.distance(target.Coord);
        _already_pushed.Add(target.Character);
        target.Character.push(_character, target.Coord - _character.Coord, amount);
    }
}

public class RoublabotPullAbility : ActivatedAbility
{
    private readonly int[]  _ability_mana_cost = {1, 0, 0, 0, 0, 0};
    private List<Character> _already_pulled = new List<Character>();

    public override string Cost { get { return "1"; } }
    public override string Name { get { return "Pull target character up to 3 tiles away"; } }

    public RoublabotPullAbility(Character character) : base(character) {}

    public override bool payCost()
    {
        return _character.Owner.ManaReserve.payManaCost(this, _ability_mana_cost);
    }

    public override void doEffect()
    {
        Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
        {
            Tile target = gameobject.GetComponent<Tile>();
            if (
                target == null
                || target.Character == null
                || _already_pulled.Contains(target.Character)
                || !_character.Coord.isAligned(target.Coord)
                || _character.Coord.distance(target.Coord) < 2
                || _character.Coord.distance(target.Coord) > 3
            )
                return false;

            // Verify the pushed character will not collide with any other character
            Coord unitary_vector_target_to_roublabot = (_character.Coord - target.Coord).unitaryCoord();
            for (int i = 1; i < target.Coord.distance(_character.Coord); ++i)
            {
                Coord coord_to_test = target.Coord + (unitary_vector_target_to_roublabot * i);
                if ( Board.getCharacter(coord_to_test) != null )
                    return false;
            }

            return true;
        };
        Action<GameObject> do_ability_effect = (GameObject gameobject) =>
        {
            Tile target = gameobject.GetComponent<Tile>();
            _already_pulled.Add(target.Character);
            target.Character.pull(_character, _character);
        };
        TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
    }
}

public class RoublabotBlinkAbility : ActivatedAbility
{
    private readonly int[]  _ability_mana_cost = {1, 0, 0, 0, 0, 0};

    public override string Cost { get { return "1"; } }
    public override string Name { get { return "Symmetrically teleport with respect to target adjacent character."; } }

    public RoublabotBlinkAbility(Character character) : base(character) {}

    public override bool payCost()
    {
        return _character.Owner.ManaReserve.payManaCost(this, _ability_mana_cost);
    }

    public override void doEffect()
    {
        Predicate<GameObject> is_target_valid = (GameObject gameobject) =>
        {
            Tile target = gameobject.GetComponent<Tile>();
            return (
                target != null
                && target.Coord.distance(_character.Coord) == 1
                && Board.isSymmetricTeleportValid(_character.Coord, target.Coord)
            );
        };
        Action<GameObject> do_ability_effect = (GameObject gameobject) =>
        {
            Tile target = gameobject.GetComponent<Tile>();
            _character.symmetricTeleport(_character, target.Coord);
        };
        TargetingPanel.doTargetingAbility(is_target_valid, do_ability_effect);
    }
}