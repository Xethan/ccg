using UnityEngine;

public class RoublabotBuff : Buff
{
    private int _movements_left = 6;

    public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
    {
        if ( _movements_left > 0 )
            return flags | Character.e_flags.Can_move;
        else
            return flags;
    }

    public override int getMovementAfterBuff(int movement)
    {
        return _movements_left;
    }

    public override void onTrigger(Trigger trigger)
    {
        if ( trigger.type == Trigger.e_type.Movement && trigger.source == _buffed_character )
            _movements_left -= trigger.amount;

        if ( trigger.type == Trigger.e_type.End_turn )
            _buffed_character.death();
    }

}