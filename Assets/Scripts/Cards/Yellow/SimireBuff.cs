using UnityEngine;
using System;
using System.Linq;

public class SimireBuff : GlobalCharacterModifier
{
    public override int getValueEffectAfterBuff(Trigger trigger, int amount)
    {
        if (
            trigger.Is("Attack")
            && trigger.source.Owner == _buffed_character.Owner
        ) {
            Character attacker = trigger.source as Character;

            if ( BackstabBuff.isBackstabAttack(attacker, trigger.receiver) )
                return amount + 2;

            if ( FlankingBuff.isFlankingAttack(attacker, trigger.receiver) )
                return amount + 1;
        }

        return amount;
    }
}