using UnityEngine;

public class KhaZixBuff : Buff
{
	public override void onTrigger(Trigger trigger)
	{
		if ( trigger.type == Trigger.e_type.Attack && trigger.source == _buffed_character && Board.isCharacterIsolated(trigger.receiver) )
			_buffed_character.reactivate(Character.e_flags.Can_move);
	}

	public override int getValueEffectAfterBuff(Trigger trigger, int amount)
	{
		if ( trigger.type == Trigger.e_type.Attack && trigger.source == _buffed_character && Board.isCharacterIsolated(trigger.receiver) )
			return amount + 2;
		if ( trigger.type == Trigger.e_type.Counter_attack && trigger.receiver == _buffed_character && Board.isCharacterIsolated(trigger.source as Character) )
			return amount - 2;
		return amount;
	}

	public override int getMovementAfterBuff(int movement)
	{
		return movement + 1;
	}
}