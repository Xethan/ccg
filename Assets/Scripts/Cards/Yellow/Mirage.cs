using UnityEngine;
using System.Collections.Generic;

public class Mirage : CombatTrick
{
	public override bool isTargetValid(Tile target)
	{
		return true;
	}

	protected override void doEffect(Tile target)
	{
		_owner.Generals[1].teleport(_owner.Generals[1], _owner.Generals[0].Coord, allow_tp_generals:true);
	}
}