using UnityEngine;

public class EliteRangerBuff : Buff
{
    private Board.e_daytime _summoning_daytime;

    public override void onTrigger(Trigger trigger)
    {
        if (
            trigger.Is("End_turn")
            && trigger.player == _buffed_character.Owner
            && Board.daytime == Board.e_daytime.Moon
        )
            _buffed_character.receiveHeal(5);
    }

    public override int getHealthAfterBuff(int health)
    {
        return Board.daytime == Board.e_daytime.Moon ? health + 5 : health;
    }

    public override int getMaxHealthAfterBuff(int max_health)
    {
        return Board.daytime == Board.e_daytime.Moon ? max_health + 5 : max_health;
    }

    public override int getValueEffectAfterBuff(Trigger trigger, int amount)
    {
        if (
            Board.daytime == Board.e_daytime.Moon
            && trigger.Is("Damage")
            && trigger.source == _buffed_character
            && _buffed_character.Health >= trigger.receiver.Health
        )
            return amount + 2;
        return amount;
    }

    public override int getRangeAfterBuff(int range)
    {
        return Board.daytime == Board.e_daytime.Sun ? range + 1 : range;
    }

    public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
    {
        if ( Board.daytime == Board.e_daytime.Sun )
            return flags | Character.e_flags.StrategicUnit;
        else
            return flags;
    }
}