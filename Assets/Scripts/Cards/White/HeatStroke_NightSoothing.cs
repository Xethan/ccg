using UnityEngine;

public class HeatStroke_NightSoothing : Spell
{
    public override bool isTargetValid(Tile target)
    {
        return target.Character != null;
    }

    protected override void doEffect(Tile target)
    {
        if ( Board.daytime == Board.e_daytime.Sun )
            target.Character.receiveDamage(this, 6);
        else
            target.Character.receiveHeal(6);
    }
}