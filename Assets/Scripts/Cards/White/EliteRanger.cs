using UnityEngine;

public class EliteRanger : Unit
{
    protected override void onSummon()
    {
        receiveModifier(typeof(EliteRangerBuff));
    }
}