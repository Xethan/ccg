using UnityEngine;

public class VersatileSoldierBuff : Buff
{
    private Board.e_daytime _summoning_daytime;

    public override void onTrigger(Trigger trigger)
    {
        if ( trigger.type == Trigger.e_type.Summon && trigger.receiver == this )
            _summoning_daytime = Board.daytime;
    }

    public override Character.e_flags getFlagsAfterBuff(Character.e_flags flags)
    {
        if ( _summoning_daytime == Board.e_daytime.Sun )
            return flags | Character.e_flags.Provoke;
        else
            return flags | Character.e_flags.Tackle;
    }
}