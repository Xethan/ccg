using UnityEngine;

public class YoungInitiate : Unit
{
    protected override void onSummon()
    {
        receiveModifier(typeof(YoungInitiateBuff));
    }
}