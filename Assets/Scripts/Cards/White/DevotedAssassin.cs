using UnityEngine;

public class DevotedAssassin : Unit
{
    protected override void onSummon()
    {
        if ( Board.daytime == Board.e_daytime.Sun )
            _flags |= Character.e_flags.Rush;
        else
            _flags |= Character.e_flags.StrategicUnit;
    }
}