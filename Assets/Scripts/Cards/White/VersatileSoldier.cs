using UnityEngine;

public class VersatileSoldier : Unit
{
    protected override void onSummon()
    {
        receiveModifier(typeof(VersatileSoldierBuff));
    }
}