using UnityEngine;

public class DisturbedCycle : CombatTrick
{
    public override bool isTargetValid(Tile target)
    {
        return true;
    }

    protected override void doEffect(Tile target)
    {
        Board.changeDaytime();
    }
}