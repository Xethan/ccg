using UnityEngine;

public class YoungInitiateBuff : Buff
{
    private int _armor_left_at_end_of_own_turn = 0;

    public override void onTrigger(Trigger trigger)
    {
        if ( trigger.Is("Moonrise")
            || (
                trigger.Is("Summon")
                && trigger.receiver == _buffed_character
                && Board.daytime == Board.e_daytime.Moon
            )
        )
            _buffed_character.receiveArmor(3);

        if (
            Board.daytime == Board.e_daytime.Moon
            && trigger.Is("End_turn")
            && Board.getCurrentPlayer().isPlayerOne
        )
            _armor_left_at_end_of_own_turn = _buffed_character.Armor;

        if ( trigger.Is("Start_turn") && !Board.getCurrentPlayer().isPlayerOne )
        {
            _buffed_character.receiveArmor(_armor_left_at_end_of_own_turn);
            _armor_left_at_end_of_own_turn = 0;
        }
    }

    public override int getAttackAfterBuff(int attack)
    {
        return Board.daytime == Board.e_daytime.Sun ? attack + 1 : attack;
    }
}