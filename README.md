### General presentation of the project : ###
I'm Xethan, a 19 years old student at [42 Paris](https://www.42.fr). I'm passionated by programming, esport and video and board games (in particular strategic and competitive games).
My name is Aspal, I'm a 21 years old engineering student. I participated in game design and cards creation.

This repository contains our work on an Unity CCG we developped to improve and show our coding and game design capabilities.
You can download the latest version of my work at :

* [Mac OS](https://drive.google.com/file/d/0B6Y79iWS1m7cR0R4c1d6TzlqMzA/view?usp=sharing)

* [Windows](https://drive.google.com/drive/folders/0B6Y79iWS1m7cSFc1ZG1HbXYyaVU?usp=sharing)


### Gameplay : ###
Our game is a **grid and turn based CCG** taking various inspirations from strategy games such as Duelyst, Dofus, Eredan ITCG or Magic the Gathering.

**Cards** are divided into several **factions** and decks are formed using a **General**, a **Lieutenant** and a list of cards. The **General** and **Lieutenant** will define whether your deck is **mono-faction** or **bi-faction** and thus which cards you can or can't put in your deck. Each card can be present up to a maximum of 3 copies.

The game is played on a **rectangle shaped grid** with the **General** and **Lieutenant** of each player already placed on their respective side of the field (the leftmost and rightmost column).

Both players start with **2 neutral mana**, **1 mana** from the respective **faction** of their **General** and **Lieutenant** and **5 cards** from their deck.
Each turn except for the first turn of the first player, each player can **consume** once a card from their hand **to gain a neutral mana** and a **mana from one of the factions** of the consumed card (if the card is not neutral).

**Charaters** controlled since the beginning of the turn **can move and then attack**. Characters can move up to 2 tiles and attack ennemies on the 8 nearby tiles.

Cards (**Characters** or **Spells**) with **various abilities and effects** can be played from the player's hand by paying the required **Mana cost**.

Every time an **ennemy character dies**, you **gain an amount of xp equal to its converted mana cost** and your **General** becomes **stronger** when reaching **level 2**.

At the **end of the turn**, the player **draws 2 cards** and then the game goes on until one player **wins** by either **killing the ennemy General** or **reaching level 3**.


### Collection :  ###
The **Collection** allows you to create, save or delete **custom decks**.
Decks are saved in a text file serialized in json using the [Newtonsoft framework](http://www.newtonsoft.com/json). (For the moment, they are saved without any protection so you can technically cheat to add cards that you couldn't normally add. It is planned to add protection, probably by encrypting the json).
There is a faction and a text filter to help you navigate the **Collection** and create **Decks** easily.


### How to play : ###
Since there is currently no in-game indication of what is to be dragged, left-clicked or right-clicked (everithing is done with the mouse), here is a rundown of how to play :

  - **Collection :**

    ~ Left click a deck to open it

    ~ Navigate through the collection by clicking the two arrow shaped buttons

    ~ **Left click** a card in the collection to **add it to the deck**

    ~ **Left click** a card in the deck to **remove a copy** from it

    ~ Use the **search box** and click on the **faction filters** to narrow down the pool of cards

  - **Deck Selection :**

    ~ Navigate through the pages with the arrow shaped buttons

    ~ **Left click and right click** a deck to respectively **select the decks** of players 1 and 2

  - **In Game :**

    ~ **Drag a card** from hand to the Mana UI to **consume** it (for multi-faction cards, you have to drag it to the desired faction mana). A toggle indicate wether or not you can consume

    ~ **Drag a character or a spell** from hand to a **yellow tile** (valid target tiles) to play it if you can **pay the mana cost**

    ~ **Drag a character** who can **move** from its tile to a valid tile (in white) to move it

    ~ **Drag a character** to an **adjacent ennemy** to **attack** it

    ~ Use your General's or Lieutenant's **Battle Cry** as a normal spell if it's **off cooldown**


### Additional notes : ###
Both players play on the same screen since network is not part of my current knowledge.

Most graphic assets I used come from Duelyst, property of Counterplay Games".

Thanks to [icon8](https://icons8.com/) for their icons too.