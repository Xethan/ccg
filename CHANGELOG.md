# CHANGELOG

### [18 octobre 2023]
- Séparation de `Provoke` en `Tackle` et `Provoke`
- Bugfix de `PlantTheBomb` et de Simire niveau 2 quand des généraux attaquent
- Modifications de cartes (Spirit à 3 mana, nerf de Simire, buff de Spectral Blade et de Dagger of Sand, ajustement des cartes `Provoke`, refonte de `VersatileSoldier`)

### [8 mai 2023]
- Complétion partielle du PanelKeywordsUI
- Supression de la mécanique de Telefrag, rework de Simire en général Backstab / Flanking
- Modification de beaucoup de cartes
- Qualité de code

### [19 mars 2023]
- Ajout d'un unittest pour `Thakthun`, `Kahanaa` et `Chthunafh`
- Ajout d'un champ `Types` pour les `Unit`

### [27 février 2023]
- Ajout d'un unittest pour `CorrosionBuff`
- Ajout d'araignées : `Kahanaa`, `Sonoth`, `Cthumnom`, `Thakthun`, `Yoggoth`, `Kagnoth`

### [23 novembre 2022]
- Ajout d'un unittest pour `Roublabot` (avec nouvelle fonction `_simulateLeftClick`)

### [29 octobre 2022]
- Unification des sources de couleurs pour les factions
- Ajout de `Trigger.Is` pour raccourcir les vérifications du type d'un trigger

### [5 septembre 2022]
- Ajout de `YoungInitiate`, `EliteRanger`, `DevotedAssassin`, `VersatileSoldier`, `DisturbedCycle` et de leurs unittests

### [29 août 2022]
- Ajout de la faction blanche, avec la mécanique de `Soleil` et de `Lune`
- Fonctions qui gèrent le pathing et l'initialisation des `Players` déplacées de `Board` vers leurs propres fichiers

### [3 avril 2022]
- Création de `Sunburn`, `Sands of Time`, `Roublabot`
- Ajout du `MultiTargetingPanel`
- Utilisation de `Board.getCharacter` et `Board.getTile` partout où c'était possible
- La page de tutoriel affiche maintenant directement l'arbre complet des pages

### [2 janvier 2022]
- Les `Artifact` sont maintenant des types de cartes à part entière, ils héritent de `Card` et plus de `Spell`
- Création d'une `TriggerQueue` pour éviter les boucles infinies lorsque deux créatures s'entretuaient avec leur effet de mort
- Changement du fonctionnement de la mana de réserve et de l'avantage du joueur 2.

### [19 décembre 2021]
- Les personnages ne pouvant plus attaquer sont désormais grisés via le shader `Greyscale`

### [4 décembre 2021]
- Ajout de la scène des `Tutorial`

### [13 novembre 2021]
- Ajout des `KeywordsUI`

### [17 octobre 2021]
- `Frenzy Of Nature` redevient un Sort, `Armor Of Nature` devient un Combat Trick
- Création de `RelatedCardsPanel`
- Ajout d'un test pour les `ManaCostModif`

### [13 septembre 2021]
- Création de `Shaman of Gigantism`, `Slahxo`, `Seanea`, `Dagger of Sand`, `Desert Assassin`, `Backstab` et `Flanking`

### [6 septembre 2021]
- Ajout de `Blacksmith of War`
- Les `ManaCostModif` modifient désormais l'UI des `CardSlot`

### [5 septembre 2021]
- Les généraux ne comptent plus dans le décompte des 40 cartes d'un deck
- Supression de la majorité des références aux lieutenants

### [22 août 2021]
- Création de `Voodoo Doll` et `Spectral Blade`
- `Stomper` et `Flamethrower` passent jaune-violet, `WindRunner` passe jaune
- Modification du niveau 2 de `Xethan`, du pouvoir d'`Azir`, de `BloodPact`, `BombDrops`, `CorrosiveBlade`, `WindRunner`, `TimeJailer`, `Trample`, `Telefrag`, `Blink`

### [22 août 2021]
- Implémentation de `Fleeting`
- Ajustements de l'UI des cartes

### [22 août 2021]
- Ajout des `CombatTricks` et un début de gestion des modes de jeu

### [27 juin 2021]
- Fix de l'interaction entre Though et ShiftingSand, ajout d'un unittest
- Création de la fonction `Board.getCharactersInRange`, utilisé partout où c'est utile

### [19 juin 2021]
- Reworks : Spirit of nature, Spirit of sand, Stompy 4, Stompy 5, Embodiment of wisdom
- Création de Stun bomb, Hashas, Strength of nature, Detorkhan, Draskels, Thaka
- Ajout des keywords Hunter, LifeSteal, Lightning, Link, Regeneration

### [27 mai 2021]
- Création du général push jaune `Vorgaz` qui récupère le BattleCry `SquallSword`
- Création de `Mastodont`, `UnitManaBlocker`, `BombDrops`, `ChainReaction`, `DancingBlades`, `CorrosiveBlade`, `CorrosiveCurse` et `TurningWind`
- Sonnine a maintenant le BattleCry `PressureRise`
- Le BattleCry de Berserk augmente maintenant de 1 l'attaque de ses unités pendant son tour et de 1 les dégâts subis pendant le tour adverse
- Quand Zirix est niveau 2, il gagne maintenant à chaque début de tour une carte qui permet de bouger un ShiftingSand de jusqu'à 2 cases
- `ManaBlocker` augmente maintenant de 1 les coûts colorés de la prochaine carte jouée au tour de l'adversaire
- Les cartes ShiftingSand passent en violet
- Les `PublicGiftTile` ajoutent maintenant un mana à la réserve au lieu de donner une carte
- Certains scripts et prefabs de cartes ont été transférés vers un nouveau dossier `Archived`

### [27 février 2021]
- Ajustements de `Anger of Nature`, `Shepherd`, `Heart of the Sand`, `Bomb`, `Aspal`, `Mercenary`

### [10 février 2021]
- Ajout de `Grimmali`, `AngerOfNature`, `FrenzyOfNature`, `FuryOfNature`, `Grow`, `Kirnous`, `Kisrus`, `Risthy`, `Chthunafh`, `Gothkas`, `MindImprint`, `Protector`, `Spider`, `SpiderWeb`, `Thalthres`, `WebTile` et des outils associés

### [29 janvier 2021]
- La sélection de cartes peut désormais afficher une carré de sélection verte au lieu d'une croix rouge de remplacement
- Quand on peut sélectionner une seule carte, il n'y a plus besoin de déselectionner avant de pouvoir en sélectionner une autre

### [12 janvier 2021]
- Bugfix : la mort se fait à la fin d'une attaque / contre-attaque et plus pendant
- Gros buffs pour les cartes et la tuile ShiftingSand
- Ajustements de puissance cartes vertes (nerf créatures, buff artefact attaque, nerf pouvoir Aspal et cartes liées à l'attaque du général)

### [24 décembre 2020]
- Création de la classe `ManaCostModif` pour pouvoir appliquer des augmentations / réductions de coût en mana

### [6 décembre 2020]
- Ajout d'une classe `EventQueue` pour gérer les évènement qui arrivent en même temps (exemple : PublicGiftTile)

### [6 décembre 2020]
- Added cards to the decks in the unittest scenario so that the game doesn't end when calling `endTurn`

### [30 novembre 2020]
- HoverUI pour les SpecialTile

### [29 novembre 2020]
- Bugfix du cooldown de BattleCry pour le général 1
- Ajout de la classe `Scenario` pour faire des unittests
- Les decks ne sont plus limités aux 2 couleurs des généraux

### [29 novembre 2020]
- Changement de pouvoir pour Xethan
- La condition du gain d'xp de Berserk passe de "pendant son tour" à "pendant le tour adverse"
- Equilibrages

### [19 août 2020]
- Swap de cartes entre violet et jaune pour mieux coller à la thématique des factions (telefrags / dégâts de zone / entrave)
- Ajout de Berserk, Herald of War, Shelly, Chain Lightning, Plant the Bomb, Royal Assassin, Sand Golem
- Interdiction du Téléfrag sur les généraux adverses
- Interdiction de la prise des Public Gift Tiles par des Tokens
- Changement des noms de factions dans le code et l'arborescence de fichiers (Faction 1 -> Green, etc.)